<?php

namespace App\Actions\Payments;

use App\Models\CompanySetting;
use Srmklive\PayPal\Facades\PayPal;
use Throwable;

class PaypalPayment
{

  private $paypalProvider;
  private $paypal_credential;

  /**
   * @throws Throwable
   */
  public function __construct()
  {
    $this->paypal_credential = config('paypal');

    $company = CompanySetting::first(['paypal_client_id', 'paypal_client_secret']);
    if ($company->paypal_mode == 'live') {
      $this->paypal_credential['live']['client_id'] = $company->paypal_client_id ?? '';
      $this->paypal_credential['live']['client_secret'] = $company->paypal_client_secret ?? '';
    }else {
      $this->paypal_credential['sandbox']['client_id'] = $company->paypal_client_id ?? '';
      $this->paypal_credential['sandbox']['client_secret'] = $company->paypal_client_secret ?? '';
    }

    PayPal::setProvider();
    $this->paypalProvider = PayPal::getProvider();
    $this->paypalProvider->setApiCredentials($this->paypal_credential);
    $this->paypalProvider->setAccessToken($this->paypalProvider->getAccessToken());
  }

  /**
   * @throws Throwable
   */
  public function paymentRequest($amount)
  {
    return $this->paypalProvider->createOrder([
      "intent"              => "CAPTURE",
      "purchase_units"      => [
        [
          "amount" => [
            "currency_code" => "USD",
            "value"         => $amount
          ]
        ]
      ],
      "application_context" => [
        "cancel_url" => route('payment.cancel'),
        "return_url" => route('payment.success')
      ]
    ]);
  }

  /**
   * @throws Throwable
   */
  public function completePayment($token)
  {
    return $this->paypalProvider->capturePaymentOrder($token);
  }
}
