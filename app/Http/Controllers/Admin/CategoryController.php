<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Subcategory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;
use Illuminate\Validation\Rule;

class CategoryController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    $this->checkPermission(['admin', 'category.all', 'category.add', 'category.view', 'category.edit', 'category.delete']);
    $categories = Category::paginate($this->itemPerPage);
    $this->putSL($categories);
    return response()->view('dashboard.category.index', compact('categories'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    $this->checkPermission(['admin', 'category.all', 'category.add']);
    return response()->view('dashboard.category.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param Request $request
   * @return RedirectResponse
   */
  public function store(Request $request)
  {
    $this->checkPermission(['admin', 'category.all', 'category.add']);
    $request->validate([
      'name'   => 'required|string|max:255|unique:categories',
      'image'  => 'nullable|image|mimes:jpeg,png,jpg|max:512',
      'status' => ['required', Rule::in('0', '1')],
    ]);

    $image_path = null;
    if ($request->hasFile('image')) {
      $image_path = Rand() . '.' . $request->image->getClientOriginalExtension();
      $image_path = $request->image->storeAs('category', $image_path, 'public');
    }

    Category::create([
      'name'  => $request->input('name'),
      'image' => $image_path,
    ]);
    return back()->with('success', 'Saved Successfully.');
  }

  /**
   * Display the specified resource.
   *
   * @param Category $category
   * @return Response
   */
  public function show(Category $category)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param Category $category
   * @return Response
   */
  public function edit(Category $category)
  {
    $this->checkPermission(['admin', 'category.all', 'category.edit']);
    return response()->view('dashboard.category.edit', compact('category'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param Request $request
   * @param Category $category
   * @return RedirectResponse
   */
  public function update(Request $request, Category $category)
  {
    $this->checkPermission(['admin', 'category.all', 'category.edit']);
    $request->validate([
      'name'   => 'required|string|max:255|unique:categories,id,' . $category->id,
      'image'  => 'nullable|image|mimes:jpeg,png,jpg|max:512',
      'status' => ['required', Rule::in('0', '1')],
    ]);

    $image_path = $category->image;
    if ($request->hasFile('image')) {
      if (File::exists('storage/' . $image_path)) {
        File::delete('storage/' . $image_path);
      }
      $image_path = Rand() . '.' . $request->image->getClientOriginalExtension();
      $image_path = $request->image->storeAs('category', $image_path, 'public');
    }

    $category->update([
      'name'   => $request->input('name'),
      'image'  => $image_path,
      'status' => $request->input('status'),
    ]);
    return back()->with('success', 'Updated Successfully.');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param Category $category
   * @return RedirectResponse
   */
  public function destroy(Category $category)
  {
    $this->checkPermission(['admin', 'category.all', 'category.delete']);
    if ($category->delete()) {
      if (File::exists('storage/' . $category->image)) {
        File::delete('storage/' . $category->image);
      }
      return back()->with('success', 'Item Deleted Successfully.');
    }
    return back()->with('error', 'Item Con\'t Delete.');
  }

  //ajax subcategory request
  public function categorySubcategory(Request $request)
  {
    if (is_array($request->input('category_id'))) {
      $subcategories = Subcategory::whereStatus(1)->whereIn('category_id', $request->input('category_id'))->get();
    } else {

      $subcategories = Subcategory::whereStatus(1)->where('category_id', $request->input('category_id'))->get();
    }
    foreach ($subcategories as $subcategory) {
      echo "<option value='" . $subcategory->id . "'>" . $subcategory->name . '</option>';
    }
  }

  public function categoryEditSubcategory(Request $request)
  {
    if (is_array($request->input('category_id'))) {
      $subcategories = Subcategory::whereStatus(1)->whereIn('category_id', $request->input('category_id'))->get();
    } else {
      $subcategories = Subcategory::whereStatus(1)->where('category_id', $request->input('category_id'))->get();
    }
    foreach ($subcategories as $subcategory) {
      echo "<option value='" . $subcategory->id . "'>" . $subcategory->name . '</option>';
    }
  }

}
