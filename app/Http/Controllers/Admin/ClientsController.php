<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Client;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class ClientsController extends Controller
{
  public function index(): View
  {
    $this->checkPermission(['admin', 'clients.all', 'clients.view', 'clients.delete']);
    $clients = Client::all();
    return view('dashboard.clients.index', compact('clients'));
  }

  public function create(): View
  {
    $this->checkPermission(['admin', 'clients.all', 'clients.add']);
    return view('dashboard.clients.create');
  }

  public function store(Request $request): RedirectResponse
  {
    $this->checkPermission(['admin', 'clients.all', 'clients.add']);
    $request->validate([
      'logo' => ['required', 'image', 'max:512'],
      'name' => ['required', 'string', 'max:255'],
      'status' => ['required', 'boolean'],
    ]);

    Client::create([
      'name' => $request->input('name'),
      'status' => $request->input('status'),
    ])->addMediaFromRequest('logo')->toMediaCollection('logo');
    return back()->with('success', 'Client information successfully saved.');
  }

  public function edit(Client $client): View
  {
    $this->checkPermission(['admin', 'clients.all', 'clients.edit']);
    return view('dashboard.clients.edit', compact('client'));
  }

  public function update(Request $request, Client $client)
  {
    $this->checkPermission(['admin', 'clients.all', 'clients.edit']);
    $request->validate([
      'logo' => ['nullable', 'image', 'max:512'],
      'name' => ['required', 'string', 'max:255'],
      'status' => ['required', 'boolean'],
    ]);
    $client->update([
      'name' => $request->input('name'),
      'status' => $request->input('status'),
    ]);
    if ($request->hasFile('logo')) {
      $client->addMediaFromRequest('logo')->toMediaCollection('logo');
    }
    return back()->with('success', 'Client information successfully updated.');
  }

  public function destroy(Client $client): RedirectResponse
  {
    $this->checkPermission(['admin', 'clients.all', 'clients.delete']);
    $client->delete();
    return back()->with('success', 'Client successfully deleted.');
  }
}
