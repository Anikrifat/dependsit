<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\GuestOrder;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class GuestOrderController extends Controller
{
  public function showGuestOrders(): View
  {
    $orders = GuestOrder::when(request()->get('order_filter'), function ($query) {
      $query->where('status', request()->get('order_filter'));
    })->paginate(10);
    return view('dashboard.guest-order.index', compact('orders'));
  }

  public function showGuestOrderDetails(GuestOrder $guestOrder): View
  {
    return view('dashboard.guest-order.view', compact('guestOrder'));
  }

  public function changeOrderStatus(Request $request, GuestOrder $guestOrder): RedirectResponse
  {
    $request->validate([
      'order_status' => ['required', Rule::in(GuestOrder::STATUS)]
    ]);
    $guestOrder->update([
      'status' => $request->input('order_status'),
    ]);
    return back()->with('success', 'Order status successfully updated.');
  }

  public function deleteGuestOrder(GuestOrder $guestOrder): RedirectResponse
  {
    $guestOrder->delete();
    return back()->with('success', 'Order successfully deleted.');
  }
}
