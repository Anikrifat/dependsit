<?php

namespace App\Http\Controllers\Admin;

use App\Actions\Payments\PaypalPayment;
use App\Http\Controllers\Controller;
use App\Http\Requests\InvoiceRequest;
use App\Models\Invoice;
use App\Notifications\SendInvoiceLink;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Password;
use Throwable;

class InvoiceController extends Controller
{
  public function index()
  {
    $this->checkPermission(['admin', 'invoice.all', 'invoice.add', 'invoice.view', 'invoice.edit', 'invoice.delete']);

    $invoices = Invoice::paginate($this->itemPerPage);
    $this->putSL($invoices);
    return view('dashboard.invoice.index', compact('invoices'));
  }

  public function create()
  {
    $this->checkPermission(['admin', 'invoice.all', 'invoice.add']);

    $pageTitle = 'Create Invoice';
    $formAction = route('invoice.store');
    return view('dashboard.invoice.create', compact('pageTitle', 'formAction'));
  }

  public function store(InvoiceRequest $request): RedirectResponse
  {
    $this->checkPermission(['admin', 'invoice.all', 'invoice.add']);

    DB::transaction(function () use ($request, &$invoice) {
      $invoice = Invoice::create($request->validated() + ['remember_token' => Password::getRepository()->createNewToken()]);

      foreach ($request->input('name') as $key => $name) {
        $invoice->Items()->create([
          'name'        => $request->input('name')[$key],
          'price'       => $request->input('price')[$key],
          'quantity'    => $request->input('quantity')[$key],
          'description' => $request->input('description')[$key],
        ]);
      }
    });

    if ($request->input('btn_type') == 'save') {
      $invoice->notify(new SendInvoiceLink($invoice));
    }

    return redirect()->route('invoice.show', $invoice->id)->with('success', 'Invoice Created Successfully.');
  }

  public function show(Invoice $invoice)
  {
    $this->checkPermission(['admin', 'invoice.all', 'invoice.view']);

    return view('dashboard.invoice.view', compact('invoice'));
  }

  public function edit(Invoice $invoice)
  {
    $this->checkPermission(['admin', 'invoice.all', 'invoice.edit']);

    $pageTitle = 'Edit Invoice';
    $formAction = route('invoice.update', $invoice->id);
    return view('dashboard.invoice.create', compact('invoice', 'pageTitle', 'formAction'));
  }

  public function update(InvoiceRequest $request, Invoice $invoice): RedirectResponse
  {
    $this->checkPermission(['admin', 'invoice.all', 'invoice.edit']);

    DB::transaction(function () use ($request, $invoice) {
      $invoice->update($request->validated());
      $invoiceItems = $invoice->Items();
      $invoiceItems->delete();

      foreach ($request->input('name') as $key => $name) {
        $invoiceItems->create([
          'name'        => $request->input('name')[$key],
          'price'       => $request->input('price')[$key],
          'quantity'    => $request->input('quantity')[$key],
          'description' => $request->input('description')[$key],
        ]);
      }
    });

    if ($request->input('btn_type') == 'save') {
      $invoice->notify(new SendInvoiceLink($invoice));
    }

    return redirect()->route('invoice.show', $invoice->id)->with('success', 'Invoice Created Successfully.');
  }

  public function destroy(Invoice $invoice): RedirectResponse
  {
    $this->checkPermission(['admin', 'invoice.all', 'invoice.delete']);

    $invoice->delete();
    return redirect()->route('invoice.index')->with('success', 'Invoice Deleted Successfully.');
  }

  public function invoicePayView(Invoice $invoice, $token = false)
  {
    $invoice->where('payment_status', 0)->where('remember_token', $token)->firstOrFail();
    return view('dashboard.invoice.view', compact('invoice'));
  }

  /**
   * @throws Throwable
   */
  public function pay(Invoice $invoice): RedirectResponse
  {
    $response = (new PaypalPayment())->paymentRequest($invoice->grand_total);
    $invoice->paypal_token = $response['id'];
    $invoice->save();
    $redirectUrl = $response['links'][1]['href'];
    return redirect()->to($redirectUrl);
  }
}
