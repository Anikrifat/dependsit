<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Package;
use App\Models\Page;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class PackagesController extends Controller
{
  public function index(Page $page): View
  {
    $packages = $page->packages;
    return view('dashboard.pages.manage-packages', compact('page', 'packages'));
  }

  public function updatePackageInfo(Request $request, Page $page): RedirectResponse
  {
    $validated = $request->validate([
      'package_heading' => ['required', 'string'],
      'package_sub_heading' => ['required', 'string']
    ]);
    $page->update($validated);
    return back()->with('success', 'Package information updated.');
  }

  public function store(Request $request, Page $page): RedirectResponse
  {
    $validated = $request->validate([
      'heading' => ['required', 'string'],
      'sub_heading' => ['required', 'string'],
      'price' => ['required', 'numeric'],
      'features' => ['required', 'array'],
      'features.*' => ['string'],
      'order' => ['required', 'integer']
    ]);
    $page->packages()->create($validated);
    return back()->with('success', 'Package successfully added.');
  }

  public function update(Request $request, Page $page, Package $package): RedirectResponse
  {
    $validated = $request->validate([
      'heading' => ['required', 'string'],
      'sub_heading' => ['required', 'string'],
      'price' => ['required', 'numeric'],
      'features' => ['required', 'array'],
      'features.*' => ['string'],
      'order' => ['required', 'integer']
    ]);
    $package->update($validated);
    return back()->with('success', 'Package successfully updated.');
  }

  public function destroy(Page $page, Package $package): RedirectResponse
  {
    $package->delete();
    return back()->with('success', 'Package successfully deleted.');
  }

  public function getPackageInfo(Page $page, Package $package): JsonResponse
  {
    return response()->json($package->toArray());
  }
}
