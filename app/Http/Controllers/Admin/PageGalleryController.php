<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Page;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class PageGalleryController extends Controller
{
  public function getPageGallery(Page $page): View
  {
    return view('dashboard.pages.manage-gallery', compact('page'));
  }

  public function updatePageGallery(Request $request, Page $page): RedirectResponse
  {
    $imageRule = $page->hasMedia('gallery') ? 'nullable' : 'required';
    $request->validate([
      'gallery_image_one_heading' => ['required', 'string'],
      'gallery_image_one' => [$imageRule, 'image', 'max:1024'],
      'gallery_image_two_heading' => ['required', 'string'],
      'gallery_image_two' => [$imageRule, 'image', 'max:1024'],
      'gallery_image_three_heading' => ['required', 'string'],
      'gallery_image_three' => [$imageRule, 'image', 'max:1024'],
      'gallery_image_four_heading' => ['required', 'string'],
      'gallery_image_four' => [$imageRule, 'image', 'max:1024'],
      'gallery_image_five_heading' => ['required', 'string'],
      'gallery_image_five' => [$imageRule, 'image', 'max:1024'],
    ]);

    foreach (['gallery_image_one', 'gallery_image_two', 'gallery_image_three', 'gallery_image_four', 'gallery_image_five'] as $item) {
      $media = $page->getMedia('gallery', ['name' => $item])->first();
      if ($media) $media->setCustomProperty('heading', $request->get($item . '_heading'))->save();
      if ($request->has($item)) {
        if ($media) $media->delete();
        $page->addMediaFromRequest($item)->withCustomProperties([
          'name' => $item,
          'heading' => $request->get($item . '_heading')
        ])->toMediaCollection('gallery');
      }
    }

    return back()->with('success', 'Gallery successfully updated.');
  }
}
