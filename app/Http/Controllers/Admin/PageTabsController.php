<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Page;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class PageTabsController extends Controller
{
  public function getPageTabs(Page $page)
  {
    return view('dashboard.pages.manage-tabs', compact('page'));
  }

  public function updatePageTabs(Request $request, Page $page): RedirectResponse
  {
    $imageRule = $page->hasMedia('tabs') ? 'nullable' : 'required';
    $request->validate([
      'tab_one_name' => ['required', 'string'],
      'tab_one_heading' => ['required', 'string'],
      'tab_one_description' => ['required', 'string'],
      'tab_one_image' => [$imageRule, 'image', 'max:1024'],
      'tab_two_name' => ['required', 'string'],
      'tab_two_heading' => ['required', 'string'],
      'tab_two_description' => ['required', 'string'],
      'tab_two_image' => [$imageRule, 'image', 'max:1024'],
      'tab_three_name' => ['required', 'string'],
      'tab_three_heading' => ['required', 'string'],
      'tab_three_description' => ['required', 'string'],
      'tab_three_image' => [$imageRule, 'image', 'max:1024'],
      'tab_four_name' => ['required', 'string'],
      'tab_four_heading' => ['required', 'string'],
      'tab_four_description' => ['required', 'string'],
      'tab_four_image' => [$imageRule, 'image', 'max:1024'],
    ]);

    foreach (['tab_one', 'tab_two', 'tab_three', 'tab_four'] as $item) {
      $media = $page->getMedia('tabs', ['key' => $item])->first();
      if ($media) {
        $media->setCustomProperty('name', $request->get($item . '_name'));
        $media->setCustomProperty('heading', $request->get($item . '_heading'));
        $media->setCustomProperty('description', $request->get($item . '_description'));
        $media->save();
      }
      if ($request->has($item . '_image')) {
        if ($media) $media->delete();
        $page->addMediaFromRequest($item . '_image')->withCustomProperties([
          'key' => $item,
          'name' => $request->get($item . '_name'),
          'heading' => $request->get($item . '_heading'),
          'description' => $request->get($item . '_description')
        ])->toMediaCollection('tabs');
      }
    }

    return back()->with('success', 'Tab information successfully updated.');
  }
}
