<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\PageRequest;
use App\Models\Package;
use App\Models\Page;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class PagesController extends Controller
{
  public function index(): View
  {
    $pages = Page::all();
    return view('dashboard.pages.index', compact('pages'));
  }

  public function create(): View
  {
    return view('dashboard.pages.create');
  }

  public function store(PageRequest $request): RedirectResponse
  {
    $validated = $request->validated();
    unset($validated['icon']);
    unset($validated['banner']);
    $page = Page::create($validated);
    if ($request->hasFile('banner')) {
      $page->addMediaFromRequest('banner')->toMediaCollection('banner');
    }
    if ($request->hasFile('icon')) {
      $page->addMediaFromRequest('icon')->toMediaCollection('icon');
    }
    return redirect()->route('pages.edit', $page->id)->with('success', 'Page successfully created.');
  }

  public function edit(Page $page): View
  {
    return view('dashboard.pages.edit', compact('page'));
  }

  public function update(PageRequest $request, Page $page): RedirectResponse
  {
    $validated = $request->validated();
    unset($validated['icon']);
    unset($validated['banner']);
    unset($validated['banner_heading']);
    unset($validated['banner_description']);
    if (is_array($page->properties))
      $validated['properties'] = array_merge($page->properties, $validated['properties']);
    $page->update($validated);
    if ($request->hasFile('icon')) {
      $page->addMediaFromRequest('icon')->toMediaCollection('icon');
    }
    if ($request->hasFile('banner')) {
      $page->addMediaFromRequest('banner')->toMediaCollection('banner');
    }
    if ($page->hasMedia('banner')) {
      $media = $page->getFirstMedia('banner');
      $media->setCustomProperty('heading', $request->get('banner_heading'));
      $media->setCustomProperty('description', $request->get('banner_description'));
      $media->save();
    }
    return back()->with('success', 'Page information successfully updated.');
  }

  public function destroy(Page $page): RedirectResponse
  {
    $page->delete();
    return back()->with('success', 'Page successfully deleted.');
  }
}
