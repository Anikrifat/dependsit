<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Portfolio;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class PortfoliosController extends Controller
{
  public function index(): View
  {
    $this->checkPermission(['admin', 'portfolios.all', 'portfolios.view', 'portfolios.delete']);
    $portfolios = Portfolio::all();
    return view('dashboard.portfolios.index', compact('portfolios'));
  }

  public function create(): View
  {
    $this->checkPermission(['admin', 'portfolios.all', 'portfolios.add']);
    return view('dashboard.portfolios.create');
  }

  public function store(Request $request): RedirectResponse
  {
    $this->checkPermission(['admin', 'portfolios.all', 'portfolios.add']);
    $request->validate([
      'image' => ['required', 'image', 'max:512'],
      'name' => ['required', 'string', 'max:255'],
      'url' => ['nullable', 'string', 'max:255'],
      'type' => ['required', Rule::in(Portfolio::TYPE)],
      'status' => ['required', 'boolean'],
    ]);

    Portfolio::create([
      'name' => $request->input('name'),
      'status' => $request->input('status'),
      'url' => $request->input('url'),
      'type' => $request->input('type')
    ])->addMediaFromRequest('image')->toMediaCollection('portfolio');
    return back()->with('success', 'Portfolio information successfully saved.');
  }

  public function edit(Portfolio $portfolio): View
  {
    $this->checkPermission(['admin', 'portfolios.all', 'portfolios.edit']);
    return view('dashboard.portfolios.edit', compact('portfolio'));
  }

  public function update(Request $request, Portfolio $portfolio)
  {
    $this->checkPermission(['admin', 'portfolios.all', 'portfolios.edit']);
    $request->validate([
      'image' => ['nullable', 'image', 'max:512'],
      'name' => ['required', 'string', 'max:255'],
      'url' => ['nullable', 'string', 'max:255'],
      'type' => ['required', Rule::in(Portfolio::TYPE)],
      'status' => ['required', 'boolean'],
    ]);
    $portfolio->update([
      'name' => $request->input('name'),
      'status' => $request->input('status'),
      'url' => $request->input('url'),
      'type' => $request->input('type')
    ]);
    if ($request->hasFile('image')) {
      $portfolio->addMediaFromRequest('image')->toMediaCollection('portfolio');
    }
    return back()->with('success', 'Portfolio information successfully updated.');
  }

  public function destroy(Portfolio $portfolio): RedirectResponse
  {
    $this->checkPermission(['admin', 'portfolios.all', 'portfolios.delete']);
    $portfolio->delete();
    return back()->with('success', 'Portfolio successfully deleted.');
  }
}
