<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\PostRequest;
use App\Models\Category;
use App\Models\Post;
use App\Models\Tag;
use Illuminate\Http\RedirectResponse;

class PostController extends Controller
{
  public function index()
  {
    $data['posts'] = Post::whereStatus(1)->orderByDesc('id')->paginate($this->itemPerPage);
    $this->putSL($data['posts']);
    return view('dashboard.post.index', $data);
  }

  public function create()
  {
    $data['categories'] = Category::where('status', 1)->get();
    $data['tags'] = Tag::where('status', 1)->get();
    return view('dashboard.post.create', $data);
  }

  public function store(PostRequest $request): RedirectResponse
  {
    $post = Post::create([
      'added_by'        => auth()->id(),
      'category_id'     => $request->input('category_id'),
      'subcategory_id'  => $request->input('subcategory_id'),
      'url'             => $request->input('url'),
      'da'              => $request->input('da'),
      'pa'              => $request->input('pa'),
      'dr'              => $request->input('dr'),
      'cbd_casino'      => $request->input('cbd_casino'),
      'language'        => $request->input('language'),
      'links'           => $request->input('links'),
      'monthly_traffic' => $request->input('monthly_traffic'),
      'country'         => $request->input('country'),
      'buyer_price'     => $request->input('buyer_price'),
      'reseller_price'  => $request->input('reseller_price'),
      'ads_from'        => $request->input('ads_from'),
      'last_active'     => $request->input('last_active'),
      'status'          => $request->input('status'),
    ]);

    if ($request->input('tag_id')) {
      foreach ($request->input('tag_id') as $tag_id) {
        Tag::firstOrCreate(
          ['id' => (int)$tag_id],
          ['name' => $tag_id]
        )->Posts()->attach($post->id);
      }
    }

    return back()->with('success', 'Post Created Successfully.');
  }

  public function show(Post $post)
  {
    //
  }

  public function edit(Post $post)
  {
    $data['post'] = $post;
    $data['tags'] = Tag::where('status', 1)->get();
    $data['categories'] = Category::where('status', 1)->get();
    return view('dashboard.post.edit', $data);
  }

  public function update(PostRequest $request, Post $post): RedirectResponse
  {
    $post->update([
      'added_by'        => auth()->id(),
      'category_id'     => $request->input('category_id'),
      'subcategory_id'  => $request->input('subcategory_id'),
      'url'             => $request->input('url'),
      'da'              => $request->input('da'),
      'pa'              => $request->input('pa'),
      'dr'              => $request->input('dr'),
      'cbd_casino'      => $request->input('cbd_casino'),
      'language'        => $request->input('language'),
      'links'           => $request->input('links'),
      'monthly_traffic' => $request->input('monthly_traffic'),
      'country'         => $request->input('country'),
      'buyer_price'     => $request->input('buyer_price'),
      'reseller_price'  => $request->input('reseller_price'),
      'ads_from'        => $request->input('ads_from'),
      'last_active'     => $request->input('last_active'),
      'status'          => $request->input('status'),
    ]);

    if ($request->input('tag_id')) {
      $tags = [];
      foreach ($request->input('tag_id') as $key => $tag_id) {
        $tags[$key] = Tag::firstOrCreate(
          ['id' => (int)$tag_id],
          ['name' => $tag_id]
        )->id;
      }
      $post->Tags()->sync($tags);
    }

    return back()->with('success', 'Post Updated Successfully.');
  }

  public function destroy(Post $post): RedirectResponse
  {
    $post->delete();
    return back()->with('success', 'Deleted Successfully.');
  }
}
