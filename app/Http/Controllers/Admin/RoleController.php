<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
  public function index()
  {
    $data['permissions'] = Permission::all();
    $data['roles'] = Role::all();
    $this->permissions = $data['permissions'];
    return view('layouts.role.createRole', $data);
  }

  public function create()
  {
    //
  }

  public function store(Request $request): RedirectResponse
  {
    $role = Role::create([
      'name' => $request->name
    ]);
    $role->syncPermissions($request->permissions);
    return redirect()->back();
  }

  public function show($id)
  {
    //
  }

  public function edit($id)
  {
    $data['oldrole'] = Role::find($id);
    $data['roles'] = Role::all();
    $data['permissions'] = Permission::all();
    return view('layouts.role.createRole', $data);
  }

  public function update(Request $request, $id): RedirectResponse
  {
    $role = Role::find($id);
    $role->revokePermissionTo($role->getAllPermissions());
    $role->update(['name' => $request->name]);
    $role->syncPermissions($request->permissions);
    return redirect()->back();
  }

  public function destroy($id): RedirectResponse
  {
    Role::find($id)->delete();
    return redirect()->back();
  }

  public function roleAssign()
  {
    $data['users'] = User::all();
    $data['roles'] = Role::all();
    return view('layouts.role.roleassign', $data);
  }

  public function storeAssign(Request $request): RedirectResponse
  {
    $user = User::find($request->user);
    $user->syncRoles($request->roles);
    return redirect()->back();
  }
}
