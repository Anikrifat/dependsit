<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Service;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class ServicesController extends Controller
{
  public function index(): View
  {
    $this->checkPermission(['admin', 'services.all', 'services.view', 'services.delete']);
    $services = Service::all();
    return view('dashboard.services.index', compact('services'));
  }

  public function create(): View
  {
    $this->checkPermission(['admin', 'services.all', 'services.add']);
    return view('dashboard.services.create');
  }

  public function store(Request $request): RedirectResponse
  {
    $this->checkPermission(['admin', 'services.all', 'services.add']);
    $request->validate([
      'icon' => ['required', 'image', 'max:512'],
      'name' => ['required', 'string', 'max:255'],
      'description' => ['required', 'string'],
      'url' => ['required', 'string'],
      'status' => ['required', 'boolean'],
    ]);

    Service::create([
      'name' => $request->input('name'),
      'description' => $request->input('description'),
      'url' => $request->input('url'),
      'status' => $request->input('status'),
    ])->addMediaFromRequest('icon')->toMediaCollection('icon');
    return back()->with('success', 'Service successfully added.');
  }

  public function edit(Service $service): View
  {
    $this->checkPermission(['admin', 'services.all', 'services.edit']);
    return view('dashboard.services.edit', compact('service'));
  }

  public function update(Request $request, Service $service): RedirectResponse
  {
    $this->checkPermission(['admin', 'services.all', 'services.edit']);
    $request->validate([
      'icon' => ['nullable', 'image', 'max:512'],
      'name' => ['required', 'string', 'max:255'],
      'description' => ['required', 'string'],
      'url' => ['required', 'string'],
      'status' => ['required', 'boolean'],
    ]);
    $service->update([
      'name' => $request->input('name'),
      'description' => $request->input('description'),
      'url' => $request->input('url'),
      'status' => $request->input('status')
    ]);
    if ($request->hasFile('icon')) {
      $service->addMediaFromRequest('icon')->toMediaCollection('icon');
    }
    return back()->with('success', 'Service information successfully updated.');
  }

  public function destroy(Service $service): RedirectResponse
  {
    $this->checkPermission(['admin', 'services.all', 'services.delete']);
    $service->delete();
    return back()->with('success', 'Service successfully deleted.');
  }
}
