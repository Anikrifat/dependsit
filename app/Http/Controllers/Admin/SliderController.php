<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Slider;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class SliderController extends Controller
{
  public function index(): View
  {
    $this->checkPermission(['admin', 'slider.all', 'slider.view', 'slider.delete']);
    $slides = Slider::all();
    return view('dashboard.sliders.index', compact('slides'));
  }

  public function create(): View
  {
    $this->checkPermission(['admin', 'slider.all', 'slider.add']);
    return view('dashboard.sliders.create');
  }

  public function store(Request $request): RedirectResponse
  {
    $this->checkPermission(['admin', 'slider.all', 'slider.add']);
    $request->validate([
      'image' => ['required', 'image', 'max:1024'],
      'status' => ['required', 'boolean'],
    ]);

    Slider::create([
      'status' => $request->input('status'),
    ])->addMediaFromRequest('image')->toMediaCollection('slide');
    return back()->with('success', 'Slide successfully added.');
  }

  public function edit(Slider $slider): View
  {
    $this->checkPermission(['admin', 'slider.all', 'slider.edit']);
    return view('dashboard.sliders.edit', compact('slider'));
  }

  public function update(Request $request, Slider $slider): RedirectResponse
  {
    $this->checkPermission(['admin', 'slider.all', 'slider.edit']);
    $request->validate([
      'image' => ['required', 'image', 'max:1024'],
      'status' => ['required', 'boolean']
    ]);
    $slider->update([
      'status' => $request->input('status'),
    ]);
    if ($request->hasFile('image')) {
      $slider->addMediaFromRequest('image')->toMediaCollection('slide');
    }
    return back()->with('success', 'Slide successfully updated.');
  }

  public function destroy(Slider $slider): RedirectResponse
  {
    $this->checkPermission(['admin', 'slider.all', 'slider.delete']);
    $slider->delete();
    return back()->with('success', 'Slide successfully deleted.');
  }
}
