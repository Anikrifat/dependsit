<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Subcategory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;

class SubcategoryController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    $this->checkPermission(['admin', 'subcategory.all', 'subcategory.add', 'subcategory.view', 'subcategory.edit', 'subcategory.delete']);
    $subcategories = Subcategory::paginate($this->itemPerPage);
    $this->putSL($subcategories);
    return response()->view('dashboard.subcategory.index', compact('subcategories'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    $this->checkPermission(['admin', 'subcategory.all', 'subcategory.add']);
    $categories = Category::whereStatus(1)->get();
    return response()->view('dashboard.subcategory.create', compact('categories'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param Request $request
   * @return RedirectResponse
   */
  public function store(Request $request)
  {
    $this->checkPermission(['admin', 'subcategory.all', 'subcategory.add']);
    $request->validate([
      'name'        => 'required|max:255',
      'image'       => 'nullable|image|mimes:jpeg,png,jpg|max:512',
      'category_id' => 'required|exists:categories,id',
      'status'      => 'required',
    ]);

    $image_path = null;
    if ($request->hasFile('image')) {
      $filename = Rand() . '.' . $request->image->getClientOriginalExtension();
      $image_path = $request->image->storeAs('subcategory', $filename, 'public');
    }

    Subcategory::create([
      'name'        => $request->input('name'),
      'image'       => $image_path,
      'category_id' => $request->input('category_id'),
      'status'      => $request->input('status'),
    ]);

    $message = 'Subcategory Created Successfully';
    return back()->with('success', $message);
  }

  /**
   * Display the specified resource.
   *
   * @param Subcategory $subcategory
   * @return Response
   */
  public function show(Subcategory $subcategory)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param Subcategory $subcategory
   * @return Response
   */
  public function edit(Subcategory $subcategory)
  {
    $this->checkPermission(['admin', 'subcategory.all', 'subcategory.edit']);
    $categories = Category::whereStatus(1)->get();
    return response()->view('dashboard.subcategory.edit', compact('categories', 'subcategory'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param Request $request
   * @param Subcategory $subcategory
   * @return RedirectResponse
   */
  public function update(Request $request, Subcategory $subcategory)
  {
    $this->checkPermission(['admin', 'subcategory.all', 'subcategory.edit']);
    $request->validate([
      'name'        => 'required|max:255',
      'image'       => 'nullable|image|mimes:jpeg,png,jpg|max:512',
      'category_id' => 'required|exists:categories,id',
      'status'      => 'required',
    ]);

    $image_path = $subcategory->image;
    if ($request->hasFile('image')) {
      if (File::exists('storage/' . $subcategory->image)) {
        File::delete('storage/' . $subcategory->image);
      }
      $filename = Rand() . '.' . $request->image->getClientOriginalExtension();
      $image_path = $request->image->storeAs('subcategory', $filename, 'public');
    }

    $subcategory->update([
      'name'        => $request->input('name'),
      'image'       => $image_path,
      'category_id' => $request->input('category_id'),
      'status'      => $request->input('status'),
    ]);

    $message = 'Subcategory Updated Successfully';
    return back()->with('success', $message);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param Subcategory $subcategory
   * @return RedirectResponse
   */
  public function destroy(Subcategory $subcategory)
  {
    $this->checkPermission(['admin', 'subcategory.all', 'subcategory.delete']);
    if ($subcategory->delete()) {
      if (File::exists('storage/' . $subcategory->image)) {
        File::delete('storage/' . $subcategory->image);
      }
      return back()->with('success', 'Deleted Successfully.');
    }
    return back()->with('error', 'Item Can\'t Delete.');
  }
}
