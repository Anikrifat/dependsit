<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Tag;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\Rule;

class TagController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    $this->checkPermission(['admin', 'tag.all', 'tag.view', 'tag.delete']);
    $tags = Tag::paginate($this->itemPerPage);
    $this->putSL($tags);
    return response()->view('dashboard.tag.index', compact('tags'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    $this->checkPermission(['admin', 'tag.all', 'tag.add']);
    return response()->view('dashboard.tag.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param Request $request
   * @return RedirectResponse
   */
  public function store(Request $request)
  {
    $this->checkPermission(['admin', 'tag.all', 'tag.add']);
    $request->validate([
      'name'   => ['required', 'string', 'max:255', 'unique:tags'],
      'status' => ['required', Rule::in('0', '1')],
    ]);

    Tag::create([
      'name'   => $request->input('name'),
      'status' => $request->input('status'),
    ]);
    return back()->with('success', 'Saved Successfully.');
  }

  /**
   * Display the specified resource.
   *
   * @param Tag $tag
   * @return Response
   */
  public function show(Tag $tag)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param Tag $tag
   * @return Response
   */
  public function edit(Tag $tag)
  {
    $this->checkPermission(['admin', 'tag.all', 'tag.edit']);
    return response()->view('dashboard.tag.edit', compact('tag'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param Request $request
   * @param Tag $tag
   * @return RedirectResponse
   */
  public function update(Request $request, Tag $tag)
  {
    $this->checkPermission(['admin', 'tag.all', 'tag.edit']);
    $request->validate([
      'name'   => ['required', 'string', 'max:255', 'unique:tags,id,' . $tag->id],
      'status' => ['required', Rule::in('0', '1')],
    ]);

    $tag->update([
      'name'   => $request->input('name'),
      'status' => $request->input('status'),
    ]);
    return back()->with('success', 'Updated Successfully.');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param Tag $tag
   * @return RedirectResponse
   */
  public function destroy(Tag $tag)
  {
    $this->checkPermission(['admin', 'tag.all', 'tag.delete']);
    if ($tag->delete()) {
      return back()->with('success', 'Item Deleted Successfully.');
    }
    return back()->with('error', 'Item Con\'t Delete.');
  }
}
