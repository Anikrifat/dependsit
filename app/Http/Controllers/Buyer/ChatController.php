<?php

namespace App\Http\Controllers\Buyer;

use App\Http\Controllers\Controller;
use App\Models\Chat;
use App\Models\User;
use Illuminate\Http\Request;

class ChatController extends Controller
{
  public function index()
  {
    //
  }

  public function create()
  {
    $users = User::role(['Buyer', 'Reseller'])->with('Chats')->get();
    $chats = $this->chats($users->first()->id);

    if(auth()->user()->hasRole(['Buyer', 'Reseller'])) {
      Chat::where(['receiver_id' => auth()->id(), 'status' => 0])->update(['status' => 1]);
    }else {
      Chat::where(['sender_id' => $users->first()->id, 'receiver_id' => auth()->id(), 'status' => 0])->update(['status' => 1]);
    }

    return view('dashboard.chat.create', compact('chats', 'users'));
  }

  protected function chats($user_id)
  {
    if (auth()->user()->hasRole(['Buyer', 'Reseller'])) {
      $chats = Chat::where('sender_id', auth()->id())->orWhere('receiver_id', auth()->id())->get();
    } else {
      $chats = Chat::whereIn('sender_id', [auth()->id(), $user_id])
        ->whereIn('receiver_id', [auth()->id(), $user_id])
        ->oldest('id')
        ->get();
    }
    return $chats;
  }

  public function adminChat(Request $request)
  {
    Chat::where(['sender_id' => $request->input('user_id'), 'receiver_id' => auth()->id(), 'status' => 0])->update(['status' => 1]);

    $chats = $this->chats($request->input('user_id'));
    return view('dashboard.chat.ajax.message_out', compact('chats'));
  }

  public function store(Request $request)
  {
    $request->validate([
      'message'  => ['nullable', 'string', 'max:1000'],
      'document' => ['nullable', 'file', 'max:5120'],
    ]);

    $document = uploadFile($request->file('document'), 'chat/' . auth()->id() . '/document');

    if (auth()->user()->hasRole(['Buyer', 'Reseller'])) {
      $receiver_id = User::role('Admin')->first()->id;
    } else {
      $receiver_id = $request->input('receiver_id');
    }

    Chat::create([
      'sender_id'   => auth()->id(),
      'receiver_id' => $receiver_id,
      'document'    => $document,
      'message'     => $request->input('message'),
    ]);

    if ($request->ajax()) {
      $chats = $this->chats($receiver_id);
      return view('dashboard.chat.ajax.message_out', compact('chats'));
    }

    return back();
  }

  public function show(Chat $chat)
  {
    //
  }

  public function edit(Chat $chat)
  {
    //
  }

  public function update(Request $request, Chat $chat)
  {
    //
  }

  public function destroy(Chat $chat)
  {
    //
  }
}
