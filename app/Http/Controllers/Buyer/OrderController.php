<?php

namespace App\Http\Controllers\Buyer;

use App\Actions\Payments\PaypalPayment;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Post;
use Cart;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class OrderController extends Controller
{
  public function index()
  {
    if (auth()->user()->hasRole(['Buyer', 'Reseller'])) {
      $all_orders = auth()->user()->Orders();
    } else {
      $all_orders = Order::query();
    }

    $order_filter = null;
    if (request()->has('order_filter') && request()->get('order_filter') != 'all') {
      $order_filter = request()->get('order_filter');
    }

    $orders = $all_orders->when($order_filter, function ($query) use ($order_filter) {
      return $query->where('status', $order_filter);
    })->latest('id')->paginate($this->itemPerPage);

    return view('dashboard.buyer.order.index', compact('orders'));
  }

  public function create()
  {
    //
  }

  /**
   * @throws \Throwable
   */
  public function store(Request $request): RedirectResponse
  {
    $request->validate([
      'posts'               => ['required', 'array'],
      'posts.*'             => ['required', 'exists:posts,id'],
      'with_article'        => ['required', 'boolean'],
      'document'            => [Rule::requiredIf($request->input('with_article') == 1), 'file'],
      'your_niche'          => ['required', 'array'],
      'your_niche.*'        => ['required', 'string'],
      'article_topic'       => ['required', 'string', 'max:255'],
      'anchor_keyword_note' => ['required', 'string', 'max:500'],
      'website_link'        => ['nullable', 'string', 'max:255'],
      'order_requirement'   => ['required', 'string'],
      'special_requirement' => ['required', 'string'],
      'email'               => ['required', 'string'],
      'message'             => ['nullable', 'string'],
      'btn_type'            => ['required', Rule::in('pay_now', 'pay_later')],
    ]);

    $niches = '';
    if ($request->input('your_niche')) {
      $niches = implode(',', $request->input('your_niche'));
    }

    $order_post = [];
    $total_price = 0;
    foreach ($request->input('posts') as $post_id) {
      $post = Post::select(['id', 'buyer_price', 'reseller_price'])->find($post_id);

      if (auth()->user()->hasRole('Buyer')) {
        $total_price += $post->buyer_price;
        $order_post[$post->id] = [
          'price' => $post->buyer_price
        ];
      } elseif (auth()->user()->hasRole('Reseller')) {
        $total_price += $post->reseller_price;
        $order_post[$post->id] = [
          'price' => $post->buyer_price
        ];
      }
    }

    $extra_cost = 0;
    if ($request->input('with_article') == 1) {
      $extra_cost = 10;
      $total_price += $extra_cost;
    }

    $document = uploadFile($request->file('document'), 'orders/document');

    $order = Order::create([
      'customer_id'         => auth()->id(),
      'document'            => $document,
      'your_niche'          => $niches,
      'article_topic'       => $request->input('article_topic'),
      'anchor_keyword_note' => $request->input('anchor_keyword_note'),
      'website_link'        => $request->input('website_link'),
      'order_requirement'   => $request->input('order_requirement'),
      'special_requirement' => $request->input('special_requirement'),
      'with_article'        => $request->input('with_article'),
      'extra_cost'          => $extra_cost,
      'paid_status'         => 0,
      'total_price'         => $total_price,
      'email'               => $request->input('email'),
      'message'             => $request->input('message'),
    ]);
    $order->Posts()->sync($order_post);

    Cart::destroy();

    session()->put('latest_order_id', $order->id);

    if ($request->input('btn_type') == 'pay_later' && auth()->user()->hasRole('Reseller')) {
      return redirect()->route('order.index')->with('success', 'Order successfully added. But payment due.');
    }

    $response = (new PaypalPayment())->paymentRequest($order->total_price);
    $order->paypal_token = $response['id'];
    $order->save();
    $redirectUrl = $response['links'][1]['href'];
    return redirect()->to($redirectUrl);
  }

  public function show(Order $order)
  {
    return view('dashboard.buyer.order.view', compact('order'));
  }

  public function edit(Order $order)
  {
    //
  }

  public function update(Request $request, Order $order)
  {
    //
  }

  public function destroy(Order $order)
  {
    //
  }

  public function payLater(Order $order): RedirectResponse
  {
    abort_if($order->paid_status, 404);

    session()->put('latest_order_id', $order->id);
    return redirect()->route('payment');
  }

  public function changeOrderStatus(Request $request, Order $order): RedirectResponse
  {
    $request->validate([
      'order_status' => ['required', Rule::in('0', '1', '2')]
    ]);

    $order->update([
      'status' => $request->input('order_status'),
    ]);
    return back()->with('success', 'Order Status Changed.');
  }
}
