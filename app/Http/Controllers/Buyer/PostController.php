<?php

namespace App\Http\Controllers\Buyer;

use App\Http\Controllers\Controller;
use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
  public function allPost()
  {
    $category = request()->get('category');
    $follow = request()->get('follow');

    $data['posts'] = Post::whereStatus(1)->when(isset($category) && $category != 'all', function ($query) use ($category) {
      return $query->where('category_id', $category);
    })->when(isset($follow) && $follow != 'all', function ($query) use ($follow) {
      return $query->where('links', $follow);
    })->orderByDesc('id')->paginate($this->itemPerPage);

    return view('dashboard.buyer.post.index', $data);
  }

  public function viewPost(Post $post)
  {
    abort_unless($post->status, 404);
    return view('dashboard.buyer.post.view', compact('post'));
  }

  public function resellerOrder()
  {
    return view('dashboard.reseller.make_order');
  }
}
