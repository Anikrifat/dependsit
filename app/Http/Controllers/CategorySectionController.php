<?php

namespace App\Http\Controllers;

use App\Models\CategorySection;
use Illuminate\Http\Request;

class CategorySectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CategorySection  $categorySection
     * @return \Illuminate\Http\Response
     */
    public function show(CategorySection $categorySection)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CategorySection  $categorySection
     * @return \Illuminate\Http\Response
     */
    public function edit(CategorySection $categorySection)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CategorySection  $categorySection
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CategorySection $categorySection)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CategorySection  $categorySection
     * @return \Illuminate\Http\Response
     */
    public function destroy(CategorySection $categorySection)
    {
        //
    }
}
