<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;

class Controller extends BaseController
{
  use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

  protected $itemPerPage = 15;

  /**
   * Checking user permission
   * @param $permission
   * @param string $message
   * @throws \Exception
   */
  public function checkPermission($permission, string $message = "Permission denied.")
  {
    abort_if(!Auth::user()->hasAnyPermission($permission), 403, $message);
  }

  /**
   * A Function to PUT a sl number with Collection of data.
   * @param $collection | A collection of data collected with paginate function
   * @return void
   */
  public function putSL($collection)
  {
    $start = ($collection->currentPage() * $this->itemPerPage - $this->itemPerPage) + 1;
    $collection->each(function ($value) use (&$start) {
      $value->sl = $start++;
    });
  }
}
