<?php

namespace App\Http\Controllers;

use App\Actions\Payments\PaypalPayment;
use App\Models\GuestOrder;
use App\Models\Invoice;
use App\Models\Order;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use PayPal;
use Throwable;

class PayPalController extends Controller
{
  public function cancel(): RedirectResponse
  {
    if (auth()->user()->hasRole('Buyer')) {
      $order = Order::find(session()->get('latest_order_id'));
      if($order) {
        $order->delete();
      }
    }
    return redirect()->route('dashboard')->with('error', 'Payment process not completed.');
  }


  /**
   * @throws Throwable
   */
  public function success(Request $request): RedirectResponse
  {
    /*
     * Invoice Payment
     * */
    $invoice = Invoice::where('paypal_token', $request->token)->first();
    if ($invoice) {
      $response = (new PaypalPayment())->completePayment($request->token);
      if (isset($response['status']) && $response['status'] == 'COMPLETED') {
        $invoice->paypal_payer_email = $response['payer']['email_address'];
        $invoice->paypal_payer_id = $response['payer']['payer_id'];
        $invoice->payment_status = true;
        $invoice->save();
        return redirect()->route('dashboard')->with('success', 'Payment successful.');
      }
      return redirect()->route('dashboard')->with('error', 'Payment process not completed.');
    }

    /*
     * Fro Guest Order
     * */
    $guestOrder = GuestOrder::where('paypal_token', $request->token)->with('package.page')->first();
    if ($guestOrder) {
      $response = (new PaypalPayment())->completePayment($request->token);
      if (isset($response['status']) && $response['status'] == 'COMPLETED') {
        $guestOrder->paypal_payer_email = $response['payer']['email_address'];
        $guestOrder->paypal_payer_id = $response['payer']['payer_id'];
        $guestOrder->payment_status = true;
        $guestOrder->save();
        return redirect()->route('page', $guestOrder->package->page->slug)->with('success', 'Order completed successfully.');
      }
      return redirect()->route('page', $guestOrder->package->page->slug)->with('error', 'Payment process not completed.');
    }

    /*
     * for Post Order
     * */
    $latest_order = Order::where('paypal_token', $request->token)->first();
    if ($latest_order) {
      $response = (new PaypalPayment())->completePayment($request->token);
      if (isset($response['status']) && $response['status'] == 'COMPLETED') {
        $latest_order->paypal_payer_email = $response['payer']['email_address'];
        $latest_order->paypal_payer_id = $response['payer']['payer_id'];
        $latest_order->paid_status = 1;
        $latest_order->save();
        return redirect()->route('order.index')->with('success', 'Order completed successfully.');
      }
      return redirect()->route('order.index')->with('error', 'Payment process not completed.');
    }
  }
}
