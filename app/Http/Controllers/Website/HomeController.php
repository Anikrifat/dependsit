<?php

namespace App\Http\Controllers\Website;

use App\Models\Post;
use App\Models\Gallery;
use App\Models\Category;
use App\Models\Subcategory;
use App\Models\CategorySection;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
  public function index()
  {
    $data['latest']     = Post::where('status', 1)->latest()->take(9)->get();
    // $data['mostRead']   = Post::sortByDesc('views')->take(5)->get();
    $data['mostRead']   = Post::orderBy('views','DESC')->take(5)->get();
    $data['galleries']   = Gallery::whereStatus(1)->latest()->take(10)->get();
    $data['random'] = Post::where('status', 1)->inRandomOrder()->first();
    // dd($data['mostRead']);
    $data['mainSlider'] = Post::where('status', 1)->where('is_main_slider', 1)->latest()->take(10)->get();
    $data['sideSlider'] = Post::where('status', 1)->where('is_side_slider', 1)->get();
    $data['position1']   = CategorySection::where('status', 1)->where('section_position',1)->first();
    $data['position2']   = CategorySection::where('status', 1)->where('section_position',2)->first();
    $data['position3']   = CategorySection::where('status', 1)->where('section_position',3)->first();
    $data['position4']   = CategorySection::where('status', 1)->where('section_position',4)->first();
    $data['position5']   = CategorySection::where('status', 1)->where('section_position',5)->first();
    $data['position6']   = CategorySection::where('status', 1)->where('section_position',6)->first();
    $data['position7']   = CategorySection::where('status', 1)->where('section_position',7)->first();
    $data['position8']   = CategorySection::where('status', 1)->where('section_position',8)->first();
    $data['position9']   = CategorySection::where('status', 1)->where('section_position',9)->first();
    $data['position10']   = CategorySection::where('status', 1)->where('section_position',10)->first();
    $data['position11']   = CategorySection::where('status', 1)->where('section_position',11)->first();
    $data['position12']   = CategorySection::where('status', 1)->where('section_position',12)->first();

    return view('website.home', $data);
  }

  public function show($id)
  {
    $data['post'] = Post::findOrFail($id);
    abort_unless($data['post']->status, 404);
    $data['post']->views += 1;
    $data['post']->save();
    $data['todayPosts'] = $data['post']->whereStatus(1)->where('date', now()->toDateString())->latest()->take(10)->get();
    $data['postCategoryTodayPosts'] = $data['post']->Category->Posts()->latest()->take(10)->get();
    return view('website.details', $data);
  }

  public function categoryPost(Category $category, Subcategory $subcategory = null)
  {
    abort_unless($category->status, 404);
    $data['category'] = $category;
    $data['thisWeekMostViewedPosts'] = Post::whereStatus(1)
      ->whereBetween('date', [now()->subDays(6)->toDateString(), now()->toDateString()])
      ->orderByDesc('views')
      ->take(10)
      ->get(['id', 'image', 'title']);

    // subcategory wise post
    if (!is_null($subcategory)) {
      abort_unless($subcategory->status, 404);
      $data['subcategory'] = $subcategory;
      $data['posts'] = $subcategory->Posts()->latest()->take(15)->get(['id', 'title', 'image']);
      return view('website.category_post', $data);
    }

    //category wise post
    $data['posts'] = $category->Posts()->latest()->take(15)->get(['id', 'title', 'image']);
    return view('website.category_post', $data);
  }

  public function ajaxCategoryPost(Category $category, Subcategory $subcategory = null, $lastPostId = null)
  {
    abort_unless($category->status, 404);
    $data['category'] = $category;

    // subcategory wise post
    if (!is_null($subcategory)) {
      abort_unless($subcategory->status, 404);
      $data['subcategory'] = $subcategory;
      $data['posts'] = $subcategory->Posts()->where('id', '<', $lastPostId)->latest()->take(12)->get(['id', 'title', 'image']);
      return view('website.category_post', $data);
    }

    //category wise post
    $data['posts'] = $category->Posts()->where('id', '<', $lastPostId)->latest()->take(12)->get(['id', 'title', 'image']);
    return view('website.category_post', $data);
  }

  public function allGallery()
  {
    $data['galleries']   = Gallery::whereStatus(1)->latest()->take(10)->paginate(30);
    return view('website.allgalleries',$data);

  }
}
