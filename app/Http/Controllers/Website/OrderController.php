<?php

namespace App\Http\Controllers\Website;

use App\Actions\Payments\PaypalPayment;
use App\Http\Controllers\Controller;
use App\Models\GuestOrder;
use App\Models\Package;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Srmklive\PayPal\Facades\PayPal;

class OrderController extends Controller
{
  public function showAmountSelectionPage(Package $package): View
  {
    return view('website.payment', compact('package'));
  }

  public function submitAmount(Request $request, Package $package): RedirectResponse
  {
    $request->validate([
      'amount' => ['required', 'numeric'],
    ]);
    $amount = $request->get('amount');
    return redirect()->route('package.purchase.place-order', ['package' => $package->id, 'amount' => $amount]);
  }

  public function showConfirmAndPaymentPage(Package $package, $amount)
  {
    return view('website.shipping-info', compact('amount', 'package'));
  }

  public function placeOrder(Request $request, Package $package): RedirectResponse
  {
    $dynamicRule = auth()->check() ? 'nullable' : 'required';
    $request->validate([
      'first_name' => [$dynamicRule, 'string', 'max:255'],
      'last_name' => [$dynamicRule, 'string', 'max:255'],
      'email' => [$dynamicRule, 'email'],
      'mobile' => [$dynamicRule, 'numeric'],
      'message' => ['required', 'string'],
      'amount' => ['required', 'numeric'],
    ]);
    $order = GuestOrder::create([
      'user_id' => auth()->id() ?? null,
      'package_id' => $package->id,
      'first_name' => $request->get('first_name'),
      'last_name' => $request->get('last_name'),
      'email' => $request->get('email'),
      'mobile' => $request->get('mobile'),
      'message' => $request->get('message'),
      'amount' => $request->get('amount')
    ]);
    $response = (new PaypalPayment())->paymentRequest($order->amount);
    $order->paypal_token = $response['id'];
    $order->save();
    $redirectUrl = $response['links'][1]['href'];
    return redirect()->to($redirectUrl);
  }
}
