<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Models\Client;
use App\Models\Page;
use App\Models\Portfolio;
use App\Models\Service;
use App\Models\Slider;
use Illuminate\Http\Request;

class PagesController extends Controller
{
  public function showPages(Page $page)
  {
    switch ($page->slug) {
      case null:
      case 'home':
        return $this->showHome($page);
      case 'seo':
        return $this->showSeoPage($page);
      case 'clients':
        return $this->showClientsPage($page);
      case 'portfolio':
        return $this->showPortfolioPage($page);
      case 'product':
        return $this->showProductPage($page);
      case 'product-details':
        return $this->showProductDetailsPage($page);
      case 'about-us':
        return $this->showAboutUsPage($page);
      case 'contact-us':
        return $this->showContactUsPage($page);
      default:
        return $this->showDynamicPage($page);
    }
  }

  private function showDynamicPage(Page $page)
  {
    if ($page->type == Page::TYPE['seo'])
      return view('website.dynamic-page.seo', compact('page'));
    elseif ($page->type == Page::TYPE['development'])
      return view('website.dynamic-page.development', compact('page'));
    else
      abort(404);
  }

  private function showSeoPage(Page $page)
  {
    $seoPages = Page::where('type', Page::TYPE['seo'])->get();
    return view('website.seo', compact('page', 'seoPages'));
  }

  private function showHome(Page $page)
  {
    if (!$page->exists)
      $page = Page::where('slug', 'home')->firstOrFail();
    $slides = Slider::whereStatus(true)->get();
    $services = Service::whereStatus(true)->get();
    $clients = Client::whereStatus(true)->get();
    $clientPage = Page::where('slug', 'clients')->first();
    return view('website.home', compact('page', 'slides', 'services', 'clients', 'clientPage'));
  }

  private function showAboutUsPage($page)
  {
    return view('website.about', compact('page'));
  }

  private function showContactUsPage($page)
  {
    return view('website.contact-us', compact('page'));
  }

  private function showProductPage($page)
  {
    return view('website.product', compact('page'));
  }
  private function showProductDetailsPage($page)
  {
    return view('website.product-details', compact('page'));
  }
  private function showPortfolioPage($page)
  {
    $portfolios = Portfolio::whereStatus(true)->get();
    $types = array_flip(Portfolio::TYPE);
    return view('website.portfolio', compact('page', 'portfolios', 'types'));
  }

  private function showClientsPage($page)
  {
    $clients = Client::whereStatus(true)->get();
    return view('website.clients', compact('page', 'clients'));
  }
}
