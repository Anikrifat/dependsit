<?php

use App\Models\Post;
use App\Models\Category;
use App\Models\CompanySetting;
use App\Models\Page;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;

use function PHPSTORM_META\type;

function uploadFile($file, $folder = '/'): ?string
{
  if ($file) {
    $image_name = Rand() . '.' . $file->getClientOriginalExtension();
    return $file->storeAs($folder, $image_name, 'public');
  }
  return null;
}

function setImage($url = null, $type = null): string
{
  if ($type == 'user') {
    return ($url != null) ? asset('storage/' . $url) : asset('default/default_user.png');
  }
  return ($url != null) ? asset('storage/' . $url) : asset('default/default_image.png');
}

function company(): CompanySetting
{
  return CompanySetting::first();
}

function pages($type = 'all')
{
  if ($type == 'all') {
    return Cache::get('pages');
  }
  return Cache::get('pages')->where('type', $type);
}
