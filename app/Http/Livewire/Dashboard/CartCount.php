<?php

namespace App\Http\Livewire\Dashboard;

use Gloudemans\Shoppingcart\Facades\Cart;
use Livewire\Component;

class CartCount extends Component
{
  public $show = false;
  public $cart_count = 0;

  protected $listeners = ['notify' => 'updateCount'];

  public function mount()
  {
    $this->updateCount();
  }

  public function updateCount()
  {
    $this->cart_count = Cart::count();

    if ($this->cart_count > 0) {
      $this->show = true;
    } else {
      $this->show = false;
    }
  }

  public function render()
  {
    return view('livewire.dashboard.cart-count');
  }
}
