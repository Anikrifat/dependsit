<?php

namespace App\Http\Livewire\Dashboard;

use App\Models\Post;
use Gloudemans\Shoppingcart\Facades\Cart;
use Livewire\Component;

class ShoppingCart extends Component
{
  protected $listeners = ['add-to-cart' => 'addToCart'];

  public function render()
  {
    return view('livewire.dashboard.shopping-cart');
  }

  public function addToCart(Post $post)
  {
    if (Cart::content()->where('id', $post->id)->first()) {
      $this->emit('notify', 'Item already in cart.');
      return;
    }
    Cart::add($post->id, $post->url, 1, $post->reseller_price);
    $this->emit('notify', 'Item added to your cart.');
  }

  public function remove($rowId)
  {
    Cart::remove($rowId);
    $this->emit('notify', 'Item removed from your cart.');
  }
}
