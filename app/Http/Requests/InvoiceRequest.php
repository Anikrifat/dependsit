<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InvoiceRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      'bill_to'              => ['required', 'email', 'max:255'],
      'name'                 => ['required', 'array'],
      'name.*'               => ['required', 'string', 'max:255'],
      'quantity'             => ['required', 'array'],
      'quantity.*'           => ['required', 'string', 'max:255'],
      'price'                => ['required', 'array'],
      'price.*'              => ['required', 'string', 'max:255'],
      'description'          => ['required', 'array'],
      'description.*'        => ['nullable', 'string'],
      'recipient_first_name' => ['nullable', 'string'],
      'recipient_last_name'  => ['nullable', 'string'],
      'recipient_mobile'     => ['nullable', 'string'],
      'recipient_company'    => ['nullable', 'string'],
      'sub_total'            => ['required', 'string'],
      'discount'             => ['required', 'string'],
      'grand_total'          => ['required', 'string'],
      'note_to_recipient'    => ['nullable', 'string'],
      'terms_and_conditions' => ['nullable', 'string'],
      'btn_type'             => ['required', 'string'],
    ];
  }
}
