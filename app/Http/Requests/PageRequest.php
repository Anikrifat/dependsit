<?php

namespace App\Http\Requests;

use App\Models\Page;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PageRequest extends FormRequest
{
  public function authorize(): bool
  {
    return true;
  }

  public function rules(): array
  {
    return [
      'icon' => ['nullable', 'image', 'max:512'],
      'banner' => ['nullable', 'image', 'max:1024'],
      'name' => ['required', 'string'],
      'banner_heading' => ['nullable', 'string'],
      'banner_description' => ['nullable', 'string'],
      'slug' => [Rule::requiredIf(!$this->page || ($this->page && !$this->page->build_in)), 'string'],
      'heading' => ['nullable', 'string'],
      'sub_heading' => ['nullable', 'string'],
      'description' => ['nullable', 'string'],
      'type' => [Rule::requiredIf(!$this->page || ($this->page && !$this->page->build_in)), 'integer', Rule::in(Page::TYPE)],
      'status' => ['required', 'boolean'],
      'properties' => ['nullable', 'array']
    ];
  }
}
