<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PostRequest extends FormRequest
{

  public function rules()
  {

    return [
      'category_id'     => 'required|exists:categories,id',
      'subcategory_id'  => 'nullable|exists:subcategories,id',
      'url'             => 'required|string',
      'da'              => 'required|string',
      'pa'              => 'required|string',
      'dr'              => 'required|string',
      'cbd_casino'      => 'required|string',
      'language'        => 'required|string',
      'links'           => 'nullable|boolean',
      'monthly_traffic' => 'nullable|integer',
      'country'         => 'required|string',
      'buyer_price'     => 'required|string',
      'reseller_price'  => 'required|string',
      'last_active'     => 'required|date',
      'status'          => 'required|boolean',
    ];
  }

  public function authorize()
  {
    return true;
  }
}
