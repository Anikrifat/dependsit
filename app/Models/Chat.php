<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
  use HasFactory;

  protected $guarded = [];

  public function Sender(): \Illuminate\Database\Eloquent\Relations\BelongsTo
  {
    return $this->belongsTo(User::class, 'sender_id');
  }

//  public function Receiver(): \Illuminate\Database\Eloquent\Relations\BelongsTo
//  {
//    return $this->belongsTo(User::class, 'receiver_id');
//  }
}
