<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class GuestOrder extends Model
{
  const STATUS = [
    'processing' => 0,
    'delivered' => 1,
    'canceled' => 2,
  ];

  protected $guarded = [];
  protected $with = ['package'];

  public function package(): BelongsTo
  {
    return $this->belongsTo(Package::class);
  }

  public function user(): BelongsTo
  {
    return $this->belongsTo(User::class);
  }

  public function getFullNameAttribute()
  {
    return $this->user->name ?? $this->first_name . ' ' . $this->last_name;
  }

  public function getEmailAttribute($value)
  {
    return $this->user->email ?? $value;
  }

  public function getMobileAttribute($value)
  {
    return $this->user->phone ?? $value;
  }
}
