<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Notifications\Notifiable;

class Invoice extends Model
{
  use HasFactory;
  use Notifiable;

  protected $with = ['Items'];

  protected $fillable = [
    'bill_to',
    'recipient_first_name',
    'recipient_last_name',
    'recipient_mobile',
    'recipient_company',
    'sub_total',
    'discount',
    'grand_total',
    'note_to_recipient',
    'terms_and_conditions',
    'paypal_token',
    'paypal_payer_email',
    'paypal_payer_id',
    'remember_token',
    'payment_status',
    'status',
  ];

  public function Items(): HasMany
  {
    return $this->hasMany(InvoiceItem::class);
  }
}
