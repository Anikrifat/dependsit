<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class InvoiceItem extends Model
{
  use HasFactory;

  protected $fillable = [
    'invoice_id',
    'name',
    'price',
    'quantity',
    'description',
  ];

  public function Invoice(): BelongsTo
  {
    return $this->belongsTo(Invoice::class);
  }
}
