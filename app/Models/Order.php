<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Order extends Model
{
  use HasFactory;

  protected $guarded = [];

  public function Posts(): BelongsToMany
  {
    return $this->belongsToMany(Post::class)->withPivot('price');
  }

  public function Customer(): BelongsTo
  {
    return $this->belongsTo(User::class, 'customer_id');
  }
}
