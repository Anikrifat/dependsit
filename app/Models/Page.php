<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Page extends Model implements HasMedia
{
  use InteractsWithMedia;

  const TYPE = [
    'default' => 0,
    'seo' => 1,
    'development' => 2,
    'marketing' => 3
  ];

  protected $guarded = [];
  protected $with = ['packages'];
  protected $casts = [
    'properties' => 'array'
  ];

  public function registerMediaCollections(): void
  {
    $this->addMediaCollection('icon')->singleFile();
    $this->addMediaCollection('banner')->singleFile();
    $this->addMediaCollection('gallery')->onlyKeepLatest(5);
    $this->addMediaCollection('tabs')->onlyKeepLatest(4);
  }

  public function scopeDynamic($query)
  {
    return $query->where('build_in', FALSE);
  }

  public function packages(): HasMany
  {
    return $this->hasMany(Package::class)->orderBy('order');
  }

  public function getProperties($key)
  {
    if (is_array($this->properties) && array_key_exists($key, $this->properties))
      return $this->properties[$key];
    return null;
  }
}
