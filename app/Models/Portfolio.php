<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Portfolio extends Model implements HasMedia
{
  use InteractsWithMedia;

  const TYPE = [
    'seo' => 0,
    'development' => 1,
    'marketing' => 2
  ];

  protected $guarded = [];

  public function registerMediaCollections(): void
  {
    $this->addMediaCollection('portfolio')->singleFile();
  }
}
