<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Post extends Model
{
  use HasFactory;

  protected $guarded = [];

  public function Tags(): BelongsToMany
  {
    return $this->belongsToMany(Tag::class);
  }

  public function Orders(): BelongsToMany
  {
    return $this->belongsToMany(Order::class)->withPivot('price');
  }

  public function Category(): BelongsTo
  {
    return $this->belongsTo(Category::class);
  }

  public function SubCategory(): BelongsTo
  {
    return $this->belongsTo(Subcategory::class);
  }

  public function addedBY(): BelongsTo
  {
    return $this->belongsTo(User::class, 'added_by', 'id');
  }
}
