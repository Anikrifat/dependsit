<?php

namespace App\Models;

use App\Models\Post;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Subcategory extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function Posts()
    {
      return $this->hasMany(Post::class);
    }

    public function Category()
    {
      return $this->belongsTo(Category::class);
    }
}
