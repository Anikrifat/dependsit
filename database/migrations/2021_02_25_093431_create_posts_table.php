<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->foreignId('added_by')->constrained('users')->onDelete('cascade');
            $table->foreignId('category_id')->constrained()->onDelete('cascade');
            $table->foreignId('subcategory_id')->nullable()->constrained()->onDelete('cascade');
            $table->string('url');
            $table->string('da')->nullable();
            $table->string('pa')->nullable();
            $table->string('dr')->nullable();
            $table->string('cbd_casino')->nullable();
            $table->string('language')->nullable();
            $table->boolean('links')->default(false)->comment('0:nofollow, 1:dofollow');
            $table->string('monthly_traffic')->nullable();
            $table->string('country')->nullable();
            $table->string('buyer_price')->nullable();
            $table->string('reseller_price')->nullable();
            $table->string('ads_from')->nullable();
            $table->date('last_active')->nullable();
            $table->boolean('status')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
