<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSlidersTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('sliders', function (Blueprint $table) {
      $table->id();
//      $table->string('label')->nullable();
//      $table->string('title')->nullable();
//      $table->text('description')->nullable();
//      $table->string('list_text_one')->nullable();
//      $table->string('list_text_two')->nullable();
//      $table->string('list_text_three')->nullable();
//      $table->string('button_text')->nullable();
//      $table->string('button_url')->nullable();
//      $table->integer('ordering')->default(0);
      $table->boolean('status')->default(true);
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('sliders');
  }
}
