<?php

use App\Models\Page;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePagesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('pages', function (Blueprint $table) {
      $table->id();

      $table->string('name');
      $table->string('slug')->unique();
      $table->string('heading')->nullable();
      $table->string('sub_heading')->nullable();
      $table->text('description')->nullable();
      $table->boolean('status')->default(true);
      $table->boolean('build_in')->default(false);

      $table->string('package_heading')->nullable();
      $table->string('package_sub_heading')->nullable();

      $table->tinyInteger('type')->default(Page::TYPE['default'])->comment(json_encode(Page::TYPE));

      $table->json('properties')->nullable();

      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('pages');
  }
}
