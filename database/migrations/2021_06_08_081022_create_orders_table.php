<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->foreignId('customer_id')->constrained('users')->cascadeOnDelete();
            $table->string('document')->nullable();
            $table->string('your_niche')->nullable();
            $table->string('article_topic')->nullable();
            $table->text('anchor_keyword_note')->nullable();
            $table->string('website_link')->nullable();
            $table->text('order_requirement')->nullable();
            $table->text('special_requirement')->nullable();
            $table->boolean('with_article')->default(0)->comment('0:without_article, 1:with_article');
            $table->integer('extra_cost')->nullable();
            $table->tinyInteger('paid_status')->default(0)->comment('0:unpaid, 1:paid');
            $table->tinyInteger('status')->default(0)->comment('0:processing, 1:delivered, 2:cancel');
            $table->string('total_price')->nullable();
            $table->string('email')->nullable();
            $table->text('message')->nullable();
            $table->string('paypal_token')->nullable();
            $table->string('paypal_payer_email')->nullable();
            $table->string('paypal_payer_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
