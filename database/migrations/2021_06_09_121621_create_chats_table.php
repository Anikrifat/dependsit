<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChatsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('chats', function (Blueprint $table) {
      $table->id();
      $table->foreignId('sender_id')->constrained('users')->cascadeOnDelete();
      $table->foreignId('receiver_id')->constrained('users')->cascadeOnDelete();
      $table->string('message');
      $table->string('document')->nullable();
      $table->boolean('status')->default(0)->comment('0:unseen, 1:seen');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('chats');
  }
}
