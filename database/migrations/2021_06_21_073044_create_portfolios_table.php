<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePortfoliosTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('portfolios', function (Blueprint $table) {
      $table->id();
      $table->string('name');
      $table->string('url')->nullable();
      $table->tinyInteger('type')->default(0)->comment(json_encode(\App\Models\Portfolio::TYPE));
      $table->boolean('status')->default(true);
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('portfolios');
  }
}
