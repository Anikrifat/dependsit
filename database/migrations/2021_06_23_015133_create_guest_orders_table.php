<?php

use App\Models\GuestOrder;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGuestOrdersTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('guest_orders', function (Blueprint $table) {
      $table->id();
      $table->foreignId('user_id')->nullable()->constrained();
      $table->foreignId('package_id')->constrained();
      $table->string('first_name')->nullable();
      $table->string('last_name')->nullable();
      $table->string('email')->nullable();
      $table->string('mobile')->nullable();
      $table->text('message');
      $table->float('amount');
      $table->tinyInteger('status')->default(GuestOrder::STATUS['processing'])->comment(json_encode(GuestOrder::STATUS));
      $table->string('paypal_token')->nullable();
      $table->string('paypal_payer_email')->nullable();
      $table->string('paypal_payer_id')->nullable();
      $table->boolean('payment_status')->default(false);
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('guest_orders');
  }
}
