<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->id();
            $table->string('bill_to')->nullable();
            $table->string('recipient_first_name')->nullable();
            $table->string('recipient_last_name')->nullable();
            $table->string('recipient_mobile')->nullable();
            $table->string('recipient_company')->nullable();
            $table->decimal('sub_total')->nullable();
            $table->integer('discount')->nullable();
            $table->decimal('grand_total')->nullable();
            $table->text('note_to_recipient')->nullable();
            $table->text('terms_and_conditions')->nullable();
            $table->string('paypal_token')->nullable();
            $table->string('paypal_payer_email')->nullable();
            $table->string('paypal_payer_id')->nullable();
            $table->rememberToken();
            $table->boolean('payment_status')->default(0);
            $table->boolean('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
