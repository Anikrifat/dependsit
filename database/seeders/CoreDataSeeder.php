<?php

namespace Database\Seeders;

use App\Models\CompanySetting;
use App\Models\EcommerceSetting;
use App\Models\Page;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class CoreDataSeeder extends Seeder
{
  public function run()
  {
    $this->createDefaultSettings();
    $this->createDefaultRolePermissions();
    $this->createDefaultUsers();
    $this->createDefaultPages();
  }

  private function createDefaultSettings()
  {
    CompanySetting::create([
      'name' => 'RabbiITFirm',
      'paypal_mode' => 'sandbox',
      'paypal_client_id' => 'Ad_i93JAPGw-FT4gHEWLZ-t7oflAJmvyi32Q82LG-ARZn0b05wLp-Ov_60tbm_twrs6Dz3o-FKmuylOC',
      'paypal_client_secret' => 'EEkpig_40EKQlg00UQgxydDMHj8GhyqLS48NGe1RQt0cAtrFpqviz_SUNgqiMmBhIaTtiaZuMXi4Myze',
      'invoice_note_to_recipient' => 'Thank you for Guest Posting Service Order',
      'invoice_terms_and_conditions' => 'Our Guest Post Service is Online Virtual Service. No Refund Provided. Thank You',
    ]);
  }

  private function createDefaultUsers()
  {
    User::create([
      'name' => 'admin',
      'email' => 'admin@gmail.com',
      'email_verified_at' => now(),
      'password' => Hash::make('123123123'),
      'remember_token' => Str::random(10),
    ])->assignRole('Admin');
  }

  private function createDefaultRolePermissions()
  {

    $permissions = [
      ['name' => 'admin'],

      ['name' => 'user.all'],
      ['name' => 'user.add'],
      ['name' => 'user.edit'],
      ['name' => 'user.view'],
      ['name' => 'user.delete'],

      ['name' => 'tag.all'],
      ['name' => 'tag.add'],
      ['name' => 'tag.edit'],
      ['name' => 'tag.view'],
      ['name' => 'tag.delete'],

      ['name' => 'category.all'],
      ['name' => 'category.add'],
      ['name' => 'category.edit'],
      ['name' => 'category.view'],
      ['name' => 'category.delete'],

      ['name' => 'subcategory.all'],
      ['name' => 'subcategory.add'],
      ['name' => 'subcategory.edit'],
      ['name' => 'subcategory.view'],
      ['name' => 'subcategory.delete'],

      ['name' => 'post.all'],
      ['name' => 'post.add'],
      ['name' => 'post.edit'],
      ['name' => 'post.view'],
      ['name' => 'post.delete'],

      ['name' => 'order.all'],
      ['name' => 'order.add'],
      ['name' => 'order.edit'],
      ['name' => 'order.view'],
      ['name' => 'order.delete'],

      ['name' => 'invoice.all'],
      ['name' => 'invoice.add'],
      ['name' => 'invoice.edit'],
      ['name' => 'invoice.view'],
      ['name' => 'invoice.delete'],

      ['name' => 'settings.all'],
      ['name' => 'settings.add'],
      ['name' => 'settings.edit'],
      ['name' => 'settings.view'],
      ['name' => 'settings.delete'],

      ['name' => 'feature.all'],
      ['name' => 'feature.add'],
      ['name' => 'feature.edit'],
      ['name' => 'feature.view'],
      ['name' => 'feature.delete'],
    ];

    foreach ($permissions as $permission) {
      Permission::create([
        'name' => $permission["name"]
      ]);
    };

    Role::create([
      'name' => 'Admin'
    ])->givePermissionTo('admin');
    Role::create([
      'name' => 'Buyer'
    ]);
    Role::create([
      'name' => 'Reseller'
    ]);
  }

  private function createDefaultPages()
  {
    $datas = [
      [
        'slug' => 'home',
        'name' => 'Home',
        'heading' => '1ST PAGE ON GOOGLE TO DRIVE TRAFFIC AND SALES',
        'sub_heading' => 'MEET THE SEO AGENCY THAT BRINGS YOU',
        'description' => 'As an SEO service provider in Dhaka, Bangladesh we know the importance of ranking in the Google first page to drive quality traffic for increasing your leads and sales. Check below how our SEO expert in Bangladesh implement best practice for your website ',
        'build_in' => true
      ], [
        'slug' => 'about-us',
        'name' => 'About Us',
        'heading' => 'About Us',
        'description' => 'Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in. This is Photoshop\'s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem',
        'build_in' => true
      ], [
        'slug' => 'portfolio',
        'name' => 'Portfolio',
        'heading' => 'Portfolio',
        'build_in' => true
      ], [
        'slug' => 'clients',
        'name' => 'Clients',
        'heading' => 'Our Awesome Clients',
        'description' => 'We help our clients with Website Design, Software Development, Mobile Apps, Digital Marketing, Graphics Design, Social Media, Video Production, & Consultancy service to conquer your digital landscape and outrank your competitor. Contact us today to learn how SEO Audit Agency can help you to grow your online business.',
        'build_in' => true
      ], [
        'slug' => 'seo',
        'name' => 'SEO',
        'heading' => 'How We Rank Top of The Search Results in 2021',
        'description' => '2021 is all about making a detailed SEO strategy to improve search rankings to capture more organic traffic for the website. We are the SEO service provider company in Bangladesh offering industry-leading SEO services all over the world. Our SEO Services will help you to achieve high-ranking placement in search results. SEO is much more effective than conventional advertising. We help you to increase your traffic, leads, and sales through our white label SEO services.',
        'build_in' => true,
        'properties' => [
          "seo_heading" => "What we do everyday for our clients",
          "seo_description" => "We take the hard work out of your hands and provide you with everything needed to outsource your SEO and brand management efforts effectively. Our advanced brand building, SEO optimization, and link building techniques ensure that every business that works with us is given the best opportunity to dominate their online space."
        ]
      ], [
        'slug' => 'contact-us',
        'name' => 'Contact Us',
        'build_in' => true
      ]
    ];
    foreach ($datas as $data) {
      Page::create($data);
    }
  }
}
