<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Page;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class DemoDataSeeder extends Seeder
{
  public function run(): void
  {
    $this->createDemoUsers();
    $this->createDemoPages();
    $this->createDemoPackages();

    Category::factory(10)->create();
  }

  private function createDemoUsers()
  {
    User::create([
      'name' => 'Buyer',
      'email' => 'buyer@gmail.com',
      'email_verified_at' => now(),
      'phone' => '12312312312',
      'country' => 'Bangladesh',
      'password' => Hash::make('123123123'),
      'remember_token' => Str::random(10),
    ])->assignRole('Buyer');

    User::create([
      'name' => 'Reseller',
      'email' => 'reseller@gmail.com',
      'email_verified_at' => now(),
      'phone' => '12312312312',
      'country' => 'Bangladesh',
      'password' => Hash::make('123123123'),
      'remember_token' => Str::random(10),
    ])->assignRole('Reseller');

//    User::factory(10)->create();
  }


  private function createDemoPages()
  {
    $datas = [
      [
        'slug' => 'web-design-development',
        'name' => 'Web Design Development',
        'heading' => 'Web Design',
        'sub_heading' => 'We offer a wide range of services to reach your targeted audience by sharing your valuable information and focusing on retaining your customers.',
        'description' => 'At Rabbiitfirm LTD, a team of excellence web Design do offshore web Design to fulfill job provider planning and dream, we Design and develop application and control leading-edge web sites and e-business services that interprets small to large effects for an organization in the virtual world. Rabbiitfirm, a leading promising IT firm based in Bangladesh, allows customers according to serve theirs clients higher through dependable techniques or that performs an essential position into the advancement concerning our customer’s organization.
        Rabbiitfirm is outfitted including the armory of its exceptionally advanced technologies specifically tailor-made for step to step website improvement services.
        We supply professional website or web applications according to most over the main countries around the world as consists of Australia, Canada, India, UK and USA.
        We provide a huge organized proficient team about offshore website Design and improvement functions. We do planning, Design, Design and finally implement for the valued customer – especially website Designing, CMS based any kind of Design, word press customization or even own customized CMS.',
        'build_in' => false,
        'type' => Page::TYPE['development']
      ], [
        'slug' => 'web-development',
        'name' => 'Web Development',
        'build_in' => false,
        'type' => Page::TYPE['development']
      ], [
        'slug' => 'app-development',
        'name' => 'App Development',
        'build_in' => false,
        'type' => Page::TYPE['development']
      ], [
        'slug' => 'guest-post',
        'name' => 'Guest Post',
        'heading' => 'What is Guest Post',
        'sub_heading' => 'Crafting links on relevant & high traffic websites with shareable content.',
        'description' => '2021 is all about making a detailed SEO strategy to improve search rankings to capture more organic traffic for the website. We are the SEO service provider company in Bangladesh offering industry-leading SEO services all over the world. Our SEO Services will help you to achieve high-ranking placement in search results. SEO is much more effective than conventional advertising. We help you to increase your traffic, leads, and sales through our white label SEO services.',
        'build_in' => false,
        'type' => Page::TYPE['seo']
      ], [
        'slug' => 'complete-seo',
        'name' => 'Complete SEO',
        'heading' => 'What is Complete SEO',
        'sub_heading' => 'Building links on the most powerful, aged sites through existing conten',
        'description' => '2021 is all about making a detailed SEO strategy to improve search rankings to capture more organic traffic for the website. We are the SEO service provider company in Bangladesh offering industry-leading SEO services all over the world. Our SEO Services will help you to achieve high-ranking placement in search results. SEO is much more effective than conventional advertising. We help you to increase your traffic, leads, and sales through our white label SEO services.',
        'build_in' => false,
        'type' => Page::TYPE['seo']
      ], [
        'slug' => 'on-page-seo',
        'name' => 'On Page SEO',
        'heading' => 'What is On Page SEO',
        'sub_heading' => 'Outsource your SEO to us so you don\'t miss out on future opportunities',
        'description' => '2021 is all about making a detailed SEO strategy to improve search rankings to capture more organic traffic for the website. We are the SEO service provider company in Bangladesh offering industry-leading SEO services all over the world. Our SEO Services will help you to achieve high-ranking placement in search results. SEO is much more effective than conventional advertising. We help you to increase your traffic, leads, and sales through our white label SEO services.',
        'build_in' => false,
        'type' => Page::TYPE['seo']
      ], [
        'slug' => 'off-page-seo',
        'name' => 'Off Page SEO',
        'heading' => 'What is Off Page SEO',
        'sub_heading' => 'Dominate your local area and niche through our local SEO packages',
        'description' => '2021 is all about making a detailed SEO strategy to improve search rankings to capture more organic traffic for the website. We are the SEO service provider company in Bangladesh offering industry-leading SEO services all over the world. Our SEO Services will help you to achieve high-ranking placement in search results. SEO is much more effective than conventional advertising. We help you to increase your traffic, leads, and sales through our white label SEO services.',
        'build_in' => false,
        'type' => Page::TYPE['seo']
      ]
    ];
    foreach ($datas as $data) {
      Page::insert($data);
    }
  }

  private function createDemoPackages()
  {
    $page = Page::where('slug', 'guest-post')->first();
    $page->package_heading = "Pricing Offer that suit Your Need";
    $page->package_sub_heading = "Great service with best options";
    $page->save();

    $page->packages()->create([
      'heading' => 'DA50+',
      'sub_heading' => '10 GUEST POSTS INCLUDED',
      'price' => 25.90,
      'features' => [
        'DA50+ (10 Websites)', 'Publish Your Articles', '2 Do Follow Contextual links Per Post', 'Links on Real Traffic Website',
        '600+ Word Articles each (+200$)', 'Normal Delivery Time 2 Weeks', 'CASINO & CBD Allowed (+400$)'
      ]
    ]);
    $page->packages()->create([
      'heading' => 'DA60+',
      'sub_heading' => '10 GUEST POSTS INCLUDED',
      'price' => 45.90,
      'features' => [
        'DA50+ (10 Websites)', 'Publish Your Articles', '2 Do Follow Contextual links Per Post', 'Links on Real Traffic Website',
        '600+ Word Articles each (+200$)', 'Normal Delivery Time 2 Weeks', 'CASINO & CBD Allowed (+400$)'
      ]
    ]);
    $page->packages()->create([
      'heading' => 'DA70+',
      'sub_heading' => '10 GUEST POSTS INCLUDED',
      'price' => 65.90,
      'features' => [
        'DA50+ (10 Websites)', 'Publish Your Articles', '2 Do Follow Contextual links Per Post', 'Links on Real Traffic Website',
        '600+ Word Articles each (+200$)', 'Normal Delivery Time 2 Weeks', 'CASINO & CBD Allowed (+400$)'
      ]
    ]);
  }
}
