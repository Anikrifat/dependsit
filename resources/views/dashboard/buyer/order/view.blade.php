@extends('layouts.dashboard')

@section('content')

  @if(session()->has('success'))
    <div class="alert alert-success">
      {{session()->get('success')}}
    </div>
  @elseif(session()->has('error'))
    <div class="alert alert-danger">
      {{session()->get('error')}}
    </div>
  @endif

  <section>
    <div class="card card-custom mb-5">
      <div class="card-body">
        <div class="d-flex justify-content-between">
          <h2 class="mb-10 mr-6">Order Number : {{ $order->id }}</h2>
          @canany(['admin', 'order.all', 'order.edit'])
          <form action="{{ route('changeOrderStatus', $order->id) }}" method="post" id="order_status_change_form">
            @csrf
            <div class="form-group d-flex align-items-center">
              <label for="order_status" class="text-nowrap mr-2">Order Status</label>
              <select class="form-control" name="order_status" id="order_status" onchange="document.getElementById('order_status_change_form').submit()">
                <option value="0" {{ $order->status == '0' ? 'selected' : null }}>Processing</option>
                <option value="1" {{ $order->status == '1' ? 'selected' : null }}>Delivered</option>
                <option value="2" {{ $order->status == '2' ? 'selected' : null }}>Canceled</option>
              </select>
            </div>
          </form>
          @endcanany
        </div>
        <div class="row">
          <div class="col-md-9">
            <p><b>Niche :</b> {{ $order->your_niche }}</p>
            <p><b>Article Topic :</b> {{ $order->article_topic }}</p>
            <p><b>Anchor/Keyword/Note :</b> {{ $order->anchor_keyword_note }}</p>
            <p><b>Order Requirements :</b> {{ $order->order_requirement }}</p>
            <p><b>Special Requirements :</b> {{ $order->order_requirement }}</p>
            <p><b>Document</b></p>
            <div>
              <img src="{{ setImage($order->image) }}" alt="document" style="height: 150px">
            </div>
          </div>
          <div class="col-md-3">
            <p class="bg-light-primary p-2 d-inline-block" style="border-radius: 6px"><b>Order Status :</b> <span>@if($order->status == 0) Processing @elseif($order->status == 1) Delivered @elseif($order->status == 2)
                  Canceled @endif</span></p>
            <br>
            <p><b>Article :</b> @if($order->with_article) With Article @else Without Article @endif</p>
            <p><b>Extra Cost :</b> {{ $order->extra_cost }}$</p>
            <p><b>Total Amount :</b> {{ $order->total_price }}$</p>
            <p><b>Paid Status :</b> @if($order->paid_status) <span class="badge badge-success">Paid</span> @else <span class="badge badge-danger">Unpaid</span> @endif</p>
          </div>
        </div>
      </div>
    </div>

    @foreach($order->Posts as $post)
      <div class="card card-custom mb-4">
        <div class="card-body">
          <div class="row">
            <div class="col-md-3">
              <p>{{ $post->url }}</p>
              <p>
                @foreach($post->Tags as $tag)
                  <span class="badge badge-light">{{ $tag->name }}</span>
                @endforeach
              </p>
              <p class="mb-0">Last active on: {{ $post->last_active }}</p>
            </div>
            <div class="col-md-3">
              <p><span><b>DA:</b> {{ $post->da }}</span>;&nbsp;<span><b>PA:</b> {{ $post->pa }}</span>;&nbsp;<span><b>DR:</b> {{ $post->dr }}</span></p>
              <p><b>Language:</b> {{ $post->language }}</p>
              <p class="mb-0"><b>Links:</b> {{ $post->links ? 'Dofollow' : 'Nofollow' }}</p>
            </div>
            <div class="col-md-3">
              <p><b>Monthly traffic:</b> {{ $post->monthly_traffic }}</p>
              <p><b>Country:</b> {{ $post->country }}</p>
              <p class="mb-0"><b>Price:</b> {{ $post->buyer_price }}$</p>
            </div>
            <div class="col-md-3">
              @if($post->ads_from)
                <p><b>Ads:</b> {{ $post->ads_from }}</p>
              @endif
              <p class="mb-0"><b>CBD, Casino:</b> {{ $post->cbd_casino }}$</p>
            </div>
          </div>
        </div>
      </div>
    @endforeach
  </section>


@endsection
