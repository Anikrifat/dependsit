@extends('layouts.dashboard')

@section('content')

  <section class="mb-4">
    <div class="card card-custom">
      <div class="card-body py-4">
        <div class="d-flex justify-content-between">
          <div class="form-group mb-0">
            <div class="d-flex align-items-center">
              <form action="{{ route('guest_post.index') }}" method="GET" id="filter_form">
                <label for="category" class="mr-2">Category</label>
                <select name="category" id="category" class="form-control select2" onchange="document.getElementById('filter_form').submit()">
                  <option value="all">All Websites</option>
                  @foreach(\App\Models\Category::whereStatus(1)->get() as $category)
                    <option value="{{ $category->id }}" {{ request()->get('category') == $category->id ? 'selected' : null }}>{{ $category->name }}</option>
                  @endforeach
                </select>

                <label for="follow" class="mr-2 ml-4">Follow</label>
                <select name="follow" id="follow" class="form-control select2" onchange="document.getElementById('filter_form').submit()">
                  <option value="all" {{ request()->get('follow') == 'all' ? 'selected' : null }}>All</option>
                  <option value="1" {{ request()->get('follow') == '1' ? 'selected' : null }}>Dofollow</option>
                  <option value="0" {{ request()->get('follow') == '0' ? 'selected' : null }}>Nofollow</option>
                </select>
              </form>
            </div>
          </div>
          <div>

          </div>
        </div>
      </div>
    </div>
  </section>

  <section>
    @foreach($posts as $post)
      <div class="card card-custom mb-4">
        <div class="card-body">
          <div class="row">
            <div class="col-md-10">
              <div class="row">
                <div class="col-md-4">
                  <p>{{ $post->url }}</p>
                  <p>
                    @foreach($post->Tags as $tag)
                      <span class="badge badge-light">{{ $tag->name }}</span>
                    @endforeach
                  </p>
                  <p class="mb-0">Last active on: {{ $post->last_active }}</p>
                </div>
                <div class="col-md-4">
                  <p><span><b>DA:</b> {{ $post->da }}</span>;&nbsp;<span><b>PA:</b> {{ $post->pa }}</span>;&nbsp;<span><b>DR:</b> {{ $post->dr }}</span></p>
                  <p><b>Language:</b> {{ $post->language }}</p>
                  <p class="mb-0"><b>Links:</b> {{ $post->links ? 'Dofollow' : 'Nofollow' }}</p>
                </div>
                <div class="col-md-4">
                  <p><b>Monthly traffic:</b> {{ $post->monthly_traffic }}</p>
                  <p><b>Country:</b> {{ $post->country }}</p>
                  <p class="mb-0"><b>Price:</b> {{ $post->buyer_price }}$</p>
                </div>
              </div>
            </div>
            <div class="col-md-2">
              <div class="h-100 d-flex align-items-center">
                @if(auth()->user()->hasRole('Buyer'))
                  <a href="{{ route('guest_post.view', $post->id) }}" class="btn btn-success btn-sm text-nowrap">Buy Post</a>
                @else
                  <button onclick="Livewire.emitTo('dashboard.shopping-cart', 'add-to-cart', {{ $post->id }})" class="btn btn-success btn-sm text-nowrap">Add to Cart</button>
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>
    @endforeach
    {{
    $posts->appends([
    'category' => (request()->get('category') ?? 'all'),
    'follow' => (request()->get('follow') ?? 'all'),
    ])->links()
    }}
  </section>

@endsection
