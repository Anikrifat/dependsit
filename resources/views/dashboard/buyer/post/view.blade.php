@extends('layouts.dashboard')

@section('content')
  <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.8.2/dist/alpine.min.js" defer></script>

  @if(session()->has('success'))
    <div class="alert alert-success">
      {{session()->get('success')}}
    </div>
  @elseif(session()->has('error'))
    <div class="alert alert-danger">
      {{session()->get('error')}}
    </div>
  @endif

  <section>
    <div class="card card-custom mb-4">
      <div class="card-body">
        <div class="row">
          <div class="col-md-3">
            <p>{{ $post->url }}</p>
            <p>
              @foreach($post->Tags as $tag)
                <span class="badge badge-light">{{ $tag->name }}</span>
              @endforeach
            </p>
            <p class="mb-0">Last active on: {{ $post->last_active }}</p>
          </div>
          <div class="col-md-3">
            <p><span><b>DA:</b> {{ $post->da }}</span>;&nbsp;<span><b>PA:</b> {{ $post->pa }}</span>;&nbsp;<span><b>DR:</b> {{ $post->dr }}</span></p>
            <p><b>Language:</b> {{ $post->language }}</p>
            <p class="mb-0"><b>Links:</b> {{ $post->links ? 'Dofollow' : 'Nofollow' }}</p>
          </div>
          <div class="col-md-3">
            <p><b>Monthly traffic:</b> {{ $post->monthly_traffic }}</p>
            <p><b>Country:</b> {{ $post->country }}</p>
            <p class="mb-0"><b>Price:</b> {{ $post->buyer_price }}$</p>
          </div>
          <div class="col-md-3">
            @if($post->ads_from)
              <p><b>Ads:</b> {{ $post->ads_from }}</p>
            @endif
            <p class="mb-0"><b>CBD, Casino:</b> {{ $post->cbd_casino }}$</p>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section x-data="{ tab: 'with_post', amount: {{ $post->buyer_price }} }">
    <button class="btn mb-4" value="with_post" :class="{ 'btn-primary': tab === 'with_post' }" @click="tab = 'with_post'">With Post</button>
    <button class="btn mb-4" value="without_post" :class="{ 'btn-primary': tab === 'without_post' }" @click="tab = 'without_post'">Without Post</button>

    <div>
      <div class="card card-custom">
        <div class="card-body">

          @if ($errors->any())
            <div class="alert alert-danger">
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
          @endif

          <form action="{{ route('order.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="with_article" :value="tab === 'with_post' ? 1 : 0">
            <input type="hidden" name="posts[]" value="{{ $post->id }}">
            <div class="form-group" x-show="tab === 'with_post'">
              <label for="document">Document</label>
              <input type="file" name="document" id="document" class="form-control" :disabled="tab === 'without_post'">
            </div>
            <div class="form-group">
              <label for="order_requirement">Order Requirements</label>
              <textarea name="order_requirement" class="form-control" id="order_requirement" cols="30" rows="3" minlength="500"></textarea>
            </div>
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="your_niche">Your Niche</label>
                  <select name="your_niche[]" id="your_niche" class="form-control select2-withTag" multiple></select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="article_topic">Article Topic</label>
                  <input type="text" name="article_topic" id="article_topic" class="form-control">
                </div>
              </div>
            </div>
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="anchor_keyword_note">Anchor/Keyword/Note</label>
                  <input type="text" name="anchor_keyword_note" id="anchor_keyword_note" class="form-control" maxlength="500">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="website_link">Your Website Link</label>
                  <input type="text" name="website_link" id="website_link" class="form-control">
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="special_requirement">Special Requirements</label>
              <textarea name="special_requirement" class="form-control" id="special_requirement" cols="30" rows="6" minlength="500"></textarea>
            </div>
            <div class="bg-light p-5 mb-7" x-show="tab === 'with_post'">
              <h4>Task price: 10$</h4>
              <p class="text-black-50 mt-5 mb-0">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusantium alias beatae consequuntur, corporis deleniti dolor eaque esse eum in libero nam necessitatibus perferendis quasi quod quos
                repudiandae veritatis voluptatem?
              </p>
            </div>
            <div class="form-group">
              <!-- Modal-->
              <button type="button" class="btn btn-primary mx-auto" data-toggle="modal" data-target="#order_user_info">
                Next Step
              </button>

              <div class="modal fade" id="order_user_info" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Other Information</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                      </button>
                    </div>
                    <div class="modal-body">
                      <h2 class="text-black-50 mb-4">Amount <span x-text="tab === 'with_post' ? amount + 10 : amount"></span>$</h2>
                      <div class="form-group mb-4">
                        <label for="name">Name</label>
                        <input type="text" name="name" id="name" class="form-control" value="{{ auth()->user()->name }}" disabled>
                      </div>
                      <div class="form-group mb-4">
                        <label for="email">Email</label>
                        <input type="text" name="email" id="email" class="form-control" value="{{ auth()->user()->email }}">
                      </div>
                      <div class="form-group mb-0">
                        <label for="message">Message</label>
                        <input type="text" name="message" id="message" class="form-control">
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
                      <button type="submit" class="btn btn-primary font-weight-bold" name="btn_type" value="pay_now">Complete Order</button>
                      @if(auth()->user()->hasRole('Reseller'))
                        <button type="submit" class="btn btn-primary font-weight-bold" name="btn_type" value="pay_later">Pay Later</button>
                      @endif
                    </div>
                  </div>
                </div>
              </div>
              <!-- Modal-->
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>

@endsection
