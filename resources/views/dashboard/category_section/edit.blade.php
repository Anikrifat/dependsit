@extends('layouts.dashboard')
@section('content')

  <div class="card card-custom card-sticky" id="kt_page_sticky_card">
    @if(session()->has('success'))
      <div class="alert alert-success">
        {{session()->get('success')}}
      </div>
    @elseif(session()->has('error'))
      <div class="alert alert-danger">
        {{session()->get('error')}}
      </div>
    @endif
    <div class="card-header">
      <div class="card-title">
        <h3 class="card-label">
          Update Category Section
        </h3>
      </div>
      <div class="card-toolbar">
        <div class="btn-group">
          <button type="submit" form="kt_form" class="btn btn-primary font-weight-bolder submit">
            <i class="ki ki-check icon-sm"></i>
            Save Form
          </button>
        </div>
      </div>
    </div>

    <div class="card-body">

      <form class="form" id="kt_form" enctype="multipart/form-data" method="POST" action="{{route('categorysection.update',$cat_section->id)}}">
        @csrf
        @method('PUT')
        <div class="row">
          <div class="col-xl-2"></div>
          <div class="col-xl-8">
            <div class="form-group row">
              <label class="col-md-3 col-form-label" for="image">{{__('Cover Image')}}<span class="text-danger">*</span></label>
              <div class="col-md-9">
                <div class="image-input image-input-empty image-input-outline" id="logo" style="background-image: url('{{ asset('storage/'.$cat_section->image) }}')">
                  <div class="image-input-wrapper"></div>
                  <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
                    <i class="fa fa-pen icon-sm text-muted"></i>
                    <input type="file" name="image" accept=".png, .jpg, .jpeg"/>
                    <input type="hidden" name="profile_avatar_remove"/>
                  </label>
                  <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
                  <i class="ki ki-bold-close icon-xs text-muted"></i>
                </span>
                  <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="remove" data-toggle="tooltip" title="Remove avatar">
                  <i class="ki ki-bold-close icon-xs text-muted"></i>
                </span>
                </div>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-lg-3 col-form-label" for="name">{{__('Category Section Name')}} <span class="text-danger">*</span></label>
              <div class="col-lg-9">
                <input name="name" id="name" value="{{$cat_section->name}}" placeholder="Ex: Smith Jones" type="text" class="form-control form-control-solid  @error('name') is-invalid @enderror">
                @error('name')
                <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
                @enderror
              </div>
            </div>

            <div class="form-group row">
              <label class="col-lg-3 col-form-label" for="section_position">{{__('Section Position')}}</label>
              <div class="col-lg-9">
                <select class="form-control select2" id="section_position" name="position">
                  <option value="1" {{ $cat_section->section_position == 1 ? "selected":" " }}>Section position 1</option>
                  <option value="2" {{ $cat_section->section_position == 2 ? "selected":" " }}>Section position 2</option>
                  <option value="3" {{ $cat_section->section_position == 3 ? "selected":" " }}>Section position 3</option>
                  <option value="4" {{ $cat_section->section_position == 4 ? "selected":" " }}>Section position 4</option>
                  <option value="5" {{ $cat_section->section_position == 5 ? "selected":" " }}>Section position 5</option>
                  <option value="6" {{ $cat_section->section_position == 6 ? "selected":" " }}>Section position 6</option>
                  <option value="7" {{ $cat_section->section_position == 7 ? "selected":" " }}>Section position 7</option>
                  <option value="8" {{ $cat_section->section_position == 8 ? "selected":" " }}>Section position 8</option>
                  <option value="9" {{ $cat_section->section_position == 9 ? "selected":" " }}>Section position 9</option>
                  <option value="10" {{ $cat_section->section_position == 10 ? "selected":" " }}>Section position 10</option>
                  <option value="11" {{ $cat_section->section_position == 11 ? "selected":" " }}>Section position 11</option>
                  <option value="12" {{ $cat_section->section_position == 12 ? "selected":" " }}>Section position 12</option>
                </select>
                @error('position')
                <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
                @enderror
              </div>
            </div>

            <div class="form-group row">
              <label class="col-lg-3" for="category_id">{{__(' Section category')}}</label>
              <div class="col-lg-9">
                <select class="form-control select2" name="category_id">
                  <option value="">-- Select Section category --</option>
                  @foreach ($categories as $category)
                    <option value="{{ $category->id }}" {{ $cat_section->Category->id == $category->id ? "selected" : "" }}>{{ $category->name }}</option>
                  @endforeach
                </select>
                @error('products_id')
                <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
                @enderror
              </div>
            </div>

            <div class="form-group row">
              <label class="col-lg-3" for="status">{{__('Status')}}</label>
              <div class="col-lg-9">
                <select name="status" id="status" value="{{old('status')}}" class="custom-select form-control select2 @error('status') is-invalid @enderror">
                  <option value="1" {{ $cat_section->status == 1 ? "selected" : "" }}>Active</option>
                  <option value="0" {{ $cat_section->status == 0 ? "selected" : "" }}>Inactive</option>
                </select>
                @error('status')
                <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
                @enderror
                <span class="form-text text-muted">Please select the status.</span>
              </div>
            </div>
          </div>
      </form>
      <!--end::Form-->
    </div>
  </div>
@endsection
@push('script')
  <script>
    $('.alert-success').fadeOut('slow').delay(2000);
    var avatar5 = new KTImageInput('logo');

    avatar5.on('cancel', function (imageInput) {
      swal.fire({
        title: 'Image successfully changed !',
        type: 'success',
        buttonsStyling: false,
        confirmButtonText: 'Awesome!',
        confirmButtonClass: 'btn btn-primary font-weight-bold'
      });
    });

    avatar5.on('change', function (imageInput) {
      swal.fire({
        title: 'Image successfully changed !',
        type: 'success',
        buttonsStyling: false,
        confirmButtonText: 'Awesome!',
        confirmButtonClass: 'btn btn-primary font-weight-bold'
      });
    });

    $('.select2').select2({
      placeholder: 'Select option',
    });
    $('.select2-withTag').select2({
      placeholder: 'Select option',
      tags: "true",
    });
  </script>
@endpush
