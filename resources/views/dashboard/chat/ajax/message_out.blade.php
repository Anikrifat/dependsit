@php($user_id = auth()->id())
@foreach($chats as $chat)

  @if($chat->sender_id == $user_id)
    <div class="d-flex flex-column mb-5 align-items-end">
      <div class="d-flex align-items-center">
        <div>
          <span class="text-muted font-size-sm">{{ $chat->created_at->diffForHumans() }}</span>
          <a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">You</a>
        </div>
      </div>
      <div class="mt-2 rounded p-5 bg-light-primary text-dark-50 font-weight-bold font-size-lg text-right max-w-400px">
        {{ $chat->message }}
      </div>
    </div>
  @else
    <div class="d-flex flex-column mb-5 align-items-start">
      <div class="d-flex align-items-center">
        <div>
          <a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">{{ \App\Models\User::find($chat->sender_id)->name }}</a>
          <span class="text-muted font-size-sm">{{ $chat->created_at->diffForHumans() }}</span>
        </div>
      </div>
      <div class="mt-2 rounded p-5 bg-light-success text-dark-50 font-weight-bold font-size-lg text-left max-w-400px">
        {{ $chat->message }}
      </div>
    </div>
  @endif

@endforeach
