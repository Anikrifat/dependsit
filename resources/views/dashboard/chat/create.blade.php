@extends('layouts.dashboard')

@section('content')

  <div class="container">
    <div class="d-flex flex-row">

    @if(!auth()->user()->hasRole(['Buyer', 'Reseller']))
      <!--begin::Aside-->
        <div class="flex-row-auto offcanvas-mobile w-350px w-xl-400px mr-lg-8" id="kt_chat_aside">
          <div class="card card-custom">
            <div class="card-body">
              {{--<div class="input-group input-group-solid">
                <div class="input-group-prepend">
                <span class="input-group-text">
                  <span class="svg-icon svg-icon-lg">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                      <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect x="0" y="0" width="24" height="24"/>
                        <path
                          d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z"
                          fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                        <path
                          d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z"
                          fill="#000000" fill-rule="nonzero"/>
                      </g>
                    </svg>
                  </span>
                </span>
                </div>
                <input type="text" class="form-control py-4 h-auto" placeholder="Email"/>
              </div>--}}

              <div class="mt-7 scroll scroll-pull user_list">
                <input type="hidden" name="user_id" id="chat_with_this_user" value="">

                @foreach($users as $user)
                  <label for="user_{{ $user->id }}" class="d-block cursor-pointer">
                    <input type="radio" id="user_{{ $user->id }}" name="user" value="{{ $user->id }}" class="d-none user_selected" {{ $loop->first ? 'checked' : null }}>

                    <div class="d-flex align-items-center justify-content-between mb-5 p-2 bg-light" style="border-radius: 4px">
                      <div class="d-flex align-items-center">
                        {{--<div class="symbol symbol-circle symbol-50 mr-3">
                          <img alt="Pic" src="assets/media/users/300_11.jpg"/>
                        </div>--}}
                        <div class="d-flex flex-column">
                          <a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-lg">{{ ucfirst($user->name) }}</a>
                          <span class="text-muted font-weight-bold font-size-sm">@if($user->hasRole('Buyer')) Buyer @else Reseller @endif</span>
                        </div>
                      </div>
                      <div class="d-flex flex-column align-items-end">
                        <span class="label label-sm label-success">{{ $user->Chats()->where('status', 0)->count() }}</span>
                      </div>
                    </div>
                  </label>
                  <style>
                    .user_list input[type="radio"]:checked ~ div {
                      background: #C9F7F5 !important;
                    }
                  </style>
                @endforeach

              </div>
            </div>
          </div>
        </div>
        <!--end::Aside-->
    @endif

    <!--begin::Content-->
      <div class="flex-row-fluid" id="kt_chat_content">
        <div class="card card-custom">
          <div class="card-header align-items-center px-4 py-3">
            <div class="text-center flex-grow-1">
              <div class="text-dark-75 font-weight-bold font-size-h5">Chat @if(auth()->user()->hasRole(['Buyer', 'Reseller'])) With Admin @endif</div>
            </div>
          </div>

          <div class="card-body">
            <div class="scroll scroll-pull" data-mobile-height="350">
              <div class="messages" id="messages">
                @php($user_id = auth()->id())
                @foreach($chats as $chat)

                  @if($chat->sender_id == $user_id)
                    <div class="d-flex flex-column mb-5 align-items-end">
                      <div class="d-flex align-items-center">
                        <div>
                          <span class="text-muted font-size-sm">{{ $chat->created_at->diffForHumans() }}</span>
                          <a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">You</a>
                        </div>
                      </div>
                      <div class="mt-2 rounded p-5 bg-light-primary text-dark-50 font-weight-bold font-size-lg text-right max-w-400px">
                        {{ $chat->message }}
                      </div>
                    </div>
                  @else
                    <div class="d-flex flex-column mb-5 align-items-start">
                      <div class="d-flex align-items-center">
                        <div>
                          <a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">{{ \App\Models\User::find($chat->sender_id)->name }}</a>
                          <span class="text-muted font-size-sm">{{ $chat->created_at->diffForHumans() }}</span>
                        </div>
                      </div>
                      <div class="mt-2 rounded p-5 bg-light-success text-dark-50 font-weight-bold font-size-lg text-left max-w-400px">
                        {{ $chat->message }}
                      </div>
                    </div>
                  @endif

                @endforeach
              </div>
            </div>
          </div>

          <!--begin::Footer-->
          <div class="card-footer align-items-center">
            <input type="text" class="form-control border-0 p-0" rows="2" placeholder="Type a message" id="message"/>
            <div class="d-flex align-items-center justify-content-between mt-5">
              <div class="mr-3">
                <a href="#" class="btn btn-clean btn-icon btn-md mr-1">
                  <i class="flaticon-attachment icon-lg"></i>
                </a>
              </div>
              <div>
                <button type="button" class="btn btn-primary btn-md text-uppercase font-weight-bold chat-send py-2 px-6">Send</button>
              </div>
            </div>
          </div>
          <!--end::Footer-->
        </div>
      </div>
      <!--end::Content-->
    </div>
  </div>

@endsection

@push('script')
  <script src="{{ asset('assets/dashboard/js/pages/custom/chat/chat.js') }}"></script>
  <script>
    $(document).ready(function () {
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });

      let $selected_user = $('.user_selected');

      $selected_user.each(function () {
        if ($(this).prop('checked')) {
          $('#chat_with_this_user').val($(this).val());
          chatWithUser($(this).val());
        }
      });

      $selected_user.change(function () {
        $('#chat_with_this_user').val($(this).val());
        chatWithUser($(this).val());
      });

      function chatWithUser(id) {
        $.ajax({
          method: "POST",
          url: `{{ route('admin.chat') }}`,
          data: {
            user_id: id,
          },
          dataType: "html",
          success: function (response) {
            let messages = $("#messages");
            messages.html(response);
            scrollBottom();
          },
          error: function (err) {
            console.log(err);
          }
        });
      }

      $('#message').on('keyup', function (e) {
        e.preventDefault();
        if (e.keyCode === 13) {
          let receiver_id = $('#chat_with_this_user').val();
          let message = $(this).val();
          $(this).val('');

          $.ajax({
            method: "POST",
            url: "{{ route('chat.store') }}",
            data: {
              message: message,
              receiver_id: receiver_id
            },
            dataType: "html",
            success: function (response) {
              let messages = $("#messages");
              messages.html(response);
              scrollBottom();
            },
            error: function (err) {
              console.log(err);
            }
          });
        }
      });

      scrollBottom();
      function scrollBottom() {
        $('.scroll').scrollTop(parseInt($("#messages").height()));
      }
    });

  </script>
@endpush
