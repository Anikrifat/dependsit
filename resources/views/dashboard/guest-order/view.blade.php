@extends('layouts.dashboard')

@section('content')

  @if(session()->has('success'))
    <div class="alert alert-success">
      {{session()->get('success')}}
    </div>
  @elseif(session()->has('error'))
    <div class="alert alert-danger">
      {{session()->get('error')}}
    </div>
  @endif

  <section>
    <div class="card card-custom mb-5">
      <div class="card-body">
        <div class="d-flex justify-content-between">
          <h2 class="mb-10 mr-6">Order Number : {{ $guestOrder->id }}</h2>
          @canany(['admin', 'order.all', 'order.edit'])
            <form action="{{ route('guest.orders.update-status', $guestOrder->id) }}" method="post" id="order_status_change_form">
              @csrf
              <div class="form-group d-flex align-items-center">
                <label for="order_status" class="text-nowrap mr-2">Order Status</label>
                <select class="form-control" name="order_status" id="order_status" onchange="document.getElementById('order_status_change_form').submit()">
                  <option value="0" {{ $guestOrder->status == '0' ? 'selected' : null }}>Processing</option>
                  <option value="1" {{ $guestOrder->status == '1' ? 'selected' : null }}>Delivered</option>
                  <option value="2" {{ $guestOrder->status == '2' ? 'selected' : null }}>Canceled</option>
                </select>
              </div>
            </form>
          @endcanany
        </div>
        <div class="row">
          <div class="col-md-9">
            <p>
              <b>Name :</b> {{ $guestOrder->fullName }}
            </p>
            <p>
              <b>Email :</b> {{ $guestOrder->email }}
            </p>
            <p>
              <b>Package Heading :</b> {{ $guestOrder->package->heading }}
            </p>
            <p>
              <b>Package Sub Heading :</b> {{ $guestOrder->package->sub_heading }}
            </p>
            <p>
              <b>Features :</b>
                @foreach($guestOrder->package->features as $feature)
                <br>
                {{ $feature }}
                @endforeach
            </p>
          </div>
          <div class="col-md-3">
            <p class="bg-light-primary p-2 d-inline-block" style="border-radius: 6px">
              <b>Order Status :</b>
              <span>@if($guestOrder->status == 0) Processing @elseif($guestOrder->status == 1) Delivered @elseif($guestOrder->status == 2)
                  Canceled @endif</span>
            </p>
            <br>
            <p>
              <b>Order Date :</b> {{ $guestOrder->created_at->format('d M Y') }}
            </p>
            <p>
              <b>Total Amount :</b> {{ $guestOrder->amount }}$
            </p>
            <p>
              <b>Paid Status :</b> @if($guestOrder->payment_status)
                <span class="badge badge-success">Paid</span> @else
                <span class="badge badge-danger">Unpaid</span> @endif
            </p>
          </div>
        </div>
      </div>
    </div>
  </section>


@endsection
