@extends('layouts.dashboard')

@section('content')
  @php($company = company())
  <div class="card card-custom card-sticky" id="kt_page_sticky_card">
    @if (session()->has('success'))
      <div class="alert alert-success">
        {{ session()->get('success') }}
      </div>
    @elseif(session()->has('error'))
      <div class="alert alert-danger">
        {{ session()->get('error') }}
      </div>
    @endif
    <div class="card-header">
      <div class="card-title">
        <h3 class="card-label">
          {{ $pageTitle }}
        </h3>
      </div>
      <div class="card-toolbar">
        @if (request()->routeIs('invoice.edit'))
          <div class="position-relative d-inline-block link-copy-box">
            <button onclick="selectLink({{ $invoice->id }})" type="button" class="btn btn-sm btn-light-primary font-weight-bolder mr-2">
              <i class="ki ki-copy icon-nm"></i>
              Copy
            </button>
            <input type="url" id="link-copy-input-{{ $invoice->id }}" class="link-copy-input form-control form-control-sm" value="{{ route('invoice.pay.view', [$invoice->id, $invoice->remember_token]) }}">
          </div>
        @endif
        <button type="submit" name="btn_type" value="saveAndShowPreview" form="kt_form" class="btn btn-sm btn-primary font-weight-bolder submit mr-2">Save & Preview</button>
        <div class="btn-group">
          <button type="submit" name="btn_type" value="save" form="kt_form" class="btn btn-sm btn-primary font-weight-bolder submit">
            @if (request()->routeIs('invoice.edit'))
              Send Update
            @else
              Send Invoice
            @endif
          </button>
        </div>
      </div>
    </div>

    <div class="card-body">
      @if ($errors->any())
        <div class="alert alert-danger">
          <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
      @endif

      <form action="{{ $formAction }}" method="post" id="kt_form">
        @csrf
        @if (request()->routeIs('invoice.edit'))
          @method('PUT')
        @endif

        <div x-data="ItemsData({{ $invoice->Items ?? null }})">
          <div class="row">
            <div class="col-md-6">
              <div class="my-5">
                <div class="form-group mb-2">
                  <div class="d-flex align-items-center">
                    <label for="bill_to" class="mr-2 text-nowrap">Bill To</label>
                    <input name="bill_to" id="bill_to" x-model="bill_to" class="form-control @error('bill_to') is-invalid @enderror" type="email">
                    <div class="btn btn-link" data-toggle="modal" data-target="#recipientInfo">Edit</div>
                  </div>
                  @error('bill_to')
                  <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
                <template x-if="recipient_company">
                  <p class="mb-2">Company : <span x-text="recipient_company"></span></p>
                </template>
                <template x-if="recipient_first_name">
                  <p class="mb-2">First Name : <span x-text="recipient_first_name"></span></p>
                </template>
                <template x-if="recipient_last_name">
                  <p class="mb-2">Last Name : <span x-text="recipient_last_name"></span></p>
                </template>
                <template x-if="recipient_mobile">
                  <p class="mb-2">Mobile : <span x-text="recipient_mobile"></span></p>
                </template>
              </div>
            </div>
          </div>

          {{-- Recipient Info Edit Modal --}}
          <div class="modal fade" id="recipientInfo" tabindex="-1" aria-labelledby="recipientInfoLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="recipientInfoLabel">Recipient Information</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <div class="form-row">
                    <div class="form-group col-6">
                      <label for="recipient_email">Bill To</label>
                      <input type="text" x-model="bill_to" id="recipient_email" class="form-control form-control-sm">
                    </div>
                    <div class="form-group col-6">
                      <label for="recipient_company">Company</label>
                      <input type="text" x-model="recipient_company" id="recipient_company" name="recipient_company" class="form-control form-control-sm">
                    </div>
                  </div>
                  <div class="form-row">
                    <div class="form-group col-6">
                      <label for="recipient_first_name">First Name</label>
                      <input type="text" x-model="recipient_first_name" id="recipient_first_name" name="recipient_first_name" class="form-control form-control-sm">
                    </div>
                    <div class="form-group col-6">
                      <label for="recipient_last_name">Last Name</label>
                      <input type="text" x-model="recipient_last_name" id="recipient_last_name" name="recipient_last_name" class="form-control form-control-sm">
                    </div>
                  </div>
                  <div class="form-row">
                    <div class="form-group col-6 mb-0">
                      <label for="recipient_mobile">Mobile</label>
                      <input type="text" x-model="recipient_mobile" id="recipient_mobile" name="recipient_mobile" class="form-control form-control-sm">
                    </div>
                  </div>
                </div>
                <div class="modal-footer py-3">
                  <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>

          <template x-for="(item, index) in items" :key="index">
            <div class="bg-light p-4 mb-5 rounded position-relative">
              <i class="fas fa-times-circle fa-2x cursor-pointer position-absolute top-0 right-0" style="transform: translate(50%, -50%)" @click="removeItem(index)"></i>

              <div class="form-row">
                <div class="col-md-6">
                  <div class="form-group mb-3">
                    <label>Item Name</label>
                    <input type="text" x-model="item.name" name="name[]" class="form-control form-control-sm" required>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group mb-3">
                    <label>Quantity</label>
                    <input type="number" x-model="item.quantity" @change="calcInvoice()" @keyup="calcInvoice()" name="quantity[]" class="form-control form-control-sm" required>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group mb-3">
                    <label>Price</label>
                    <input type="number" step="0.1" x-model="item.price" @change="calcInvoice()" @keyup="calcInvoice()" name="price[]" class="form-control form-control-sm" required>
                  </div>
                </div>
                <div class="col-12">
                  <div class="form-group mb-0">
                    <textarea type="text" x-model="item.description" name="description[]" class="form-control form-control-sm" placeholder="Description (optional)"></textarea>
                  </div>
                </div>
              </div>
            </div>
          </template>

          <button class="btn btn-primary px-2 pb-1 pt-1" @click="addItem()" type="button">
            <i class="fas fa-plus"></i>Add New Item
          </button>

          <div class="row">
            <div class="col-md-5 offset-md-7">
              <div class="form-group mb-3 form-row">
                <label class="col-md-3 col-form-label" for="sub_total">Sub Total</label>
                <div class="col-md-9">
                  <input name="sub_total" id="sub_total" :value="subTotal" class="form-control form-control-solid @error('sub_total') is-invalid @enderror" type="text" readonly required>
                  @error('sub_total')
                  <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>
              <div class="form-group mb-3 form-row">
                <label class="col-md-3 col-form-label" for="discount">Discount</label>
                <div class="col-md-9">
                  <input name="discount" id="discount" x-model="discount" @change="calcInvoice()" @keyup="calcInvoice()" class="form-control form-control-solid @error('discount') is-invalid @enderror" type="number"
                         autocomplete="off" required>
                  @error('discount')
                  <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>
              <div class="form-group mb-3 form-row">
                <label class="col-md-3 col-form-label" for="grand_total">Grand Total</label>
                <div class="col-md-9">
                  <input name="grand_total" id="grand_total" :value="grandTotal" class="form-control form-control-solid @error('grand_total') is-invalid @enderror" type="text" readonly required>
                  @error('grand_total')
                  <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="row justify-content-center mt-10">
          <div class="form-group col-md-5">
            <label for="note_to_recipient">{{__('Note to Recipient')}}</label>
            <textarea name="note_to_recipient" id="note_to_recipient" rows="4" class="form-control">{{ old('note_to_recipient', ($invoice->note_to_recipient ?? $company->invoice_note_to_recipient)) }}</textarea>
            @error('note_to_recipient')
            <div class="text-danger">{{ $message }}</div>
            @enderror
          </div>
          <div class="form-group col-md-5">
            <label for="terms_and_conditions">{{__('Terms & Conditions')}}</label>
            <textarea name="terms_and_conditions" id="terms_and_conditions" rows="4"
                      class="form-control">{{ old('terms_and_conditions', ($invoice->terms_and_conditions ?? $company->invoice_terms_and_conditions)) }}</textarea>
            @error('terms_and_conditions')
            <div class="text-danger">{{ $message }}</div>
            @enderror
          </div>
        </div>

      </form>
    </div>
  </div>
@endsection

@push('script')
  <script src="//unpkg.com/alpinejs" defer></script>
  <script>
    document.addEventListener('alpine:init', () => {
      Alpine.data('ItemsData', (initialValue = [{name: '', price: '', quantity: 1, description: '',}]) => ({

        items: initialValue,
        bill_to: '{{ $invoice->bill_to ?? null }}',
        recipient_first_name: '{{ $invoice->recipient_first_name ?? null }}',
        recipient_last_name: '{{ $invoice->recipient_last_name ?? null }}',
        recipient_company: '{{ $invoice->recipient_company ?? null }}',
        recipient_mobile: '{{ $invoice->recipient_mobile ?? null }}',

        subTotal: '{{ $invoice->sub_total ?? 0 }}',
        discount: '{{ $invoice->discount ?? 0 }}',
        grandTotal: '{{ $invoice->grand_total ?? 0 }}',

        addItem() {
          this.items.push({
            name: '',
            price: '',
            quantity: 1,
            description: '',
          });
        },

        removeItem(index) {
          if (this.items.length === 1) return;
          this.items.splice(index, 1);

          this.calcInvoice();
        },

        calcInvoice() {
          this.subTotal = 0;
          this.items.forEach(item => {
            this.subTotal += item.price * item.quantity;
          })
          this.grandTotal = this.subTotal - this.discount;
        }
      }))
    })
  </script>
@endpush

@push('style')
  <style>
    .link-copy-box:hover .link-copy-input {
      visibility: visible;
    }

    .link-copy-input {
      position: absolute;
      bottom: 105%;
      left: 50%;
      transform: translateX(-50%);
      width: 200px;
      visibility: hidden;
    }
  </style>
@endpush

@push('script')
  <script>
    function selectLink(id) {
      let input = document.getElementById('link-copy-input-' + id);
      input.select();
      document.execCommand('copy');
    }
  </script>
@endpush
