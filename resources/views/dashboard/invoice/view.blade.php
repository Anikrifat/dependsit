<!DOCTYPE html>
<html lang="en">
<head>
  @php($company = company())
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>{{ $company->name }} | {{ $invoice->id }}</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">

  <style>
    @media print {

      .table.table-bordered thead th,
      .table.table-bordered thead td,
      .table.table-bordered thead th,
      .table.table-bordered tbody td {
        border: 1px solid #e8e7e6 !important;
      }

      .table.table-bordered,
      .table.table-bordered tfoot th,
      .table.table-bordered tfoot td {
        border: none !important;
      }

      .table th,
      .table td {
        padding: 0.5rem !important;
      }

      header, footer, nav, aside {
        display: none !important;
      }

      #print {
        width: 100vw !important;
        margin: 0 !important;
      }

      .no-print {
        display: none !important;
      }
    }

    .card {
      border: none;
    }

    .card-header {
      border-bottom: none
    }

    h3 {
      font-size: 20px
    }

    .table.table-bordered thead th,
    .table.table-bordered thead td,
    .table.table-bordered thead th,
    .table.table-bordered tbody td {
      border: 1px solid #e8e7e6 !important;
    }

    .table.table-bordered,
    .table.table-bordered tfoot th,
    .table.table-bordered tfoot td {
      border: none !important;
    }

    .table th,
    .table td {
      padding: 0.5rem;
    }

    .link-copy-box:hover .link-copy-input {
      visibility: visible;
    }

    .link-copy-input {
      position: absolute;
      bottom: 0;
      left: 105%;
      width: 200px;
      visibility: hidden;
    }
  </style>
</head>

<body class="bg-gray-100">

<div class="container mx-auto px-3 md:px-56 py-6">
  <div class="no-print">
    <div class="mb-3">
      @if(request()->routeIs('invoice.pay.view'))
        <div class="text-center mb-6">
          <a href="{{ route('invoice.pay', $invoice->id) }}" class="px-3 py-1.5 bg-blue-400 text-white rounded-md">Pay With Paypal</a>
        </div>
      @else
        <div class="flex justify-between">
          <div>
            <a href="{{ route('invoice.index') }}" class="px-3 py-1.5 bg-blue-400 text-white rounded-md mr-1">Back</a>
            <button onclick="printDiv()" class="px-3 py-1.5 bg-blue-400 text-white rounded-md ">Print</button>
            <div class="relative inline-block link-copy-box">
              <button onclick="selectLink({{ $invoice->id }})" type="button" class="px-3 py-1.5 bg-blue-400 text-white rounded-md">Copy</button>
              <input type="url" id="link-copy-input-{{ $invoice->id }}" class="link-copy-input p-1 focus:outline-none border rounded" value="{{ route('invoice.pay.view', [$invoice->id, $invoice->remember_token]) }}">
            </div>
          </div>
          <a href="{{ route('invoice.edit', $invoice->id) }}" class="px-3 py-1.5 bg-blue-400 text-white rounded-md mr-1">Edit</a>
        </div>
      @endif
    </div>
  </div>

  <div class="card rounded-lg bg-white px-4 py-6 text-sm" id="print">
    <div class="card-header">
      <table style="width: 100%">
        <tr>
          <td>
            <div class="text-left">
              @if($company->logo)
                <img src="{{ setImage($company->logo) }}" alt="{{ $company->name }}" class="max-h-16">
              @else
                <h3 class="mb-0">{{ $company->name }}</h3>
              @endif
              <p>{{ $company->location }}</p>
              <div class="p-2 md:p-5 bg-gray-100 rounded-md inline-block mt-3">
                @if($invoice->payment_status)
                  <span class="text-blue-500 font-bold">Paid:</span> {{ $invoice->grand_total }}
                @else
                  <span class="text-blue-500 font-bold">Due:</span> {{ $invoice->grand_total }}
                @endif
              </div>
              <p class="text-gray-700 mt-4">Date : {{ $invoice->created_at->format('F d, Y') }}</p>
            </div>
          </td>
          <td>
            <div class="text-right">
              <h4 class="text-xl mb-3">{{ $invoice->id }} : Invoice</h4>
              <p class="text-gray-700 text-lg">Bill To</p>
              @if($invoice->recipient_first_name)
                <p class="text-gray-700">{{ $invoice->recipient_first_name . ' ' . $invoice->recipient_last_name }}</p>
              @endif
              @if($invoice->recipient_mobile)
                <p class="text-gray-700">{{ $invoice->recipient_mobile }}</p>
              @endif
              <p class="text-gray-700">{{ $invoice->bill_to }}</p>
              @if($invoice->recipient_company)
                <p class="text-gray-700">{{ $invoice->recipient_company }} : Company</p>
              @endif
            </div>
          </td>
        </tr>
      </table>
    </div>

    <div class="card-body pt-4">
      <div class="overflow-x-auto">
        <table class="table table-bordered" style="margin-bottom: 0; width: 100%">
          <thead>
          <tr>
            <th style="width: 50px;">#</th>
            <th style="width: 500px;">Items</th>
            <th style="width: 100px;text-align:center;">Quantity</th>
            <th style="width: 100px;text-align:center;">Price</th>
          </tr>
          </thead>
          <tbody>
          @foreach ($invoice->Items as $item )
            <tr>
              <td>{{ $loop->iteration }}</td>
              <td>
                <div>{{ $item->name }}</div>
                <div>
                  {{ $item->description }}
                </div>
              </td>
              <td style="text-align:center;">{{ $item->quantity }}</td>
              <td style="text-align:center;">{{ $item->price }}</td>
            </tr>
          @endforeach
          </tbody>
          <tfoot>
          <tr>
            <td colspan="3" class="text-right"><strong>Sub Total </strong></td>
            <td style="text-align:center;">{{ $invoice->sub_total }}</td>
          </tr>
          <tr>
            <td colspan="3" class="text-right"><strong>Discount </strong></td>
            <td style="text-align:center;">{{ $invoice->discount }}</td>
          </tr>
          <tr>
            <td colspan="3" class="text-right"><strong>Grand Total </strong></td>
            <td style="text-align:center;">{{ $invoice->grand_total }}</td>
          </tr>
          </tfoot>
        </table>
        <div class="flex justify-center">
          <div class="w-full md:w-10/12 grid grid-cols-2 pt-8">
            <div>
              @if($invoice->note_to_recipient)
                <h4 class="font-bold mb-1">Note to Recipient</h4>
                <p class="text-xs">{{ $invoice->note_to_recipient }}</p>
              @endif
            </div>
            <div>
              @if($invoice->terms_and_conditions)
                <h4 class="font-bold mb-1">Terms & Conditions</h4>
                <p class="text-xs">{{ $invoice->terms_and_conditions }}</p>
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  function printDiv() {
    window.print()
  }

  function selectLink(id) {
    let input = document.getElementById('link-copy-input-' + id);
    input.select();
    document.execCommand('copy');
  }
</script>
</body>

</html>
