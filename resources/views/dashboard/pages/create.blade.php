@extends('layouts.dashboard')

@section('content')
  <div class="card card-custom card-sticky" id="kt_page_sticky_card">
    @if(session()->has('success'))
      <div class="alert alert-success">
        {{session()->get('success')}}
      </div>
    @elseif(session()->has('error'))
      <div class="alert alert-danger">
        {{session()->get('error')}}
      </div>
    @endif
    <div class="card-header">
      <div class="card-title">
        <h3 class="card-label">
          Add Page
        </h3>
      </div>
      <div class="card-toolbar">
        <a href="{{ route('pages.index') }}" class="btn btn-light-primary font-weight-bolder mr-2">
          <i class="ki ki-long-arrow-back icon-sm"></i>
          Back
        </a>
        <div class="btn-group">
          <button type="submit" form="kt_form" class="btn btn-primary font-weight-bolder submit">
            <i class="ki ki-check icon-sm"></i>
            Save Page
          </button>
        </div>
      </div>
    </div>
    <div class="card-body">
      @if ($errors->any())
        <div class="alert alert-danger">
          <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
      @endif
      <form class="form" id="kt_form" method="post" action="{{ route('pages.store') }}" enctype="multipart/form-data">
        @csrf
        <div class="row">
          <div class="col-xl-8 offset-xl-2">
            <div class="my-5">

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="icon">Icon</label>
                <div class="col-md-9">
                  <input name="icon" id="icon" value="{{ old('icon') }}" class="form-control form-control-solid @error('icon') is-invalid @enderror" type="file">
                  @error('icon')
                  <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="banner">Banner</label>
                <div class="col-md-9">
                  <input name="banner" id="banner" value="{{ old('banner') }}" class="form-control form-control-solid @error('banner') is-invalid @enderror" type="file">
                  @error('banner')
                  <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="name">Name&nbsp;<span class="text-danger">*</span></label>
                <div class="col-md-9">
                  <input name="name" id="name" value="{{ old('name') }}" class="form-control form-control-solid @error('name') is-invalid @enderror" type="text">
                  @error('name')
                  <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="slug">Slug&nbsp;<span class="text-danger">*</span></label>
                <div class="col-md-9">
                  <input name="slug" id="slug" value="{{ old('slug') }}" class="form-control form-control-solid @error('slug') is-invalid @enderror" type="text">
                  @error('slug')
                  <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="heading">Heading</label>
                <div class="col-md-9">
                  <input name="heading" id="heading" value="{{ old('heading') }}" class="form-control form-control-solid @error('heading') is-invalid @enderror" type="text">
                  @error('heading')
                  <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="sub_heading">Sub Heading</label>
                <div class="col-md-9">
                  <input name="sub_heading" id="sub_heading" value="{{ old('sub_heading') }}" class="form-control form-control-solid @error('sub_heading') is-invalid @enderror" type="text">
                  @error('sub_heading')
                  <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="description">Description</label>
                <div class="col-md-9">
                  <textarea name="description" id="description" rows="3" class="form-control form-control-solid @error('description') is-invalid @enderror">{{ old('description') }}</textarea>
                  @error('description')
                  <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>

              <div class="form-group row">
                <label class="col-lg-3 col-form-label" for="type"> Page Type <span class="text-danger">*</span></label>
                <div class="col-lg-9">
                  <select class="form-control form-control-solid" id="type" name="type" required>
                    @foreach (\App\Models\Page::TYPE as $type => $value)
                      @if ($type == 'default') @continue @endif
                      <option value="{{ $value }}" {{ old('type') == $value ? 'selected' : null }}>{{ ucfirst($type) }}</option>
                    @endforeach
                  </select>
                  @error('type')
                  <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="status">Publish&nbsp;<span class="text-danger">*</span></label>
                <div class="col-md-9">
                  <input type="hidden" value="0" name="status"/>
                  <span class="switch switch-icon">
                  <label>
                    <input type="checkbox" {{ old('status') ? 'checked' : null }} value="1" name="status"/>
                    <span></span>
                  </label>
                </span>
                </div>
              </div>

            </div>
          </div>
        </div>
      </form>
    </div>
  </div
@endsection
