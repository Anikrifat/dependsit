@extends('layouts.dashboard')

@section('content')
  <div class="card card-custom card-sticky" id="kt_page_sticky_card">
    @if(session()->has('success'))
      <div class="alert alert-success">
        {{session()->get('success')}}
      </div>
    @elseif(session()->has('error'))
      <div class="alert alert-danger">
        {{session()->get('error')}}
      </div>
    @endif
    <div class="card-header">
      <div class="card-title">
        <h3 class="card-label">
          Edit Page
        </h3>
      </div>
      <div class="card-toolbar">
        <a href="{{ route('pages.index') }}" class="btn btn-light-primary font-weight-bolder mr-2">
          <i class="ki ki-long-arrow-back icon-sm"></i>
          Back
        </a>
        <div class="btn-group">
          <div class="btn-group ml-2">
            <button type="submit" form="kt_form" class="btn btn-primary font-weight-bolder submit">
              <i class="ki ki-check icon-sm"></i>
              Update
            </button>
            <button type="button" class="btn btn-primary font-weight-bold btn-sm px-3 font-size-base dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
            <div class="dropdown-menu dropdown-menu-sm p-0 m-0 dropdown-menu-right" style="">
              <ul class="navi py-5">
                <li class="navi-item">
                  @if($page->type == \App\Models\Page::TYPE['seo'])
                    <a href="{{ route('packages.index', $page->id) }}" class="navi-link">
                      {{--                    <span class="navi-icon">--}}
                      {{--                      <i class="flaticon2-hourglass-1"></i>--}}
                      {{--                    </span>--}}
                      <span class="navi-text">Manage&nbsp;Packages</span>
                    </a>
                  @elseif($page->type == \App\Models\Page::TYPE['development'])
                    <a href="{{ route('pages.gallery', $page->id) }}" class="navi-link">
                      {{--                    <span class="navi-icon">--}}
                      {{--                      <i class="flaticon2-hourglass-1"></i>--}}
                      {{--                    </span>--}}
                      <span class="navi-text">Manage&nbsp;Gallery</span>
                    </a>
                  @elseif($page->slug == 'seo')
                    <a href="{{ route('pages.tabs', $page->id) }}" class="navi-link">
                      {{--                    <span class="navi-icon">--}}
                      {{--                      <i class="flaticon2-hourglass-1"></i>--}}
                      {{--                    </span>--}}
                      <span class="navi-text">Manage&nbsp;Tabs</span>
                    </a>
                  @endif
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="card-body">
      @if ($errors->any())
        <div class="alert alert-danger">
          <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
      @endif
      @if($page->hasMedia('icon'))
        <form action="{{ route('remove-media', $page->getFirstMedia('icon')->id) }}" method="post" id="icon-remove-form">@csrf @method('DELETE')</form>
      @endif
      @if($page->hasMedia('banner'))
        <form action="{{ route('remove-media', $page->getFirstMedia('banner')->id) }}" method="post" id="banner-remove-form">@csrf @method('DELETE')</form>
      @endif
      <form class="form" id="kt_form" method="post" action="{{ route('pages.update', $page->id) }}" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="row">
          <div class="col-xl-8 offset-xl-2">
            <div class="my-5">
              @if($page->hasMedia('icon'))
                <div class="form-group row">
                  <div class="col-md-9 offset-3">
                    <h4>Icon</h4>
                    <a href="{{ $page->getFirstMediaUrl('icon') }}" target="_blank">
                      <img src="{{ $page->getFirstMediaUrl('icon') }}" class="img-fluid" width="200" alt="">
                    </a>
                    <button type="submit" class="btn btn-danger btn-xs" style="padding: 1px 1px 3px 5px" form="icon-remove-form" onclick="return confirm('Are you sure to delete this media?');">
                      <i class="fas fa-trash"></i>
                    </button>
                  </div>
                </div>
              @endif

              @if($page->hasMedia('banner'))
                <div class="form-group row">
                  <div class="col-md-9 offset-3">
                    <h4>Banner</h4>
                    <a href="{{ $page->getFirstMediaUrl('banner') }}" target="_blank">
                      <img src="{{ $page->getFirstMediaUrl('banner') }}" class="img-fluid" width="200" alt="">
                    </a>
                    <button type="submit" class="btn btn-danger btn-xs" style="padding: 1px 1px 3px 5px" form="banner-remove-form" onclick="return confirm('Are you sure to delete this media?');">
                      <i class="fas fa-trash"></i>
                    </button>
                  </div>
                </div>
              @endif

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="icon">Icon</label>
                <div class="col-md-9">
                  <input name="icon" id="icon" value="{{ old('icon') }}" class="form-control form-control-solid @error('icon') is-invalid @enderror" type="file">
                  @error('icon')
                  <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="banner">Banner</label>
                <div class="col-md-9">
                  <input name="banner" id="banner" value="{{ old('banner') }}" class="form-control form-control-solid @error('banner') is-invalid @enderror" type="file">
                  @error('banner')
                  <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>

              @if($page->slug == 'home')
                @php($banner = $page->getFirstMedia('banner'))
                <div class="form-group row">
                  <label class="col-md-3 col-form-label" for="banner_heading">Banner Heading</label>
                  <div class="col-md-9">
                    <input name="banner_heading" id="banner_heading" value="{{ old('banner_heading', $banner ? $banner->getCustomProperty('heading') : null) }}" class="form-control form-control-solid @error('banner_heading') is-invalid @enderror" type="text">
                    @error('banner_heading')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-3 col-form-label" for="banner_description">Banner Description</label>
                  <div class="col-md-9">
                    <textarea name="banner_description" id="banner_description" rows="3" class="form-control form-control-solid @error('banner_description') is-invalid @enderror">{{ old('banner_description', $banner ? $banner->getCustomProperty('description') : null) }}</textarea>
                    @error('banner_description')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                  </div>
                </div>
              @endif

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="name">Name&nbsp;<span class="text-danger">*</span></label>
                <div class="col-md-9">
                  <input name="name" id="name" value="{{ old('name', $page->name) }}" class="form-control form-control-solid @error('name') is-invalid @enderror" type="text">
                  @error('name')
                  <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="slug">Slug&nbsp;<span class="text-danger">*</span></label>
                <div class="col-md-9">
                  <input name="slug" id="slug" value="{{ old('slug', $page->slug) }}" class="form-control form-control-solid @error('slug') is-invalid @enderror" type="text" @if($page->build_in) disabled @endif>
                  @error('slug')
                  <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="heading">Heading</label>
                <div class="col-md-9">
                  <input name="heading" id="heading" value="{{ old('heading', $page->heading) }}" class="form-control form-control-solid @error('heading') is-invalid @enderror" type="text">
                  @error('heading')
                  <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="sub_heading">Sub Heading</label>
                <div class="col-md-9">
                  <input name="sub_heading" id="sub_heading" value="{{ old('sub_heading', $page->sub_heading) }}" class="form-control form-control-solid @error('sub_heading') is-invalid @enderror" type="text">
                  @error('sub_heading')
                  <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="description">Description</label>
                <div class="col-md-9">
                  <textarea name="description" id="description" rows="3" class="form-control form-control-solid @error('description') is-invalid @enderror">{{ old('description', $page->description) }}</textarea>
                  @error('description')
                  <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>

              <div class="form-group row">
                <label class="col-lg-3 col-form-label" for="type"> Page Type
                  <span class="text-danger">*</span></label>
                <div class="col-lg-9">
                  <select class="form-control form-control-solid" id="type" name="type" required @if($page->build_in) disabled @endif>
                    @foreach (\App\Models\Page::TYPE as $type => $value)
                      @if ($type == 'default' && !$page->build_in) @continue @endif
                      <option value="{{ $value }}" {{ old('type', $page->type) == $value ? 'selected' : null }}>{{ ucfirst($type) }}</option>
                    @endforeach
                  </select>
                  @error('type')
                  <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="status">Publish&nbsp;<span class="text-danger">*</span></label>
                <div class="col-md-9">
                  <input type="hidden" value="0" name="status"/>
                  <span class="switch switch-icon">
                  <label>
                    <input type="checkbox" {{ old('status', $page->status) ? 'checked' : null }} value="1" name="status"/>
                    <span></span>
                  </label>
                </span>
                </div>
              </div>

              @if($page->slug == 'seo')
                <div class="form-group row">
                  <label class="col-md-3 col-form-label" for="seo_heading">Seo Heading&nbsp;<span class="text-danger">*</span></label>
                  <div class="col-md-9">
                    <input name="properties[seo_heading]" id="seo_heading" value="{{ old('seo_heading', $page->getProperties('seo_heading')) }}" class="form-control form-control-solid @error('seo_heading') is-invalid @enderror" type="text">
                    @error('seo_heading')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-md-3 col-form-label" for="seo_description">Seo Description&nbsp;<span class="text-danger">*</span></label>
                  <div class="col-md-9">
                    <textarea name="properties[seo_description]" id="seo_description" rows="3" class="form-control form-control-solid @error('seo_description') is-invalid @enderror">{{ old('seo_description', $page->getProperties('seo_description')) }}</textarea>
                    @error('seo_description')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                  </div>
                </div>
              @endif
              @if($page->slug == 'about-us')
                <h3 class="card-label pt-2 border-1 border-top">Features</h3>
                <div class="form-group row">
                  <label class="col-md-3 col-form-label" for="feature_title_one">Title One&nbsp;<span class="text-danger">*</span></label>
                  <div class="col-md-9">
                    <input name="properties[feature_title_one]" id="feature_title_one" value="{{ old('feature_title_one', $page->getProperties('feature_title_one')) }}" class="form-control form-control-solid @error('feature_title_one') is-invalid @enderror" type="text">
                    @error('feature_title_one')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-md-3 col-form-label" for="feature_description_one">Description One&nbsp;<span class="text-danger">*</span></label>
                  <div class="col-md-9">
                    <textarea name="properties[feature_description_one]" id="feature_description_one" rows="3" class="form-control form-control-solid @error('feature_description_one') is-invalid @enderror">{{ old('feature_description_one', $page->getProperties('feature_description_one')) }}</textarea>
                    @error('feature_description_one')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-md-3 col-form-label" for="feature_title_two">Title Two&nbsp;<span class="text-danger">*</span></label>
                  <div class="col-md-9">
                    <input name="properties[feature_title_two]" id="feature_title_two" value="{{ old('feature_title_two', $page->getProperties('feature_title_two')) }}" class="form-control form-control-solid @error('feature_title_two') is-invalid @enderror" type="text">
                    @error('feature_title_two')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-md-3 col-form-label" for="feature_description_two">Description Two&nbsp;<span class="text-danger">*</span></label>
                  <div class="col-md-9">
                    <textarea name="properties[feature_description_two]" id="feature_description_two" rows="3" class="form-control form-control-solid @error('feature_description_two') is-invalid @enderror">{{ old('feature_description_two', $page->getProperties('feature_description_two')) }}</textarea>
                    @error('feature_description_two')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-md-3 col-form-label" for="feature_title_three">Title Three&nbsp;<span class="text-danger">*</span></label>
                  <div class="col-md-9">
                    <input name="properties[feature_title_three]" id="feature_title_three" value="{{ old('feature_title_three', $page->getProperties('feature_title_three')) }}" class="form-control form-control-solid @error('feature_title_three') is-invalid @enderror" type="text">
                    @error('feature_title_three')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-md-3 col-form-label" for="feature_description_three">Description Three&nbsp;<span class="text-danger">*</span></label>
                  <div class="col-md-9">
                    <textarea name="properties[feature_description_three]" id="feature_description_three" rows="3" class="form-control form-control-solid @error('feature_description_three') is-invalid @enderror">{{ old('feature_description_three', $page->getProperties('feature_description_three')) }}</textarea>
                    @error('feature_description_three')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                  </div>
                </div>
              @endif
            </div>
          </div>
        </div>
      </form>
    </div>
  </div
@endsection
