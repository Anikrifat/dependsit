@extends('layouts.dashboard')

@section('content')
  <div class="card card-custom card-sticky" id="kt_page_sticky_card">
    @if(session()->has('success'))
      <div class="alert alert-success">
        {{session()->get('success')}}
      </div>
    @elseif(session()->has('error'))
      <div class="alert alert-danger">
        {{session()->get('error')}}
      </div>
    @endif
    <div class="card-header">
      <div class="card-title">
        <h4 class="card-label">
          Manage Gallery<small class="ml-2"><strong>Page :</strong> {{ $page->name }}
          </small>
        </h4>
      </div>
      <div class="card-toolbar">
        <a href="{{ route('pages.edit', $page->id) }}" class="btn btn-light-primary font-weight-bolder mr-2">
          <i class="ki ki-long-arrow-back icon-sm"></i>
          Back
        </a>
        <div class="btn-group">
          <button type="submit" form="kt_form" class="btn btn-primary font-weight-bolder submit">
            <i class="ki ki-check icon-sm"></i>
            Save Changes
          </button>
        </div>
      </div>
    </div>
    <div class="card-body">
      @if ($errors->any())
        <div class="alert alert-danger">
          <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
      @endif
      <form class="form" id="kt_form" method="post" action="{{ route('pages.gallery', $page->id) }}" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="row">
          <div class="col-xl-8 offset-xl-2">
            <div class="my-5">

              @php($media = $page->getMedia('gallery', ['name' => 'gallery_image_one'])->first())
              <div class="form-group row">
                <div class="col-md-9 offset-3">
                  <h4>1.</h4>
                  @if($media)
                    <a href="{{ $media->getUrl() }}" target="_blank">
                      <img src="{{ $media->getUrl() }}" class="img-fluid" width="200" alt="">
                    </a>
                  @endif
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="gallery_image_one_heading">Image Title&nbsp;<span class="text-danger">*</span></label>
                <div class="col-md-9">
                  <input name="gallery_image_one_heading" id="gallery_image_one_heading" value="{{ old('gallery_image_one_heading', $media ? $media->getCustomProperty('heading') : null) }}" class="form-control form-control-solid @error('gallery_image_one_heading') is-invalid @enderror" type="text">
                  @error('gallery_image_one_heading')
                  <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="gallery_image_one">Image&nbsp;<span class="text-danger">*</span></label>
                <div class="col-md-9">
                  <input name="gallery_image_one" id="gallery_image_one" class="form-control form-control-solid @error('gallery_image_one') is-invalid @enderror" type="file">
                  @error('gallery_image_one')
                  <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>

              @php($media = $page->getMedia('gallery', ['name' => 'gallery_image_two'])->first())
              <div class="form-group row">
                <div class="col-md-9 offset-3">
                  <h4>2.</h4>
                  @if($media)
                    <a href="{{ $media->getUrl() }}" target="_blank">
                      <img src="{{ $media->getUrl() }}" class="img-fluid" width="200" alt="">
                    </a>
                  @endif
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="gallery_image_two_heading">Image Title&nbsp;<span class="text-danger">*</span></label>
                <div class="col-md-9">
                  <input name="gallery_image_two_heading" id="gallery_image_two_heading" value="{{ old('gallery_image_two_heading', $media ? $media->getCustomProperty('heading') : null) }}" class="form-control form-control-solid @error('gallery_image_two_heading') is-invalid @enderror" type="text">
                  @error('gallery_image_two_heading')
                  <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="gallery_image_two">Image&nbsp;<span class="text-danger">*</span></label>
                <div class="col-md-9">
                  <input name="gallery_image_two" id="gallery_image_two" class="form-control form-control-solid @error('gallery_image_two') is-invalid @enderror" type="file">
                  @error('gallery_image_two')
                  <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>

              @php($media = $page->getMedia('gallery', ['name' => 'gallery_image_three'])->first())
              <div class="form-group row">
                <div class="col-md-9 offset-3">
                  <h4>3.</h4>
                  @if($media)
                    <a href="{{ $media->getUrl() }}" target="_blank">
                      <img src="{{ $media->getUrl() }}" class="img-fluid" width="200" alt="">
                    </a>
                  @endif
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="gallery_image_three_heading">Image Title&nbsp;<span class="text-danger">*</span></label>
                <div class="col-md-9">
                  <input name="gallery_image_three_heading" id="gallery_image_three_heading" value="{{ old('gallery_image_three_heading', $media ? $media->getCustomProperty('heading') : null) }}" class="form-control form-control-solid @error('gallery_image_three_heading') is-invalid @enderror" type="text">
                  @error('gallery_image_three_heading')
                  <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="gallery_image_three">Image&nbsp;<span class="text-danger">*</span></label>
                <div class="col-md-9">
                  <input name="gallery_image_three" id="gallery_image_three" class="form-control form-control-solid @error('gallery_image_three') is-invalid @enderror" type="file">
                  @error('gallery_image_three')
                  <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>

              @php($media = $page->getMedia('gallery', ['name' => 'gallery_image_four'])->first())
              <div class="form-group row">
                <div class="col-md-9 offset-3">
                  <h4>4.</h4>
                  @if($page->hasMedia('gallery'))
                    <a href="{{ $media->getUrl() }}" target="_blank">
                      <img src="{{ $media->getUrl() }}" class="img-fluid" width="200" alt="">
                    </a>
                  @endif
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="gallery_image_four_heading">Image Title&nbsp;<span class="text-danger">*</span></label>
                <div class="col-md-9">
                  <input name="gallery_image_four_heading" id="gallery_image_four_heading" value="{{ old('gallery_image_four_heading', $media ? $media->getCustomProperty('heading') : null) }}" class="form-control form-control-solid @error('gallery_image_four_heading') is-invalid @enderror" type="text">
                  @error('gallery_image_four_heading')
                  <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="gallery_image_four">Image&nbsp;<span class="text-danger">*</span></label>
                <div class="col-md-9">
                  <input name="gallery_image_four" id="gallery_image_four" class="form-control form-control-solid @error('gallery_image_four') is-invalid @enderror" type="file">
                  @error('gallery_image_four')
                  <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>

              @php($media = $page->getMedia('gallery', ['name' => 'gallery_image_five'])->first())
              <div class="form-group row">
                <div class="col-md-9 offset-3">
                  <h4>5.</h4>
                  @if($page->hasMedia('gallery'))
                    <a href="{{ $media->getUrl() }}" target="_blank">
                      <img src="{{ $media->getUrl() }}" class="img-fluid" width="200" alt="">
                    </a>
                  @endif
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="gallery_image_five_heading">Image Title&nbsp;<span class="text-danger">*</span></label>
                <div class="col-md-9">
                  <input name="gallery_image_five_heading" id="gallery_image_five_heading" value="{{ old('gallery_image_five_heading', $media ? $media->getCustomProperty('heading') : null) }}" class="form-control form-control-solid @error('gallery_image_five_heading') is-invalid @enderror" type="text">
                  @error('gallery_image_five_heading')
                  <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="gallery_image_five">Image&nbsp;<span class="text-danger">*</span></label>
                <div class="col-md-9">
                  <input name="gallery_image_five" id="gallery_image_five" class="form-control form-control-solid @error('gallery_image_five') is-invalid @enderror" type="file">
                  @error('gallery_image_five')
                  <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>

            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
@endsection
