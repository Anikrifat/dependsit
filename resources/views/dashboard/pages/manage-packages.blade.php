@extends('layouts.dashboard')

@section('content')
  <div class="card card-custom card-sticky" id="kt_page_sticky_card">
    @if(session()->has('success'))
      <div class="alert alert-success">
        {{session()->get('success')}}
      </div>
    @elseif(session()->has('error'))
      <div class="alert alert-danger">
        {{session()->get('error')}}
      </div>
    @endif
    <div class="card-header">
      <div class="card-title">
        <h3 class="card-label">
          Manage Packages<small class="ml-2"><strong>Page :</strong> {{ $page->name }}
          </small>
        </h3>
      </div>
      <div class="card-toolbar">
        <a href="{{ route('pages.edit', $page->id) }}" class="btn btn-light-primary font-weight-bolder mr-2">
          <i class="ki ki-long-arrow-back icon-sm"></i>
          Back
        </a>
        <div class="btn-group">
          <button type="submit" form="kt_form" class="btn btn-primary font-weight-bolder submit">
            <i class="ki ki-check icon-sm"></i>
            Save Changes
          </button>
        </div>
      </div>
    </div>
    <div class="card-body">
      @if ($errors->any())
        <div class="alert alert-danger">
          <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
      @endif
      <form class="form" id="kt_form" method="post" action="{{ route('packages.update-info', $page->id) }}" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="row">
          <div class="col-xl-8 offset-xl-2">
            <div class="my-5">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="package_heading">Heading</label>
                <div class="col-md-9">
                  <input name="package_heading" id="package_heading" value="{{ old('package_heading', $page->package_heading) }}" class="form-control form-control-solid @error('package_heading') is-invalid @enderror" type="text">
                  @error('package_heading')
                  <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="package_sub_heading">Sub Heading</label>
                <div class="col-md-9">
                  <input name="package_sub_heading" id="package_sub_heading" value="{{ old('package_sub_heading', $page->package_sub_heading) }}" class="form-control form-control-solid @error('package_sub_heading') is-invalid @enderror" type="text">
                  @error('package_sub_heading')
                  <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>
      <h3 class="card-label">Packages List</h3>
      <div class="table-responsive" x-data="{}">
        <table class="table table-separate table-head-custom table-checkable" id="kt_datatable">
          <thead>
          <tr>
            <th>SL</th>
            <th>Heading</th>
            <th>Sub Heading</th>
            <th>Price</th>
            <th>Features</th>
            <th>Ordering</th>
            <th>Actions</th>
          </tr>
          </thead>
          <tbody>
          @foreach($packages as $package)
            <tr>
              <td>{{ $loop->iteration }}</td>
              <td>{{ $package->heading }}</td>
              <td>{{ $package->sub_heading }}</td>
              <td>{{ $package->price }} $</td>
              <td>
                @foreach($package->features as $feature)
                  <span><i class="fas fa-arrow-right fa-xs"></i>&nbsp;{{ $feature }}</span>
                  <br>
                @endforeach
              </td>
              <td>{{ $package->order }}</td>
              <td nowrap="nowrap">
                <button @click="$dispatch('notify', {{ $package->id }})" class="btn btn-icon btn-light btn-hover-primary btn-sm mx-3">
                  <span class="svg-icon svg-icon-md svg-icon-primary">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                      <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect x="0" y="0" width="24" height="24"></rect>
                        <path d="M12.2674799,18.2323597 L12.0084872,5.45852451 C12.0004303,5.06114792 12.1504154,4.6768183 12.4255037,4.38993949 L15.0030167,1.70195304 L17.5910752,4.40093695 C17.8599071,4.6812911 18.0095067,5.05499603 18.0083938,5.44341307 L17.9718262,18.2062508 C17.9694575,19.0329966 17.2985816,19.701953 16.4718324,19.701953 L13.7671717,19.701953 C12.9505952,19.701953 12.2840328,19.0487684 12.2674799,18.2323597 Z"
                              fill="#000000" fill-rule="nonzero" transform="translate(14.701953, 10.701953) rotate(-135.000000) translate(-14.701953, -10.701953)"></path>
                        <path d="M12.9,2 C13.4522847,2 13.9,2.44771525 13.9,3 C13.9,3.55228475 13.4522847,4 12.9,4 L6,4 C4.8954305,4 4,4.8954305 4,6 L4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 L2,6 C2,3.790861 3.790861,2 6,2 L12.9,2 Z"
                              fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                      </g>
                    </svg>
                  </span>
                </button>
                <form method="post" action="{{ route('packages.destroy', [$page->id, $package->id]) }}" class="d-inline-block">
                  @csrf
                  @method('DELETE')
                  <button type="submit" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-icon btn-light btn-hover-danger btn-sm">
                    <span class="svg-icon svg-icon-md svg-icon-primary">
                      <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                          <rect x="0" y="0" width="24" height="24"></rect>
                          <path d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero"></path>
                          <path d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z"
                                fill="#000000" opacity="0.3"></path>
                        </g>
                      </svg>
                    </span>
                  </button>
                </form>
              </td>
            </tr>
          @endforeach
          <tr>
            <td class="text-center" colspan="6">
              <button class="btn btn-primary px-2 pb-1 pt-1" type="button" @click="$dispatch('notify', 0)">
                <i class="fas fa-plus"></i>&nbsp;Add Package
              </button>
            </td>
          </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
@endsection

@push('model')
  <!-- Modal-->
  <div x-data="managePackageFormData()" @notify.window="modal($event.detail)" class="modal fade" id="packageAddEditModel" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="packageAddEditModelLabel" x-text="modal_title"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <i aria-hidden="true" class="ki ki-close"></i>
          </button>
        </div>
        <div class="modal-body">
          <form x-bind:action="form_action" id="packageAddEditForm" method="post">
            @csrf
            <template x-if="updating">
              <input type="hidden" name="_method" value="PUT">
            </template>
            <div class="form-group">
              <label for="heading">Package Heading&nbsp;<span class="text-danger">*</span></label>
              <input x-model="heading" type="text" id="heading" name="heading" class="form-control" placeholder="Package Heading" required/>
            </div>
            <div class="form-group">
              <label for="sub_heading">Package Sub Heading&nbsp;<span class="text-danger">*</span></label>
              <input x-model="sub_heading" type="text" id="sub_heading" name="sub_heading" class="form-control" placeholder="Package Sub Heading" required/>
            </div>
            <div class="form-group">
              <label for="price">Price&nbsp;<span class="text-danger">*</span></label>
              <input x-model="price" type="text" id="price" name="price" class="form-control" placeholder="Price" required/>
            </div>
            <div class="form-group">
              <label for="order">Ordering&nbsp;<span class="text-danger">*</span></label>
              <input x-model="order" type="text" id="order" name="order" class="form-control" placeholder="Ordering" value="0" required/>
            </div>
            <div class="form-group">
              <label>Features&nbsp;<span class="text-danger">*</span></label>
              <template x-for="(feature, index) in features" :key="index">
                <div class="input-group mb-1">
                  <input type="text" name="features[]" x-model="features[index]" class="form-control" placeholder="Feature" required/>
                  <div class="input-group-append">
                    <button @click="removeFeature(index)" class="btn btn-danger btn-sm" type="button">
                      <i class="fas fa-trash"></i>
                    </button>
                  </div>
                </div>
              </template>
            </div>
            <div class="form-group mb-0 form-row justify-content-end">
              <button class="btn btn-primary px-2 pb-1 pt-1" @click="addFeature()" type="button">
                <i class="fas fa-plus"></i>Add More
              </button>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary font-weight-bold" form="packageAddEditForm" x-text="updating ? 'Update' : 'Save'"></button>
        </div>
      </div>
    </div>
  </div>
@endpush

@push('script')
  <script src="//unpkg.com/alpinejs" defer></script>
  <script>
    const PACKAGE_INFO_URL = function (package_id) {
      return '{{ url('/dashboard/website-settings/pages/' . $page->id . '/packages/get-package-info/') }}' + '/' + package_id;
    };
    const PACKAGE_STORE_URL = '{{ route('packages.store', $page->id) }}';
    const PACKAGE_UPDATE_URL = function (package_id) {
      return '{{ url('/dashboard/website-settings/pages/' . $page->id . '/packages/') }}' + '/' + package_id;
    };

    function managePackageFormData() {
      return {
        'modal_title': 'Add Package',
        'form_action': PACKAGE_STORE_URL,
        'heading': '',
        'sub_heading': '',
        'price': '',
        'order': '',
        'features': [''],
        'updating': false,
        addFeature() {
          this.features.push('');
        },
        removeFeature(index) {
          if (this.features.length === 1)
            return;
          this.features.splice(index, 1);
        },
        'modal': function (package_id = '') {
          if (package_id) {
            fetch(PACKAGE_INFO_URL(package_id)).then(response => response.json())
              .then(data => {
                this.modal_title = 'Edit Package';
                this.form_action = PACKAGE_UPDATE_URL(package_id);
                this.heading = data.heading;
                this.sub_heading = data.sub_heading;
                this.price = data.price;
                this.order = data.order;
                this.features = data.features;
                this.updating = true;
              });
          } else {
            this.modal_title = 'Add Package';
            this.form_action = PACKAGE_STORE_URL;
            this.heading = '';
            this.sub_heading = '';
            this.price = '';
            this.order = 0;
            this.features = [''];
            this.updating = false;
          }
          $('#packageAddEditModel').modal('show');
        }
      };
    }
  </script>
@endpush
