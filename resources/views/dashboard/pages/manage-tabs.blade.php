@extends('layouts.dashboard')

@section('content')
  <div class="card card-custom card-sticky" id="kt_page_sticky_card">
    @if(session()->has('success'))
      <div class="alert alert-success">
        {{session()->get('success')}}
      </div>
    @elseif(session()->has('error'))
      <div class="alert alert-danger">
        {{session()->get('error')}}
      </div>
    @endif
    <div class="card-header">
      <div class="card-title">
        <h4 class="card-label">
          Manage Tabs<small class="ml-2"><strong>Page :</strong> {{ $page->name }}
          </small>
        </h4>
      </div>
      <div class="card-toolbar">
        <a href="{{ route('pages.edit', $page->id) }}" class="btn btn-light-primary font-weight-bolder mr-2">
          <i class="ki ki-long-arrow-back icon-sm"></i>
          Back
        </a>
        <div class="btn-group">
          <button type="submit" form="kt_form" class="btn btn-primary font-weight-bolder submit">
            <i class="ki ki-check icon-sm"></i>
            Save Changes
          </button>
        </div>
      </div>
    </div>
    <div class="card-body">
      @if ($errors->any())
        <div class="alert alert-danger">
          <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
      @endif
      <form class="form" id="kt_form" method="post" action="{{ route('pages.tabs', $page->id) }}" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="row">
          <div class="col-xl-8 offset-xl-2">
            <div class="my-5">

              @php($media = $page->getMedia('tabs', ['key' => 'tab_one'])->first())
              <div class="form-group row">
                <div class="col-md-9 offset-3">
                  <h4>Tab 1.</h4>
                  @if($media)
                    <a href="{{ $media->getUrl() }}" target="_blank">
                      <img src="{{ $media->getUrl() }}" class="img-fluid" width="200" alt="">
                    </a>
                  @endif
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="tab_one_image">Image&nbsp;<span class="text-danger">*</span></label>
                <div class="col-md-9">
                  <input name="tab_one_image" id="tab_one_image" class="form-control form-control-solid @error('tab_one_image') is-invalid @enderror" type="file">
                  @error('tab_one_image')
                  <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="tab_one_name">Name&nbsp;<span class="text-danger">*</span></label>
                <div class="col-md-9">
                  <input name="tab_one_name" id="tab_one_name" value="{{ old('tab_one_name', $media ? $media->getCustomProperty('name') : null) }}" class="form-control form-control-solid @error('tab_one_name') is-invalid @enderror" type="text">
                  @error('tab_one_name')
                  <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="tab_one_heading">Heading&nbsp;<span class="text-danger">*</span></label>
                <div class="col-md-9">
                  <input name="tab_one_heading" id="tab_one_heading" value="{{ old('tab_one_heading', $media ? $media->getCustomProperty('heading') : null) }}" class="form-control form-control-solid @error('tab_one_heading') is-invalid @enderror" type="text">
                  @error('tab_one_heading')
                  <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="tab_one_description">Description&nbsp;<span class="text-danger">*</span></label>
                <div class="col-md-9">
                  <textarea name="tab_one_description" id="tab_one_description" rows="3" class="form-control form-control-solid @error('tab_one_description') is-invalid @enderror">{{ old('tab_one_description', $media ? $media->getCustomProperty('description') : null) }}</textarea>
                  @error('tab_one_description')
                  <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>

              @php($media = $page->getMedia('tabs', ['key' => 'tab_two'])->first())
              <div class="form-group row">
                <div class="col-md-9 offset-3">
                  <h4>Tab 2.</h4>
                  @if($media)
                    <a href="{{ $media->getUrl() }}" target="_blank">
                      <img src="{{ $media->getUrl() }}" class="img-fluid" width="200" alt="">
                    </a>
                  @endif
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="tab_two_image">Image&nbsp;<span class="text-danger">*</span></label>
                <div class="col-md-9">
                  <input name="tab_two_image" id="tab_two_image" class="form-control form-control-solid @error('tab_two_image') is-invalid @enderror" type="file">
                  @error('tab_two_image')
                  <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="tab_two_name">Name&nbsp;<span class="text-danger">*</span></label>
                <div class="col-md-9">
                  <input name="tab_two_name" id="tab_two_name" value="{{ old('tab_two_name', $media ? $media->getCustomProperty('name') : null) }}" class="form-control form-control-solid @error('tab_two_name') is-invalid @enderror" type="text">
                  @error('tab_two_name')
                  <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="tab_two_heading">Heading&nbsp;<span class="text-danger">*</span></label>
                <div class="col-md-9">
                  <input name="tab_two_heading" id="tab_two_heading" value="{{ old('tab_two_heading', $media ? $media->getCustomProperty('heading') : null) }}" class="form-control form-control-solid @error('tab_two_heading') is-invalid @enderror" type="text">
                  @error('tab_two_heading')
                  <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="tab_two_description">Description&nbsp;<span class="text-danger">*</span></label>
                <div class="col-md-9">
                  <textarea name="tab_two_description" id="tab_two_description" rows="3" class="form-control form-control-solid @error('tab_two_description') is-invalid @enderror">{{ old('tab_two_description', $media ? $media->getCustomProperty('description') : null) }}</textarea>
                  @error('tab_two_description')
                  <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>

              @php($media = $page->getMedia('tabs', ['key' => 'tab_three'])->first())
              <div class="form-group row">
                <div class="col-md-9 offset-3">
                  <h4>Tab 3.</h4>
                  @if($media)
                    <a href="{{ $media->getUrl() }}" target="_blank">
                      <img src="{{ $media->getUrl() }}" class="img-fluid" width="200" alt="">
                    </a>
                  @endif
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="tab_three_image">Image&nbsp;<span class="text-danger">*</span></label>
                <div class="col-md-9">
                  <input name="tab_three_image" id="tab_three_image" class="form-control form-control-solid @error('tab_three_image') is-invalid @enderror" type="file">
                  @error('tab_three_image')
                  <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="tab_three_name">Name&nbsp;<span class="text-danger">*</span></label>
                <div class="col-md-9">
                  <input name="tab_three_name" id="tab_three_name" value="{{ old('tab_three_name', $media ? $media->getCustomProperty('name') : null) }}" class="form-control form-control-solid @error('tab_three_name') is-invalid @enderror" type="text">
                  @error('tab_three_name')
                  <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="tab_three_heading">Heading&nbsp;<span class="text-danger">*</span></label>
                <div class="col-md-9">
                  <input name="tab_three_heading" id="tab_three_heading" value="{{ old('tab_three_heading', $media ? $media->getCustomProperty('heading') : null) }}" class="form-control form-control-solid @error('tab_three_heading') is-invalid @enderror" type="text">
                  @error('tab_three_heading')
                  <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="tab_three_description">Description&nbsp;<span class="text-danger">*</span></label>
                <div class="col-md-9">
                  <textarea name="tab_three_description" id="tab_three_description" rows="3" class="form-control form-control-solid @error('tab_three_description') is-invalid @enderror">{{ old('tab_three_description', $media ? $media->getCustomProperty('description') : null) }}</textarea>
                  @error('tab_three_description')
                  <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>

              @php($media = $page->getMedia('tabs', ['key' => 'tab_four'])->first())
              <div class="form-group row">
                <div class="col-md-9 offset-3">
                  <h4>Tab 4.</h4>
                  @if($media)
                    <a href="{{ $media->getUrl() }}" target="_blank">
                      <img src="{{ $media->getUrl() }}" class="img-fluid" width="200" alt="">
                    </a>
                  @endif
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="tab_four_image">Image&nbsp;<span class="text-danger">*</span></label>
                <div class="col-md-9">
                  <input name="tab_four_image" id="tab_four_image" class="form-control form-control-solid @error('tab_four_image') is-invalid @enderror" type="file">
                  @error('tab_four_image')
                  <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="tab_four_name">Name&nbsp;<span class="text-danger">*</span></label>
                <div class="col-md-9">
                  <input name="tab_four_name" id="tab_four_name" value="{{ old('tab_four_name', $media ? $media->getCustomProperty('name') : null) }}" class="form-control form-control-solid @error('tab_four_name') is-invalid @enderror" type="text">
                  @error('tab_four_name')
                  <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="tab_four_heading">Heading&nbsp;<span class="text-danger">*</span></label>
                <div class="col-md-9">
                  <input name="tab_four_heading" id="tab_four_heading" value="{{ old('tab_four_heading', $media ? $media->getCustomProperty('heading') : null) }}" class="form-control form-control-solid @error('tab_four_heading') is-invalid @enderror" type="text">
                  @error('tab_four_heading')
                  <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="tab_four_description">Description&nbsp;<span class="text-danger">*</span></label>
                <div class="col-md-9">
                  <textarea name="tab_four_description" id="tab_four_description" rows="3" class="form-control form-control-solid @error('tab_four_description') is-invalid @enderror">{{ old('tab_four_description', $media ? $media->getCustomProperty('description') : null) }}</textarea>
                  @error('tab_four_description')
                  <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>

            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
@endsection
