<div class="d-block d-md-none">

    <div id="mySidenav" class="sidenav bg-light">
        <div class="sidenav_inner">
            <div class="contenedor-menu">
                <!-- service secction -->


                <div class="menu_section_mobile">
                    <button type="button" class="btn btn-block btn_side_menu fw-600 pl-2" data-toggle="collapse"
                        data-target="#service">Service <i class="fa fa-chevron-down float-right mt-1"></i></button>
                    <div id="service" class="collapse">
                        <ul class="menu p-0">
                            <li>
                                <a href="#">SEO<i class="fa fa-chevron-down"></i></a>
                                <ul class="p-0">
                                    @foreach (pages(\App\Models\Page::TYPE['seo']) as $dynamicPage)
                                        <li><a href="{{ route('page', $dynamicPage->slug) }}">{{ $dynamicPage->name }}</a>
                                        </li>

                                    @endforeach

                                    {{-- <li><a href="guest_post.php">On page Seo</a></li> --}}
                                </ul>
                            </li>
                            <li><a href="#">Development <i class="fa fa-chevron-down"></i></a>
                                <ul class="p-0">

                                  @foreach (pages(\App\Models\Page::TYPE['development']) as $dynamicPage)
                                  <li><a href="{{ route('page', $dynamicPage->slug) }}">{{ $dynamicPage->name }}</a>
                                  </li>

                              @endforeach
                                </ul>
                            </li>
                            <li><a href="#">Digintal marketting<i class="fa fa-chevron-down"></i></a>
                                <ul class="p-0">
                                    <li><a href="#ex2">Website</a></li>
                                    <li><a href="#ex1">Seo</a></li>
                                </ul>
                            </li>

                            <li>

                            </li>
                        </ul>
                    </div>
                </div>
                <!-- //service secction -->
                <!-- pricing section -->
                {{-- <div class="menu_section_mobile">
                    <button type="button" class="btn btn-block btn_side_menu fw-600 pl-2" data-toggle="collapse"
                        data-target="#pricing">Pricing</button>
                    <div id="pricing" class="collapse">
                        <ul class="menu p-0">

                            <li><a href="#">Web Development pricing <i class="fa fa-chevron-down"></i></a>
                                <ul class="p-0">

                                    <li><a href="pricing.php">Web Design pricing</a></li>
                                    <li><a href="pricing.php">Web Development pricing</a></li>
                                    <li><a href="pricing.php">App Development pricing</a></li>
                                    <li><a href="pricing.php">SEO pricing</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Digintal marketting<i class="fa fa-chevron-down"></i></a>
                                <ul class="p-0">
                                    <li><a href="pricing.php">Website pricing</a></li>
                                    <li><a href="pricing.php">Seo Pricing</a></li>
                                </ul>
                            </li>
                            <li><a href="#">SEO<i class="fa fa-chevron-down"></i></a>
                                <ul class="p-0">
                                    <li><a href="pricing.php">SEO HOST pricing</a></li>
                                    <li><a href="pricing.php">On page Seo Pricing</a></li>
                                </ul>
                            </li>A
                            <li>

                            </li>
                        </ul>
                    </div>
                </div> --}}
                <!-- //pricing section -->
                <div class="menu_section_mobile">
                    <a href="{{ route('page', 'product') }}" class="btn btn-block btn_side_menu fw-600 pl-2">Products </a>
                </div>
                <div class="menu_section_mobile">
                    <a href="{{ route('page', 'clients') }}" class="btn btn-block btn_side_menu fw-600 pl-2">Clients </a>
                </div>
                <div class="menu_section_mobile">
                    <a href="{{ route('page', 'portfolio') }}" class="btn btn-block btn_side_menu fw-600 pl-2">Our Works</a>
                </div>
                <div class="menu_section_mobile">
                    <a href="{{ route('page', 'about-us') }}" class="btn btn-block btn_side_menu fw-600 pl-2">About us </a>
                </div>
            </div>
            <div class="p-2">
              <div class="">
                @guest
                <a href="{{ route('login') }}" class="btn btn-success text-light btn-sm btn-block rounded-0">Sign In</a>
                <a href="{{ route('register') }}" class="btn btn-info text-light btn-sm btn-block rounded-0">Register</a>
                @else
                <a href="{{ route('dashboard') }}" class="btn btn-orange text-light btn-sm btn-block rounded-0">Dashboard</a>

                @endguest


              </div>
              {{-- <div class="aside-img-area my-3 mx-auto text-center fixed-bottom">
              @if($company->logo)
              <img src="{{ setImage($company->logo) }}" class="company-logo" alt="{{ $company->name }}">
            @else
              <h2>{{ $company->name }}</h2>
            @endif
          </div> --}}
            </div>
        </div>
    </div>
</div>
