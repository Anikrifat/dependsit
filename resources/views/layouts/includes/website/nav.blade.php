<section class="main-nav-section sticky-top">
  <div id="main-nav" class="navbar navbar-inverse bs-docs-nav" role="banner">
    <div class="container ">
      <div class="navbar-header responsive-logo  d-flex justify-contnent-between">
        <div id="seo-obak-navber" class="navbar-brand">
          <a href="{{ route('page') }}" class="custom-logo-link text-dark" rel="home" aria-current="page">
            @if($company->logo)
              <img src="{{ setImage($company->logo) }}" class="company-logo" alt="{{ $company->name }}">
            @else
              <h2>{{ $company->name }}</h2>
            @endif
          </a>

          <div class="logo-hover-content" style="display: none;">
            <h1>{{ company()->name }}</h1>
            <ul>
              <li>
                <i class="fa fa-phone"></i>
                <p>
                  {{ company()->mobile1 }}
                  @if(company()->mobile2)
                    <br>
                    {{ company()->mobile2 }}
                  @endif
                </p>
              </li>
              <li>
                <i class="fa fa-envelope"></i>
                <p>{{ company()->email }}</p>
              </li>
              <li>
                <i class="fa fa-map-marker"></i>
                <p>{{ company()->location }}</p>
              </li>
            </ul>
          </div>

        </div>
        <div class="d-block d-md-none">
          <div class="mbs text-dark">
                        <span style="font-size:30px;cursor:pointer" class="inline-block py-1 px-2 md:py-2 clNav"
                              id="onav">
                            <i class="fas fa-bars text-gray-500"></i>
                        </span>
            <span style="font-size:30px;cursor:pointer" class="inline-block py-1 px-2 md:py-2 d-none"
                  title="Voice Search" id="cnav">
                            <i class="fas fa-times text-gray-500"></i>
                        </span>
          </div>

        </div>
      </div>
      <nav class="navbar-collapse bs-navbar-collapse collapse show  d-none d-md-block" id="site-navigation">
        <ul id="menu-primary2" class="nav navbar-nav navbar-right responsive-nav main-nav-list">
          <li id="" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children ">
            <a href="{{ route('page', 'seo') }}">Services
              <span class="ml-2 font-orange"><i class="fas fa-angle-down"></i></span></a>
            <ul class="sub-menu">

              <li id=""
                  class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children ">
                <a>
                  <h3 class="mx-auto">SEO </h3>
                  <span class="menu_description"></span>
                  <div class="bb mx-auto w-75"></div>

                </a>
                <ul class="sub-menu">
                  @foreach (pages(\App\Models\Page::TYPE['seo']) as $dynamicPage)
                    <li id="" class="menu-item menu-item-type-post_type menu-item-object-page ">
                      <a href="{{ route('page', $dynamicPage->slug) }}">{{ $dynamicPage->name }}
                        <span class="menu_description"></span></a>
                    </li>
                @endforeach

                <!-- <li id="" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="seo.php">1 Month SEO<span class="menu_description"></span></a></li> -->
                </ul>
              </li>

              <li id=""
                  class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children ">
                <a>
                  <h3 class="mx-auto">Development </h3>
                  <span class="menu_description"></span>
                  <div class="bb mx-auto w-75"></div>

                </a>
                <ul class="sub-menu">
                  @foreach (pages(\App\Models\Page::TYPE['development']) as $dynamicPage)
                    <li id="" class="menu-item menu-item-type-post_type menu-item-object-page ">
                      <a href="{{ route('page', $dynamicPage->slug) }}">{{  $dynamicPage->name }}
                        <span class="menu_description"></span></a>
                    </li>
                  @endforeach
                </ul>
              </li>
              <li id=""
                  class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children ">
                <a>
                  <h3>Marketing &amp; Advertising</h3>
                  <span class="menu_description"></span>
                  <div class="bb mx-auto w-75"></div>
                </a>
                <ul class="sub-menu">
                  {{-- <li id="" class="menu-item menu-item-type-post_type menu-item-object-page ">
                    <a href="#">facebook marketiing Service<span
                        class="menu_description"></span></a>
                  </li>
                  <li id="" class="menu-item menu-item-type-post_type menu-item-object-page ">
                    <a href="#">Digital Marketing<span class="menu_description"></span></a>
                  </li>
                  <li id="" class="menu-item menu-item-type-post_type menu-item-object-page ">
                    <a href="#">Social Media Management<span class="menu_description"></span></a>
                  </li> --}}
                </ul>
              </li>

            </ul>
          </li>
          <!-- <li id="" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children "><a href="#">Pricing<span class="ml-2 font-orange"><i class="fas fa-angle-down"></i></span></a>
    <ul class="sub-menu">
        <li id="" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children "><a href="#">
                <h3>Website Pricing Concept</h3><span class="menu_description"></span>
                <div class="bb mx-auto w-75"></div>
            </a>
            <ul class="sub-menu">
                <li id="" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="pricing.php">Website Design Pricing<span class="menu_description"></span></a></li>
                <li id="" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="pricing.php">E-commerce Website Pricing<span class="menu_description"></span></a></li>
            </ul>
        </li>
        <li id="" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children "><a href="pricing.php">
                <h3>Digital Market pricing Concept</h3><span class="menu_description"></span>
                <div class="bb mx-auto w-75"></div>
            </a>
            <ul class="sub-menu">
                <li id="" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="pricing.php">social media pricing<span class="menu_description"></span></a></li>
                <li id="" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="pricing.php">affiliate marketing pricing<span class="menu_description"></span></a></li>
            </ul>
        </li>
        <li id="" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children "><a href="pricing.php">
                <h3>SEO Pricing Concept</h3><span class="menu_description"></span>
                <div class="bb mx-auto w-75"></div>
            </a>
            <ul class="sub-menu">
                <li id="" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="pricing.php">SEO services pricing<span class="menu_description"></span></a></li>
                <li id="" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="pricing.php">One Time SEO pricing<span class="menu_description"></span></a></li>
            </ul>
        </li>

    </ul>
</li> -->
          <li id="" class="menu-item menu-item-type-custom menu-item-object-custom ">
            <a href="{{ route('page', 'clients') }}">Clients<span class="menu_description"></span></a>
          </li>
          <li id="" class="menu-item menu-item-type-post_type menu-item-object-page ">
            <a href="{{ route('page', 'portfolio') }}">Our Works<span class="menu_description"></span></a>
          </li>
          <li id="" class="menu-item menu-item-type-custom menu-item-object-custom ">
            <a href="{{ route('page', 'about-us') }}">About Us<span class="menu_description"></span></a>
          </li>
          <li id="" class="menu-item menu-item-type-custom menu-item-object-custom ">
            <a href="{{ route('page', 'product') }}">Products<span class="menu_description"></span></a>
          </li>
          
          @guest
            <li id="" class="menu-item menu-item-type-custom menu-item-object-custom ">
              <a href="{{ route('login') }}">Sign In<span class="menu_description"></span></a>
            </li>
            <li id="" class="menu-item menu-item-type-custom menu-item-object-custom ">
              <a href="{{ route('register') }}">Register<span class="menu_description"></span></a>
            </li>
          @else
            <li id="" class="menu-item menu-item-type-custom menu-item-object-custom ">
              <a href="{{ route('dashboard') }}">Dashboard<span class="menu_description"></span></a>
            </li>
          @endguest
          <li id="menu-item-16208"
              class="menu-item menu-item-type-custom menu-item-object-custom menu-item-16208">
            <a href="#contacts/"><span class="mb-3">{{ company()->email }}</span><i class="fa fa-phone phnico"
                                                                                    aria-hidden="true"></i> {{ company()->mobile1 }}
              <span class="menu_description"></span></a>
          </li>


        </ul>
      </nav>
      @include('layouts.includes.website.mobile-nav')
    </div>
  </div>
</section>
