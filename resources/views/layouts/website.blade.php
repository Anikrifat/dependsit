@php($company = company())

<!DOCTYPE html>
<html lang="en">

<head>
  <title>{{ $company->name }}</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css">
  <link rel="stylesheet" href="{{ asset('assets/website/css/style.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/website/css/style2.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/website/css/style3.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/website/css/responsive.css') }}">

  @stack('styles')
</head>

<body class="bg-light">
<div class="main-body">

  @include('layouts.includes.website.nav')

  <div class="container">
<div class="content-field mt-5">
    @yield('content')
  </div>
  </div>
</div>
<footer class="footer-area">
  <div class="footer-top">
    <div class="container">
      <div class="row">
        <div class="col-lg-3 col-md-6 col-sm-6 col-sx-12">
          <div class="footer-heading">
            @if($company->footer_logo)
              <img src="{{ setImage($company->footer_logo) }}" alt="codewareltd" class="company-logo">
            @else
              <h2 class="text-white">{{ $company->name }}</h2>
            @endif
          </div>
        </div>
        <div class="col-lg-5 col-md-6 col-sm-6 col-6">
          <div class="footer-heading">
            <h5>Corporate office</h5>
            <address>
              {{ $company->location }}
              <br>
              <br>
              Mobile: {{ $company->mobile1 }}<br>
              Mobile: {{ $company->mobile2 }}<br>
              Email: {{ $company->email }}<br>
            </address>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-6">
          <div class="footer-heading">
            <h5>Pages </h5>
            <ul>
              <li>
                <a href="{{ route('page', 'seo') }}" class="btn btn-link bg-transparent text-left mb-0 py-0">Services</a>
              </li>
              <li>
                <a href="{{ route('page', 'about-us') }}" class="btn btn-link bg-transparent text-left mb-0 py-0">About Us</a>
              </li>
              <li>
                <a href="{{ route('page', 'contact-us') }}" class="btn btn-link bg-transparent text-left mb-0 py-0">Contact Us</a>
              </li>
            </ul>
          </div>
        </div>
        <div class="col-lg-12 col-12 text-center">
          <small><p class="text-light py-2" style="font-size: .7rem;">Developed & Maintained by <a href="https://spinnertech.dev/" target="_blank" class="text-primary">Spinner tech</a></p></small>
        </div>
      </div>
    </div>

  </div>
</footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js" integrity="sha512-3P8rXCuGJdNZOnUx/03c1jOTnMn3rP63nBip5gOP2qmUh5YAdVAvFZ1E+QLZZbC1rtMrQb+mah3AfYW11RUrWA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" integrity="sha512-uto9mlQzrs59VwILcLiRYeLKPPbS/bT71da/OEBYEwcdNUk8jYIy+D176RYoop1Da+f9mvkYrmj5MCLZWEtQuA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="{{ asset('assets/website/js/sidebar-accordion.js') }}"></script>
<script src="{{ asset('assets/website/js/app.js') }}"></script>
@stack('scripts')
</body>

</html>
