<div>
  @if($show)
    <span class="label label-danger position-absolute top-0 right-0">{{ $cart_count }}</span>
  @endif
</div>
