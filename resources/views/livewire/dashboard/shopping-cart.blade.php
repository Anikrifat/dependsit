<div>
  <div class="offcanvas-wrapper mb-5 scroll-pull scroll ps ps--active-y" style="height: 407px; overflow: hidden;">
    @foreach(Cart::content() as $item)
      <div class="d-flex align-items-center justify-content-between py-8">
        <div class="d-flex flex-column mr-2">
          <a href="#" class="font-weight-bold text-dark-75 font-size-lg text-hover-primary">{{ $item->name }}</a>
          {{--<span class="text-muted">$ {{ $item->price . ' X ' . $item->qty }}</span>--}}
          <div class="d-flex align-items-center mt-2">
            <span class="font-weight-bold mr-1 text-dark-75 font-size-lg">$ {{ $item->total }}</span>
          </div>
        </div>
        <button class="btn btn-xs btn-icon btn-light btn-hover-primary" wire:click="remove('{{ $item->rowId }}')">
          <i class="ki ki-close icon-xs text-muted"></i>
        </button>
      </div>
    @endforeach
    <div class="separator separator-solid"></div>
    <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
      <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
    </div>
    <div class="ps__rail-y" style="top: 0px; height: 407px; right: -2px;">
      <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 260px;"></div>
    </div>
  </div>

  <div class="offcanvas-footer" style="" kt-hidden-height="112">
    <div class="d-flex align-items-center justify-content-between mb-4">
      <span class="font-weight-bold text-muted font-size-sm mr-2">Total</span>
      <span class="font-weight-bolder text-dark-50 text-right">${{ Cart::total() }}</span>
    </div>
    <div class="d-flex align-items-center justify-content-between mb-7">
      <span class="font-weight-bold text-muted font-size-sm mr-2">Sub total</span>
      <span class="font-weight-bolder text-primary text-right">${{ Cart::subTotal() }}</span>
    </div>
    <div class="text-right">
      <a href="{{ route('reseller_order.create') }}" type="button" class="btn btn-primary text-weight-bold">Place Order</a>
    </div>
  </div>
</div>

@push('style')
  @livewireStyles
@endpush
@push('script')
  @livewireScripts
@endpush
