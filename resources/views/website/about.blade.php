@extends('layouts.website')

@section('content')
  <div class="content pt-5 mt-4">
    <section class="about">
      <div class="aboutus-section">
        <div class="container">
          <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="aboutus">
                <h2 class="aboutus-title">{{ $page->heading }}</h2>
                <p class="aboutus-text">{!! $page->description !!}</p>
                {{--                <a class="aboutus-more" href="#">read more</a>--}}
              </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="aboutus-banner">
                @if($page->hasMedia('banner'))
                  <img src="{{ $page->getFirstMediaUrl('banner') }}" alt="">
                @endif
              </div>
            </div>
            <div class="col-md-5 col-sm-6 col-xs-12">
              <div class="feature">
                <div class="feature-box">
                  <div class="clearfix">
                    <div class="iconset">
                      <span class=""><i class="fas fa-cog icon"></i></span>
                    </div>
                    <div class="feature-content">
                      <h4>{{ $page->getProperties('feature_title_one') }}</h4>
                      <p>{{ $page->getProperties('feature_description_one') }}</p>
                    </div>
                  </div>
                </div>
                <div class="feature-box">
                  <div class="clearfix">
                    <div class="iconset">
                      <span class=""><i class="fas fa-cog icon"></i></span>
                    </div>
                    <div class="feature-content">
                      <h4>{{ $page->getProperties('feature_title_two') }}</h4>
                      <p>{{ $page->getProperties('feature_description_two') }}</p>
                    </div>
                  </div>
                </div>
                <div class="feature-box">
                  <div class="clearfix">
                    <div class="iconset">
                      <span class=""><i class="fas fa-cog icon"></i></span>
                    </div>
                    <div class="feature-content">
                      <h4>{{ $page->getProperties('feature_title_three') }}</h4>
                      <p>{{ $page->getProperties('feature_description_three') }}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
@endsection
