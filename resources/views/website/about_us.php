<!DOCTYPE html>
<html lang="en">
<head>
  <title>Link Building Agency</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  
 <style>
 body{
 background:#f9fbff !important;
 
 }
 .payment_form{
        text-align:center;
        width:400px;
		padding:30px 15px;
		background:#e9ecef;
		border-radius:5px;
    }
   
    h1{
        margin:auto;
    }
    
input[type=text]{
	background:#2f2f5f;
	padding:2 5px;
	}
	
.button {
  background-color:#ca4825db;
  border: none;
  color: white;
  padding: 5px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 15px 2px;
  cursor: pointer;
  width:355px;
  border-radius:50px;
}
#button .button4 {
background-color: #dae0e5;
border:none;
margin:10px 10px;
padding:5px 46px;
 }
 .payment_form h3,h5
 {
 color:#2f2f5f;
 }
 
 /* menu*/
 .menu ul {
  list-style-type: none;
  margin: 0;
  padding-bottom:100px;
  overflow: hidden;
  
}

.menu li {
  float: left;
}

.menu li a {
  display: block;
  color: black;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size:20px;
}

.footer_menu ul {
list-style:none;
}

.footer_menu ul li{
 display: block;
}
.footer_menu ul li a {
color:white;
padding:5px 0;
}

.contact_us h4
{
    color: #181b31;
    font-size: 50px;
    font-weight: bold;
}
.contact_us p{
color:#797687

}
.contact_us ul li{
display:block;
}

.contact_form{

padding:40px;

}

.contact_form input[type=text],textarea
{

background:white;
border:none;
}

.form-control{
border:none;
}

.meet_team,
{

padding:40px 0;
}

.testimonial{

padding:40px 0;
background:#EEF7FD;
}

.testimonial .media.border.p-3 {
    background: white;
}

.photo{
display:inline-block;
}

.photo img
{
  width:200px;
 border-radius: 50%;
 margin:5px;

}

.meet_team,h1{

padding:50px 0;
}

@media only screen and (max-width: 600px) {

  
 .right img,.industry img,.bbc img,map img
 {
    width:400px;  
      
  }
}
</style>
</head>
<body>

<div class="header text-center" style="background:url('images/back.png'); padding:20px 0;
    background-size: contain; width:100%;"> 
	<div class="container">
	    <div class="row"> 
		 <div class="col-sm-6 text-left">
				<img src="images/logo.png" alt="" width="50"/>
		 </div>
		 <div class="col-sm-6">
				<?php include ('menu.php') ;?>
			</div>
		</div>  <!--end row-->
	
	</div>  <!--end container-->
</div>  <!--end header-->

<!--start contact-->
<br>
<section class="contact_us">
	<div class="container text-center">
	<h3> BIG ENOUGH TO DELIVER, SMALL ENOUGH TO CARE </h3>
<h4>Our story </h4> 
<p> The links we build today, will be essential for creating your future tomorrow…

Rather then compose a cheesy about page about how we have used other providers in the past and we’re never happy with their results… Our team mission goes deeper then that. We’ve always built links to our clients websites by hand since 2014, and then in 2019 Agency Backlinks was born. Launching initially with our link insertions, guest posts, fully managed SEO and local SEO packages, we have since gone from strength to strength, growing our agency at an exponential rate.
</p>
<p>
My goal is to provide not only a unrivalled service in terms of attention to detail and customer support but something you can be proud to show your clients. We care.
 </p>

</div>
</section>
<!--end contact us -->

<div class="container mt-3">
 
  <div class="media border p-3" style="background:#EEF7FD;">
    <img src="images/jameshome-150x150.jpg" alt="John Doe" class="mr-3 mt-3 rounded-circle" style="width:60px;">
    <div class="media-body">
      <h4>John Doe <small><i>Posted on February 19, 2016</i></small></h4>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>      
    </div>
  </div>
</div>

<section class="meet_team">
<div class="container text-center">
<h1> The team behind the brand changing an industry </h1>
	<div class="photo">
		<img src="images/jameshome-150x150.jpg" alt="no"/>
		<h4> James Gregory </h4>
        <p> Founder & CEO </p>
	</div>
	<div class="photo">
		<img src="images/jameshome-150x150.jpg" alt="no"/>
		<h4> James Gregory </h4>
        <p> Founder & CEO </p>
	</div>
	
	<div class="photo">
		<img src="images/jameshome-150x150.jpg" alt="no"/>
		<h4> James Gregory </h4>
        <p> Founder & CEO </p>
	</div>
	
	<div class="photo">
		<img src="images/jameshome-150x150.jpg" alt="no"/>
		<h4> James Gregory </h4>
        <p> Founder & CEO </p>
	</div>

</div>
</section>





<!--start testimonial-->
<section class="testimonial">
<div class="container">
<h1>Our results speak for themselves </h1>
<p> Learn why hundreds of SEO professionals choose Agency Backlinks to propel their marketing efforts. </p>
<div class="row"> 
		 <div class="col-sm-6">
			<div class="media border p-3">
    <img src="images/jameshome-150x150.jpg" alt="John Doe" class="mr-3 mt-3 rounded-circle" style="width:60px;">
    <div class="media-body">
      <h4>John Doe <small><i>Posted on February 19, 2016</i></small></h4>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>      
    </div>
  </div>
		 </div>
		 
		  <div class="col-sm-6">
			<div class="media border p-3">
    <img src="images/jameshome-150x150.jpg" alt="John Doe" class="mr-3 mt-3 rounded-circle" style="width:60px;">
    <div class="media-body">
      <h4>John Doe <small><i>Posted on February 19, 2016</i></small></h4>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>      
    </div>
  </div>
		 </div>
		 
 </div>

</div>

</section>




<!--start map-->
<section class="map">
<img src="images/map.png" class="img-responsive" width="1200" height="400"  alt="map"/>

</section>

<section class="bbc" style="background:#6071FE;padding:10px;">
	<img src="images/forbes.png"/>
</section>

<!--start footer-->
<section class="footer" style="background:#182540;padding:50px; color:white;">
<div class="container">
	<div class="row">
		<div class="col-sm-3">
			<img src="images/logo.png" alt="no" width="50"/>
		</div>
		<div class="col-sm-3">
		<h2> COMPANY </h2>
			<div class="footer_menu">
				<ul>
				  <li><a href="#">About Us </a></li>
				  <li><a href="#">Contact Us </a></li>
				  <li><a href="#">GDPR Policy</a></li>
				  <li><a href="#">Earnings Disclaimer </a></li>
				  <li><a href="#">Privacy Policy </a></li>
				</ul>
			</div>
		</div>
		
		<div class="col-sm-3">
			<h2> OUR SERVICES </h2>
			<div class="footer_menu">
			<ul>
			  <li><a href="#home">Link Insertions </a></li>
			  <li><a href="#news">Guest Posts</a></li>
			  <li><a href="#contact">Fully Managed SEO</a></li>
			  <li><a href="#about">Local SEO Packages</a></li>
			  <li><a href="#about">Earnings Disclaimer</a></li>
			  <li><a href="#about">Privacy Policy</a></li>
			</ul>
			
			</div>
		</div>
		
		<div class="col-sm-3">
			<h2> Contact us </h2>
			<div class="footer_menu">
			<ul>
			  <li><a href="#home">Agency Backlinks</a></li>
			  <li><a href="#news">Suit 2A</a></li>
			  <li><a href="#contact">Blackthorn House</a></li>
			  <li><a href="#about">St Paul’s Square</a></li>
			  <li><a href="#about">Birmingham</a></li>
			  <li><a href="#about">	B3 1RL</a></li>
			</ul>
			</div>
		</div>
	  </div>
</div>
</section>
<!--end footer-->


 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>
