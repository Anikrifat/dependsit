@extends('layouts.website')

@section('content')
  <div class="content pt-5 mt-4">
    <section class="mobile single-banner" style="background: url({{ asset('assets/website/images/app_back.jpg') }}) no-repeat;    background-size: cover;">
      <div class="container">
        <div class="row">
          <div class="banner-content">
            <ul class="service-list">
              <h1>Mobile App developer in Bangladesh</h1>
              <h3>The services we provide</h3>
              <a href="#">
                <li>
                  <i class="fa fa-check"></i>Android App Development
                </li>
              </a>
              <a href="#">
                <li>
                  <i class="fa fa-check"></i>iOS App Development
                </li>
              </a>
              <a href="#">
                <li>
                  <i class="fa fa-check"></i>Hybrid App Development
                </li>
              </a>
              <a href="#">
                <li>
                  <i class="fa fa-check"></i>AR/VR App Development
                </li>
              </a>
              <a href="#">
                <li>
                  <i class="fa fa-check"></i>Mobile apps with blockchain integration
                </li>
              </a>
              <a href="#">
                <li>
                  <i class="fa fa-check"></i>IoT application development
                </li>
              </a>
              <a href="#">
                <li>
                  <i class="fa fa-check"></i>Mobile Game Development
                </li>
              </a>
              <a href="#">
                <li>
                  <i class="fa fa-check"></i>Mobile ERP Applications
                </li>
              </a>
              <a href="#">
                <li>
                  <i class="fa fa-check"></i>eCommerce App Development
                </li>
              </a>
              <a href="#">
                <li>
                  <i class="fa fa-check"></i>Mobile Apps Development
                </li>
              </a>
              <a href="#">
                <li>
                  <i class="fa fa-check"></i>Point Of Sales (POS) Apps
                </li>
              </a>

              <h4>
                <a href="https://www.smartsoftware.com.bd/contact-us" class="custom-btn v2">Contact us</a>
              </h4>
            </ul>
          </div>

        </div>
      </div>
    </section>
    <section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="mobile-app-heading">
              <h1>A pace to Your Business Wings – Rabbiitfirm Develop Mobile app that raise your business Limit.</h1>
              <p>Rabbiitfirm Mobile app team is materializing your dream app in real life with a bang. It’s not a far reality to see your app appearing on grand platforms like Apple Store or Google Play. We are reaching out to you to give you the best deal on mobile app development.
                We are budget-friendly as well as never behind time – meeting every step of our client’s requirements.
                <a href="https://www.codewareltd.com/portfolio/">Our portfolio</a> for mobile apps gives you a quick glance at the apps we have created that are thriving in the market.
                <br><br><span>Steps of successful mobile apps development</span>
              </p>
            </div>
          </div>
        </div>
      </div>

      <div class="ux-section section-bg ">
        <div class="container">
          <div class="row">
            <div class="col-lg-8 d-flex align-items-center">
              <div class="ux-service-title">
                <h1>A great imagination, successful creation depends on strong imagination</h1>
                <p>Your unique idea and our capable building hands are a deadly combination for a brilliant and efficient app.<br>
                  We will enhance your beautiful idea with our strong teamwork to create a useful app that will totally worth it.<br>
                  You only need to let us know about your requirements and our android app development team will give you your dream app.
                </p>
              </div>
            </div>

            <div class="col-lg-4 d-flex align-items-center">
              <div class="ux-service-title-img">
                <img src="https://codewareltd.com/assets/images/app-development/a-great-imagination-successful-creation-depends-on-strong-imagination.png" alt="A great imagination, successful creation depends on strong imagination">
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="ux-section ">
        <div class="container">
          <div class="row">
            <div class="col-lg-4 d-flex align-items-center">
              <div class="ux-service-title-img">
                <img src="https://codewareltd.com/assets/images/app-development/selection-design-and-development-methodology.png" alt="Selection design and development methodology – is it Native, Responsive web or Hybrid">
              </div>
            </div>

            <div class="col-lg-8 d-flex align-items-center">
              <div class="ux-service-title">
                <h1>Selection design and development methodology – is it Native, Responsive web or Hybrid?</h1>
                <p>Apps are mainly of three types – Native, Web and Hybrid.
                  Native apps are usable on one particular platform; it can be ios, android or blackberry.
                  <br>
                  These apps are not functional in any other system other than their own. An android app is only usable in the android system; it cannot be applied to blackberry or ios.<br>
                  But still, native apps are preferred over hybrid or web apps when it comes to the performance.
                  <br>
                  Since they are made for a single platform, native apps have faster access to mobile features such as Camera, Microphone, GPS, etc. But hybrid and web apps don’t have these advantages.
                  <br>
                  Native apps are also very flexible and they come with optimized performance providing the users with the best app experience.<br>
                  They are compatible with other apps on the device and provide high-quality and beautiful user interfaces.<br>
                  Web apps are the opposite of native apps. They need not be on a particular platform; they can be accessed via any browser or mobile.<br>
                  Hybrid apps are a combination of both native and web apps. They act like a native app but also have some web app features added to them, although with some added inconveniences.<br>
                  Our mobile app development team is well-equipped to create these apps for you.
                  We will develop your selected app design as per your liking.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>


      <div class="ux-section section-bg ">
        <div class="container">
          <div class="row">
            <div class="col-lg-8 d-flex align-items-center">
              <div class="ux-service-title">
                <h1>Design the sketch layout and make the wireframe</h1>
                <p>Wireframing is a practical method very much needed not only in android app development but also in every kind of mobile application development.
                  <br>
                  A wireframe is like a skeleton - just like you can guess a lot about a creature by looking at its skeleton, similarly, a wireframe is a simplified visual guide that helps you understand how the application will mainly work without revealing the design or the features.<br>
                  We will make the basic wireframe for your app so you can essentially grasp what your app is going to be like and how it’s going to work.<br>
                  We have the best android developer panel to plan and draw wireframes for your app. Our team has been recognized as one of the best layout planners and we aim to satisfy your requirements.<br>
                </p>
              </div>
            </div>

            <div class="col-lg-4 d-flex align-items-center">
              <div class="ux-service-title-img">
                <img src="https://codewareltd.com/assets/images/app-development/design-the-sketch-layout-and-make-the-wireframe.png" alt="Design the sketch layout and make the wireframe">
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="ux-section ">
        <div class="container">
          <div class="row">
            <div class="col-lg-4 d-flex align-items-center">
              <div class="ux-service-title-img">
                <img src="https://codewareltd.com/assets/images/app-development/transform-wireframe-to-UX-design.png" alt="Transform wireframe to UX design">
              </div>
            </div>

            <div class="col-lg-8 d-flex align-items-center">
              <div class="ux-service-title">
                <h1>Transform wireframe to UX design</h1>
                <p>While Wireframe is the bone, UX is the skin of an app.<br>
                  We create full-fledged working apps from scratch that include rich UX, excellent performance and the best user experience.<br>
                  We ensure full-functioned, handy apps with aesthetic looks and top-notch performance for our clients.
                  <br>
                  Our android app development company has the finest designing team that will perfect your app with an enviable UX design, compelling everyone to love your app.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="ux-section section-bg">
        <div class="container">
          <div class="row">
            <div class="col-lg-8 d-flex align-items-center">
              <div class="ux-service-title">
                <h1>Develop a prototype and sync with the real plan</h1>
                <p>Before the final presentation, we will provide you with a prototype to ensure that you are on board with our project and if there are any changes to make we’ll discuss them with you thoroughly before launching the final product.</p>
              </div>
            </div>

            <div class="col-lg-4 d-flex align-items-center">
              <div class="ux-service-title-img">
                <img src="https://codewareltd.com/assets/images/app-development/develop-a-prototype-and-sync-with-the-real-plan.png" alt="Develop a prototype and sync with the real plan">
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="ux-section ">
        <div class="container">
          <div class="row">
            <div class="col-lg-4 d-flex align-items-center">
              <div class="ux-service-title-img">
                <img src="https://codewareltd.com/assets/images/app-development/refine-and-corrections-the-bugs.png" alt="Refine and corrections the bugs">
              </div>
            </div>

            <div class="col-lg-8 d-flex align-items-center">
              <div class="ux-service-title">
                <h1>Refine and corrections the bugs</h1>
                <p>Embellishing and delivering high-quality products and also providing full maintenance service.<br>
                  Rabbiitfirm mobile app development team also offers to fix bugs and further solutions to any kind of app trouble.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="ux-section section-bg">
        <div class="container">
          <div class="row">
            <div class="col-lg-8 d-flex align-items-center">
              <div class="ux-service-title">
                <h1>Connect with appropriate analytic tool</h1>
                <p>Integrating with proper analytic tools that help you keep track of your app’s online marketing, how the users behave in-app, the app’s performance, etc.
                </p>
              </div>
            </div>

            <div class="col-lg-4 d-flex align-items-center">
              <div class="ux-service-title-img">
                <img src="https://codewareltd.com/assets/images/app-development/connect-with-appropriate-analytic-tools.png" alt="Connect with appropriate analytic tool">
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="ux-section ">
        <div class="container">
          <div class="row">
            <div class="col-lg-4 d-flex align-items-center">
              <div class="ux-service-title-img">
                <img src="https://codewareltd.com/assets/images/app-development/release-to-beta-tester.png" alt="Release to beta-tester">
              </div>
            </div>

            <div class="col-lg-8 d-flex align-items-center">
              <div class="ux-service-title">
                <h1>Release to beta-tester</h1>
                <p>Our Rabbiitfirm android app development team has sharp and reputable beta-testers for apps to ensure user-friendliness and functionality.</p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="ux-section section-bg">
        <div class="container">
          <div class="row">
            <div class="col-lg-8 d-flex align-items-center">
              <div class="ux-service-title">
                <h1>Release to Market place – apple store or google play</h1>
                <p>
                  Marketing is one of the main factors in mobile app development. Because even with all the incredibility an app will fall short of success just for the lack of right marketing.<br>
                  That’s why we strategically launch apps in the market with the clarity to feature them in the highest-ranking position.

                </p>
              </div>
            </div>

            <div class="col-lg-4 d-flex align-items-center">
              <div class="ux-service-title-img">
                <img src="https://codewareltd.com/assets/images/app-development/releasetoMarketplaceapplestoreorgoogleplay.png" alt="Release to Market place – apple store or google play">
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="ux-section ">
        <div class="container">
          <div class="row">
            <div class="col-lg-4 d-flex align-items-center">
              <div class="ux-service-title-img">
                <img src="https://codewareltd.com/assets/images/app-development/upgrade-your-app-with-improvements-and-new-features.png" alt="Upgrade your app with improvements and new features">
              </div>
            </div>

            <div class="col-lg-8 d-flex align-items-center">
              <div class="ux-service-title">
                <h1>Upgrade your app with improvements and new features</h1>
                <p>
                  We are here to provide you with plenty of brand new android app features and making UX effortless with regular bug fixings.<br>
                  We will also upgrade your app regularly with the latest features and necessary changes.

                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
@endsection
