@extends('layouts.website')

@section('content')
  <div class="content pt-5 mt-4 ">
    <section class="all-client-logo-section py-5">
      <div class="container">
        <div class="home-section-header padding_btm_none">
          <h2 class="default-color-1">{{ $page->heading }}</h2>
          <p class="default-color-1">{!! $page->description !!}</p>
        </div>
        <div class="row">
          @foreach($clients as $client)
          <div class="col-lg-3 col-md-4 col-sm-6 col-6">
            <div class="client-img m-1 text-center">
              <img class="lazy loaded img-fluid" src="{{ $client->getFirstMediaUrl('logo') }}" data-src="{{ $client->getFirstMediaUrl('logo') }}" alt="{{ $client->name }}" data-was-processed="true">
            </div>
          </div>
          @endforeach
        </div>
      </div>
    </section>
  </div>
@endsection
