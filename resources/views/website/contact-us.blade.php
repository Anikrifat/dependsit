@extends('layouts.website')

@section('content')
  <div class="content pt-5 mt-4">
    <div class="site-section py-5">
      <div class="container">
        <div class="row">
          <div class="col-lg-6">
            <h2 class="mb-5 text-black">{{ $page->heading }}</h2>
            <div class="row">
              <div class="col-md-6 form-group">
                <label for="fname">First Name</label>
                <input type="text" id="fname" class="form-control form-control-lg">
              </div>
              <div class="col-md-6 form-group">
                <label for="lname">Last Name</label>
                <input type="text" id="lname" class="form-control form-control-lg">
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 form-group">
                <label for="eaddress">Email Address</label>
                <input type="text" id="eaddress" class="form-control form-control-lg">
              </div>
              <div class="col-md-6 form-group">
                <label for="tel">Tel. Number</label>
                <input type="text" id="tel" class="form-control form-control-lg">
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 form-group">
                <label for="message">Message</label>
                <textarea name="" id="message" cols="30" rows="2" class="form-control"></textarea>
              </div>
            </div>
            <div class="row">
              <div class="col-12">
                <input type="submit" value="Submit" class="btn btn-primary btn-lg px-5">
              </div>
            </div>
          </div>
          <div class="col-lg-5 ml-auto">
            <div class="mb-3 bg-light">
              <h3 class="mb-5 text-black">Contact Info</h3>
              <p class="mb-0 font-weight-bold text-black">Address</p>
              <p class="mb-4 text-black">{{ company()->location }}</p>
              <p class="mb-0 font-weight-bold text-black">Phone</p>
              <p class="mb-4">
                <a href="#">{{ company()->mobile1 }}{{ company()->mobile2 ? ', ' . company()->mobile2 : null }}</a>
              </p>
              <p class="mb-0 font-weight-bold text-black">Email Address</p>
              <p class="mb-0">
                <a href="#"><span class="__cf_email__" data-cfemail="2f56405a5d4a424e46436f4b40424e4641014c4042">{{ company()->email }}</span></a>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
