<!DOCTYPE html>
<html lang="en">
<head>
  <title>Link Building Agency</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

 <style>
 body{
 background:#f9fbff !important;

 }
 .payment_form{
        text-align:center;
        width:400px;
		padding:30px 15px;
		background:#e9ecef;
		border-radius:5px;
    }

    h1{
        margin:auto;
    }

input[type=text]{
	background:#2f2f5f;
	padding:2 5px;
	}

.button {
  background-color:#ca4825db;
  border: none;
  color: white;
  padding: 5px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 15px 2px;
  cursor: pointer;
  width:355px;
  border-radius:50px;
}
#button .button4 {
background-color: #dae0e5;
border:none;
margin:10px 10px;
padding:5px 46px;
 }
 .payment_form h3,h5
 {
 color:#2f2f5f;
 }

 /* menu*/
 .menu ul {
  list-style-type: none;
  margin: 0;
  padding-bottom:100px;
  overflow: hidden;

}

.menu li {
  float: left;
}

.menu li a {
  display: block;
  color: black;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size:20px;
}

.footer_menu ul {
list-style:none;
}

.footer_menu ul li{
 display: block;
}
.footer_menu ul li a {
color:white;
padding:5px 0;
}

.contact_us h4
{
    color: #181b31;
    font-size: 50px;
    font-weight: bold;
}
.contact_us p{
color:#797687

}
.contact_us ul li{
display:block;
}

.contact_form{

padding:40px;

}

.contact_form input[type=text],textarea
{

background:white;
border:none;
}

.form-control{
border:none;
}

@media only screen and (max-width: 600px) {


 .map img,.bbc img
 {
    width:400px;

  }
}
</style>
</head>
<body>

<div class="header text-center" style="background:url('images/back.png'); padding:20px 0;
    background-size: contain; width:100%;">
	<div class="container">
	    <div class="row">
		 <div class="col-sm-6 text-left">
				<img src="images/logo.png" alt="" width="50"/>
		 </div>
		 <div class="col-sm-6">
					<?php include ('menu.php') ;?>
			</div>
		</div>  <!--end row-->

	</div>  <!--end container-->
</div>  <!--end header-->

<!--start contact-->
<br>
<section class="contact_us">
	<div class="container">
<h4>Contact Us </h4>
<p> Our customer support team are available 24/7 so do not hesitate to get in touch with us. Feel free to drop us a message or give our support team a call if you have any service related queries.
 </p>
<ul>
<li>  +44 (0) 1213 691 036 </li>
<li>  +1 (323) 3103 632 </li>
</ul>
</div>
</section>
<!--end contact us -->

<section class="map">
<img src="images/map.png" alt="map"/>

</section>

<section class="contact_form">
<div class="container">
<h2> Drop us a message </h2>

	<div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="well well-sm">
          <form class="form-horizontal" action="" method="post">
          <fieldset>


            <!-- Name input-->
            <div class="form-group">

              <div class="col-md-9">
                <input id="name" name="name" type="text" placeholder="Your name" class="form-control">
              </div>
            </div>

            <!-- Email input-->
            <div class="form-group">

              <div class="col-md-9">
                <input id="email" name="email" type="text" placeholder="Your email" class="form-control">
              </div>
            </div>

	        <!-- Email input-->
            <div class="form-group">

              <div class="col-md-9">
                <input id="email" name="email" type="text" placeholder="Telephone" class="form-control">
              </div>
            </div>

            <!-- Message body -->
            <div class="form-group">

              <div class="col-md-9">
                <textarea class="form-control" id="message" name="message" placeholder="Your message" rows="5"></textarea>
              </div>
            </div>

            <!-- Form actions -->
            <div class="form-group">
              <div class="col-md-12">
                <button type="submit" class="btn btn-primary btn-lg">Submit</button>
              </div>
            </div>
          </fieldset>
          </form>
        </div>
      </div>
	</div>
</div>

</section>



<section class="bbc" style="background:#6071FE;padding:10px;">
	<img src="images/forbes.png"/>
</section>

<!--start footer-->
<section class="footer" style="background:#182540;padding:50px; color:white;">
<div class="container">
	<div class="row">
		<div class="col-sm-3">
			<img src="images/logo.png" alt="no" width="50"/>
		</div>
		<div class="col-sm-3">
		<h2> COMPANY </h2>
			<div class="footer_menu">
				<ul>
				  <li><a href="#">About Us </a></li>
				  <li><a href="#">Contact Us </a></li>
				  <li><a href="#">GDPR Policy</a></li>
				  <li><a href="#">Earnings Disclaimer </a></li>
				  <li><a href="#">Privacy Policy </a></li>
				</ul>
			</div>
		</div>

		<div class="col-sm-3">
			<h2> OUR SERVICES </h2>
			<div class="footer_menu">
			<ul>
			  <li><a href="#home">Link Insertions </a></li>
			  <li><a href="#news">Guest Posts</a></li>
			  <li><a href="#contact">Fully Managed SEO</a></li>
			  <li><a href="#about">Local SEO Packages</a></li>
			  <li><a href="#about">Earnings Disclaimer</a></li>
			  <li><a href="#about">Privacy Policy</a></li>
			</ul>

			</div>
		</div>

		<div class="col-sm-3">
			<h2> Contact us </h2>
			<div class="footer_menu">
			<ul>
			  <li><a href="#home">Agency Backlinks</a></li>
			  <li><a href="#news">Suit 2A</a></li>
			  <li><a href="#contact">Blackthorn House</a></li>
			  <li><a href="#about">St Paul’s Square</a></li>
			  <li><a href="#about">Birmingham</a></li>
			  <li><a href="#about">	B3 1RL</a></li>
			</ul>
			</div>
		</div>
	  </div>
</div>
</section>
<!--end footer-->


 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>
