@extends('layouts.website')

@section('content')
  <div class="content pt-5 mt-4">

    @if($page->hasMedia('banner'))
      <section class="service_baner">
        <img src="{{ $page->getFirstMediaUrl('banner') }}" alt="" class="img-fluid w-100">
      </section>
    @endif

    <section class="web_content">
      <div class="container">
        <div class="web-application">
          <div class="text-center web-title">
            <h1>{{ $page->heading }}</h1>
            <p>{{ $page->sub_heading }}</p>
          </div>

          @if($page->hasMedia('gallery'))
          <div class="service-section">
            <div class="row">
              @php($media = $page->getMedia('gallery', ['name' => 'gallery_image_one'])->first())
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="web-item webapp-item">
                  <a href="#">
                    <img src="{{ $media->getUrl() }}" alt="{{ $media->getCustomProperty('heading') }}">
                    <h1>{{ $media->getCustomProperty('heading') }}</h1>
                  </a>
                </div>
              </div>

              @php($media = $page->getMedia('gallery', ['name' => 'gallery_image_two'])->first())
              <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                <div class="web-item webdesign-item">
                  <a href="#">
                    <img src="{{ $media->getUrl() }}" alt="{{ $media->getCustomProperty('heading') }}">
                    <h1>{{ $media->getCustomProperty('heading') }}</h1>
                  </a>
                </div>
              </div>

              @php($media = $page->getMedia('gallery', ['name' => 'gallery_image_three'])->first())
              <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                <div class="web-item psddesign-item">
                  <a href="#">
                    <img src="{{ $media->getUrl() }}" alt="{{ $media->getCustomProperty('heading') }}">
                    <h1>{{ $media->getCustomProperty('heading') }}</h1>
                  </a>
                </div>
              </div>

              @php($media = $page->getMedia('gallery', ['name' => 'gallery_image_four'])->first())
              <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                <div class="web-item webcmscus-item">
                  <a href="#">
                    <img src="{{ $media->getUrl() }}" alt="{{ $media->getCustomProperty('heading') }}">
                    <h1>{{ $media->getCustomProperty('heading') }}</h1>
                  </a>
                </div>
              </div>

              @php($media = $page->getMedia('gallery', ['name' => 'gallery_image_five'])->first())
              <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                <div class="web-item webecommerce-item">
                  <a href="#">
                    <img src="{{ $media->getUrl() }}" alt="{{ $media->getCustomProperty('heading') }}">
                    <h1>{{ $media->getCustomProperty('heading') }}</h1>
                  </a>
                </div>
              </div>
            </div>
          </div>
          @endif

          <div class="about-sec">
            <div class="row">
              <div class="col-lg-12">
                <p>{!! $page->description !!}</p>
              </div>
            </div>
          </div>

        </div>
      </div>
    </section>
  </div>
@endsection
