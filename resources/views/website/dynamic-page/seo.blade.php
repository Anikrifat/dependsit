@extends('layouts.website')
@push('styles')
    <style>
        #pricing .slider {
            -webkit-appearance: none;
            width: 100%;
            height: 25px;
            background: #d3d3d3;
            outline: none;
            opacity: 0.7;
            -webkit-transition: .2s;
            transition: opacity .2s;
        }


        #pricing .slider:hover {
            opacity: 1;
        }


        #pricing .slider::-webkit-slider-thumb {
            -webkit-appearance: none;
            appearance: none;
            width: 25px;
            height: 25px;
            background: #0b5bbe;
            cursor: pointer;
        }


        #pricing .slider::-moz-range-thumb {
            width: 25px;
            height: 25px;
            background: #0b5bbe;
            cursor: pointer;
        }

        #pricing .slider {
            -webkit-appearance: none;
            width: 100%;
            height: 25px;
            background: #d3d3d3;
            outline: none;
            opacity: 0.7;
            -webkit-transition: .2s;
            transition: opacity .2s;
        }


        #pricing .slider:hover {
            opacity: 1;
        }


        #pricing .slider::-webkit-slider-thumb {
            -webkit-appearance: none;
            appearance: none;
            width: 25px;
            height: 25px;
            background: #0b5bbe;
            cursor: pointer;
        }


        #pricing .slider::-moz-range-thumb {
            width: 25px;
            height: 25px;
            background: #0b5bbe;
            cursor: pointer;
        }

    </style>
@endpush
@section('content')
    <div class="content pt-5 mt-4">

        @if ($page->hasMedia('banner'))
            <section class="content_banenr">
                <div class="banner_area">
                    <img src="{{ $page->getFirstMediaUrl('banner') }}" alt="" class="img-fluid w-100">
                </div>
            </section>
        @endif

        <section class="content_details">
            <div class="section-heading p-top-80">
                <h2>{{ $page->heading }}</h2>
                <p class="section-intro">{!! $page->description !!}</p>
            </div>
        </section>

        @if ($page->packages->count())
            <section id="pricing" class="bg-light pb-3">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 m-auto col-sm-12">
                            <div class="section-heading text-center">
                                <h2>{{ $page->package_heading }}</h2>
                                <p class="text-muted lead">{{ $page->package_sub_heading }}</p>
                                <div class="seperator"></div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        @foreach ($page->packages as $package)
                            <div class="col-lg-4 col-sm-6">
                                <div class="pricing-box my-2">
                                    <h4>{{ $package->heading }}</h4>
                                    <h6 class="mx-auto px-2 py-1">{{ $package->sub_heading }}</h6>
                                    <!-- <p class="text-muted">Monthly Package</p> -->
                                    <h3 class="price">
                                        <sup>$</sup>{{ $package->price }}
                                    </h3>
                                    <!--range-->
                                    <div class="ammount">
                                      <p><span id="amount"></span>$</p>
                                        <input type="range" min="1" max="2000" value="110" class="slider amnt" id="myRange">
                                    </div>
                                    <div class="day">
                                      <p><span id="day"></span>day</p>
                                      <input type="range" min="1" max="200" value="1" class="slider day" id="myRange2">
                                    </div>

                                    <p>Total<span id="demo"></span>৳  in <span id="days"></span>Day</p>
                                    {{-- <p>Total <span id="demo2"></span>৳</p> --}}

                                    <!--range-->

                                    <ul class="px-4">
                                        @foreach ($package->features as $feature)
                                            <li>
                                                <p>
                                                    <span> <i class="fas fa-check"></i> </span>{{ $feature }}
                                                </p>
                                            </li>
                                        @endforeach
                                    </ul>

                                    <a href="{{ route('package.purchase', $package->id) }}"
                                        class="btn btn-trans-black circled">Buy Now</a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </section>
        @endif
    </div>
@endsection
@push('scripts')
    <script>
        var slider = document.getElementById("myRange");
        var slider2 = document.getElementById("myRange2");
        var output = document.getElementById("demo");
        var amount = document.getElementById("amount");
        var day = document.getElementById("day");
        // output.innerHTML = amount * day ;
        day.innerHTML = slider2.value;
 
        // output.innerHTML = amountc * dayc;


        slider.oninput = function() {
            amount.innerHTML = this.value;
            // var amountc = this.value;
        }
        slider2.oninput = function() {
            day.innerHTML = this.value;
            // var dayc = this.value;
            
        }
       
        
    </script>
@endpush
