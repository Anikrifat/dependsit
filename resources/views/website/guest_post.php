<?php include 'inc/header.php' ?>
<div class="content pt-5 mt-4">
    <section class="content_banenr">
    <div class="banner_area">
    <img src="assets/images/gb.jpg" alt="" class="img-fluid">

    </div>

    </section>
    <section class="content_details">
    <div class="section-heading p-top-80">
            <h2>What is Guest Post</h2>
            <p class="section-intro">
                2021 is all about making a detailed SEO strategy to improve search rankings to capture more organic traffic for the website. We are the SEO service provider company in Bangladesh offering industry-leading SEO services all over the world. Our SEO Services will help you to achieve high-ranking placement in search results. SEO is much more effective than conventional advertising. We help you to increase your traffic, leads, and sales through our white label SEO services.</p>
        </div>
    </section>
    <section id="pricing" class="bg-light pb-3">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 m-auto col-sm-12">
                    <div class="section-heading text-center">
                        <h2>Pricing Offer that suit Your Need</h2>
                        <p class="text-muted lead">Great service with best options</p>
                        <div class="seperator"></div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-4 col-sm-6">
                    <div class="pricing-box my-2">
                        <h4>DA50+</h4>
                        <h6 class="mx-auto px-2 py-1">10 GUEST POSTS INCLUDED</h6>
                        <!-- <p class="text-muted">Monthly Package</p> -->
                        <h3 class="price"><sup>$</sup>25.90</h3>

                        <ul class="px-4">
                            <li><p><span> <i class="fas fa-check"></i> </span>DA50+ (10 Websites)</p></li>
                            <li><p><span> <i class="fas fa-check"></i> </span>Publish Your Articles</p></li>
                            <li><p><span> <i class="fas fa-check"></i> </span>2 Do Follow Contextual links Per Post</p></li>
                            <li><p><span> <i class="fas fa-check"></i> </span>Links on Real Traffic Website</p></li>
                            <li><p><span> <i class="fas fa-check"></i> </span>600+ Word Articles each (+200$)</p></li>
                            <li><p><span> <i class="fas fa-check"></i> </span>Normal Delivery Time 2 Weeks</p></li>
                            <li><p><span> <i class="fas fa-check"></i> </span>CASINO & CBD Allowed (+400$)</p></li>
                            <!-- <li>10 Social Account</li>
                            <li>32 <span>Analytics</span> Compaign</li>
                            <li>24 hour support</li> -->
                        </ul>

                        <a href="payment.php" class="btn btn-trans-black circled">Buy Now</a>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <div class="pricing-box my-2">
                        <h4>DA60+</h4>
                        <h6 class="mx-auto px-2 py-1">10 GUEST POSTS INCLUDED</h6>
                        <!-- <p class="text-muted">Monthly Package</p> -->
                        <h3 class="price"><sup>$</sup>45.90</h3>

                        <ul class="px-4">
                            <li><p><span> <i class="fas fa-check"></i> </span>DA60+ (10 Websites)</p></li>
                            <li><p><span> <i class="fas fa-check"></i> </span>Publish Your Articles</p></li>
                            <li><p><span> <i class="fas fa-check"></i> </span>2 Do Follow Contextual links Per Post</p></li>
                            <li><p><span> <i class="fas fa-check"></i> </span>Links on Real Traffic Website</p></li>
                            <li><p><span> <i class="fas fa-check"></i> </span>600+ Word Articles each (+200$)</p></li>
                            <li><p><span> <i class="fas fa-check"></i> </span>Normal Delivery Time 2 Weeks</p></li>
                            <li><p><span> <i class="fas fa-check"></i> </span>CASINO & CBD Allowed (+400$)</p></li>
                            <!-- <li>10 Social Account</li>
                            <li>32 <span>Analytics</span> Compaign</li>
                            <li>24 hour support</li> -->
                        </ul>

                        <a href="payment.php" class="btn btn-trans-black circled">Buy Now</a>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <div class="pricing-box my-2">
                        <h4>DA70+</h4>
                        <h6 class="mx-auto px-2 py-1">10 GUEST POSTS INCLUDED</h6>
                        <!-- <p class="text-muted">Monthly Package</p> -->
                        <h3 class="price"><sup>$</sup>65.90</h3>

                        <ul class="px-4">
                            <li><p><span> <i class="fas fa-check"></i> </span>DA70+ (10 Websites)</p></li>
                            <li><p><span> <i class="fas fa-check"></i> </span>Publish Your Articles</p></li>
                            <li><p><span> <i class="fas fa-check"></i> </span>2 Do Follow Contextual links Per Post</p></li>
                            <li><p><span> <i class="fas fa-check"></i> </span>Links on Real Traffic Website</p></li>
                            <li><p><span> <i class="fas fa-check"></i> </span>600+ Word Articles each (+200$)</p></li>
                            <li><p><span> <i class="fas fa-check"></i> </span>Normal Delivery Time 2 Weeks</p></li>
                            <li><p><span> <i class="fas fa-check"></i> </span>CASINO & CBD Allowed (+400$)</p></li>
                            <!-- <li>10 Social Account</li>
                            <li>32 <span>Analytics</span> Compaign</li>
                            <li>24 hour support</li> -->
                        </ul>

                        <a href="payment.php" class="btn btn-trans-black circled">Buy Now</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php include 'inc/footer.php' ?>