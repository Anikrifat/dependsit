@extends('layouts.website')

@section('content')
    <div class="content">
        @php($banner = $page->getFirstMedia('banner'))
        @if ($banner)
            <section class="bannersection pt-5 mt-1">
                <!-- <img src="{{ asset('assets/website/images/back.png') }}" alt="" class="w-100 b_p img-fluid"> -->

                <div class="banm w-100">
                    <div class="px-md-5">
                        <div class="row">

                            <div class="col-12 col-md-6">

                                <div class="w_b_t  p-2">
                                    <div class="ad_text">
                                        <p class="text-dark2 ban_text">{{ $banner->getCustomProperty('heading') }}</p>
                                        <div class="bb mb-2"></div>
                                    </div>

                                    <div class="btm_text">
                                        <p class="text-dark2 fw-600">{{ $banner->getCustomProperty('description') }}</p>
                                        <a href="{{ route('page', 'contact-us') }}"
                                            class="btn btn-orange px-4 border-none text-white">TALK WITH OUR TEAM
                                            <i class="fas fa-arrow-right"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 text-center">
                                <img src="{{ $banner->getUrl() }}" alt="" class="s_p img-fluid">
                            </div>

                        </div>
                    </div>
                </div>
            </section>
        @endif

        <section class="section-1 bg-light text-center ">
            <div class="row">
                <div class="col-12 col-md-2"></div>
                <div class="col-md-8 col-12">
                    <p class="mb-1 font-orange fw-600 fs-1-5">{{ $page->sub_heading }}</p>
                    <p class="font-n-blue fw-800 fs-2-5">{{ $page->heading }}</p>
                    <div class="bb mx-auto mb-3"></div>
                    <p class=" fs-1 font-n-blue">{{ $page->description }}</p>
                </div>
                <div class="col-12  col-md-1"></div>
            </div>

        </section>
        <div class="section-2">
            <div class="simple-slider">
                <div class="simple-slider-holder">
                    <div class="service_slider">
                        <div id="text-carousel" class="carousel slide" data-ride="carousel" data-interval="false">
                            <div class="col-md-12">
                                <div class="carousel-inner">
                                    @foreach ($slides as $slide)
                                        <div id="item_{{ $loop->iteration }}"
                                            class="carousel-item {{ $loop->first ? 'active' : '' }}">
                                            <div class="row">
                                                {{-- <div class="col-sm-12 col-md-6 slider_text"> --}}
                                                {{-- <h3 class="homeslidertitle mb30"> --}}
                                                {{-- <span class="sub-header-color small">Discovery: </span><br> --}}
                                                {{-- TECHNICAL SEO AUDIT --}}
                                                {{-- </h3> --}}
                                                {{-- <p class="slideinfo"> --}}
                                                {{-- It's like a medical report for a doctor to find what's going on --}}
                                                {{-- your body. In this phase, we run a complete technical audit on --}}
                                                {{-- your website to find an error in term of search engine --}}
                                                {{-- optimization best practice. --}}
                                                {{-- <br> --}}
                                                {{-- <br> --}}
                                                {{-- Technical SEO audit includes everything related to the website, --}}
                                                {{-- for example, website coding, page speed, content, image, HTML --}}
                                                {{-- etc. Its help us to find where we need enhancement for --}}
                                                {{-- better ranking on the search engine results page. --}}
                                                {{-- </p> --}}
                                                {{-- <ul class="fivews"> --}}
                                                {{-- <li>Identify the error of the website.</li> --}}
                                                {{-- <li>Identify existing content.</li> --}}
                                                {{-- <li>Implement solutions.</li> --}}
                                                {{-- </ul> --}}
                                                {{-- <a href="/contacts/" class="seo-agency-btn seo-agency-btn-2"><span class="freemin">Request Consultation</span></a> --}}
                                                {{-- </div> --}}
                                                <div class="col-sm-12 col-md-12  slider_img">
                                                    <img class="lazy loading home-slide w-100 img-fluid"
                                                        src="{{ $slide->getFirstMediaUrl('slide') }}" width="493"
                                                        height="487" data-was-processed="true">
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="bx-controls bx-has-pager anim-section animated">
                                    <div
                                        class="bx-pager bx-default-pager left-slide carousel-indicators slider-pager anim-box">
                                        @foreach ($slides as $slide)
                                            <div class="bx-pager-item">
                                                <a data-target="#text-carousel" href=""
                                                    data-slide-to="{{ $loop->index }}"
                                                    class="bx-pager-link">{{ $loop->iteration }}</a>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pull-controls anim-section animated">
                    <div class="anim-box fade-anim">
                        <div class="hwdi-left pull left">
                            <a class="bx-prev carousel-control-prev" data-slide="prev" href="#text-carousel"><i
                                    class="fas fa-chevron-left fs-3 font-orange"></i></a>
                        </div>
                        <div class="hwdi-right pull right">
                            <a class="bx-next carousel-control-next" data-slide="next" href="#text-carousel">
                                <i class="fas fa-chevron-right fs-3 font-orange"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- <section class="section-3 pt-3">
          
          <p class="font-orange fs-3 fw-600 text-center">Our Products</p>
            <div class="LatestThemeDiv row">
              
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                    <div class="ts_theme_boxes">
                        <div class="ts_theme_boxes_img">
                            <a href="{{ route('page', 'product-details') }}"> <img
                                    src="https://cpatools.shop/repo/images/8ZjMJFy.jpg"
                                    title="Make Money Online (Amazon Refund Method)" style="width:360px;height:192px;"> </a>
                        </div>

                        <div class="ts_theme_boxes_info">
                            <div class="ts_theme_details">

                                <h4>Make Money Online (Amazon Refund Method)</h4>
                                <p> <a href="{{ route('page', 'product-details') }}"><i class="fa fa-user"
                                            aria-hidden="true"></i> Hunter</a> <a
                                        href="https://cpatools.shop/home/products/buy-method"><i class="fa fa-tag"
                                            aria-hidden="true"></i> BUY METHOD</a>

                                </p>


                            </div>
                            <div class="ts_theme_price">

                                <a href="{{ route('page', 'product-details') }}" class="ts_btn btn-danger text-light">
                                    Free </a>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                    <div class="ts_theme_boxes">
                        <div class="ts_theme_boxes_img">
                            <a href="{{ route('page', 'product-details') }}"> <img
                                    src="https://cpatools.shop/repo/images/NV7q8Ww.jpg"
                                    title="Malaysian Physical Visa Card + Malaysian Sim Card"
                                    style="width:360px;height:192px;"> </a>
                        </div>

                        <div class="ts_theme_boxes_info">
                            <div class="ts_theme_details">

                                <h4>Malaysian Physical Visa Card + Malaysian Sim Card</h4>
                                <p> <a href="{{ route('page', 'product-details') }}"><i class="fa fa-user"
                                            aria-hidden="true"></i> Hunter</a> <a
                                        href="https://cpatools.shop/home/products/info"><i class="fa fa-tag"
                                            aria-hidden="true"></i> DISCOUNT</a>

                                </p>


                            </div>
                            <div class="ts_theme_price">

                                <a href="{{ route('page', 'product-details') }}" class="ts_price">$ 30</a>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                    <div class="ts_theme_boxes">
                        <div class="ts_theme_boxes_img">
                            <a href="{{ route('page', 'product-details') }}"> <img
                                    src="https://cpatools.shop/repo/images/8ZjMJFy.jpg"
                                    title="Make Money Online (Amazon Refund Method)" style="width:360px;height:192px;"> </a>
                        </div>

                        <div class="ts_theme_boxes_info">
                            <div class="ts_theme_details">

                                <h4>Make Money Online (Amazon Refund Method)</h4>
                                <p> <a href="{{ route('page', 'product-details') }}"><i class="fa fa-user"
                                            aria-hidden="true"></i> Hunter</a> <a
                                        href="https://cpatools.shop/home/products/buy-method"><i class="fa fa-tag"
                                            aria-hidden="true"></i> BUY METHOD</a>

                                </p>


                            </div>
                            <div class="ts_theme_price">

                                <a href="{{ route('page', 'product-details') }}" class="ts_btn btn-danger text-light">
                                    Free </a>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </section> --}}
        {{-- <section class="section-3 text-center">
      <div class="row">
        <div class="col-12 col-md-1"></div>
        <div class="col-12 col-md-10">

          <p class="font-orange fs-3 fw-600">SEO Audit Software</p>
        <!-- <h5><img src="{{ asset('assets/website/images/slogan.png') }}" alt="" class="img-fluid"></h5> -->
          <div class="bb mx-auto mb-4"></div>
          <p class="font-n-blue fw-600">
            Powerful SEO Software designed for Marketing Agency, SEO Professional & the Entrepreneur. Now get
            your website ranking higher than ever and make your business the most recognizable online with
            Advanced SEO Audit Software.
          </p>

        </div>
        <div class="col-12 col-md-1"></div>

    </section> --}}
        {{-- <section class="our-process">
            <div class="max-width-1170">
                <div class="process-detail">
                    <ul class="clearfix">
                        <li class="even">
                            <div class="process-col">
                                <div class="process-text wow fadeIn" data-wow-duration=".6s" data-wow-delay=".3s"
                                    style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.3s; animation-name: fadeIn;">
                                    <h4>SEO PROJECT BRIEF</h4>
                                    <p>
                                        In this phase, we listen to your goal &amp; learn more about your company vision.
                                        We’ll review your products and services to understand what will be the best practice
                                        for your search engine optimization to drive quality traffic from the SERPs.</p>
                                </div>
                                <div class="process-circle wow fadeInLeft" data-wow-duration=".6s" data-wow-delay=".3s"
                                    style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.3s; animation-name: fadeInLeft;">
                                    <div class="circle-plus">
                                        <i class="fa fa-plus"></i>
                                    </div>
                                    <div class="circle-number">
                                        <div class="circle-number-in">01</div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="odd">
                            <div class="process-col">
                                <div class="process-text wow fadeIn" data-wow-duration=".6s" data-wow-delay=".3s"
                                    style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.3s; animation-name: fadeIn;">
                                    <h4>SEO KEYWORD RESEARCH</h4>
                                    <p>
                                        After analyzing the project, we conduct deep keyword research to find the best key
                                        phrase for your products and services. We also analyze keywords difficulty, search
                                        volume &amp; find long-tail keywords.</p>
                                </div>
                                <div class="process-circle wow fadeInRight" data-wow-duration=".6s" data-wow-delay=".3s"
                                    style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.3s; animation-name: fadeInRight;">
                                    <div class="circle-plus">
                                        <i class="fa fa-plus"></i>
                                    </div>
                                    <div class="circle-number">
                                        <div class="circle-number-in">02</div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="even">
                            <div class="process-col">
                                <div class="process-text wow fadeIn" data-wow-duration=".6s" data-wow-delay=".3s"
                                    style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.3s; animation-name: fadeIn;">
                                    <h4>IDENTIFY SEO COMPETITOR</h4>
                                    <p>
                                        You will be surprised by learning that SEO competitors are different from your
                                        business competitors. We find the competitor &amp; analyze their online strength to
                                        find the gap to improve your ranking on the search engines.</p>
                                </div>
                                <div class="process-circle wow fadeInLeft" data-wow-duration=".6s" data-wow-delay=".3s"
                                    style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.3s; animation-name: fadeInLeft;">
                                    <div class="circle-plus">
                                        <i class="fa fa-plus"></i>
                                    </div>
                                    <div class="circle-number">
                                        <div class="circle-number-in">03</div>
                                    </div>
                                    <div class="logo-detail wow fadeInRight animated" data-wow-duration=".6s"
                                        data-wow-delay=".3s"
                                        style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.3s; animation-name: fadeInRight;">
                                        <span>
                                            Profile Create <br>
                                            if required
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="odd">
                            <div class="process-col">
                                <div class="process-text wow fadeIn" data-wow-duration=".6s" data-wow-delay=".3s"
                                    style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.3s; animation-name: fadeIn;">
                                    <h4>TECHNICAL SEO AUDIT</h4>
                                    <p>
                                        After learning more about your keywords &amp; competitor we conduct a deep technical
                                        audit on your website. This audit helps us to identify what we need to work on
                                        On-Page &amp; Off-Page SEO for better ranking on search engine results pages.</p>
                                </div>
                                <div class="process-circle wow fadeInRight" data-wow-duration=".6s" data-wow-delay=".3s"
                                    style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.3s; animation-name: fadeInRight;">
                                    <div class="circle-plus">
                                        <i class="fa fa-plus"></i>
                                    </div>
                                    <div class="circle-number">
                                        <div class="circle-number-in">04</div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="even">
                            <div class="process-col">
                                <div class="process-text wow fadeIn" data-wow-duration=".6s" data-wow-delay=".3s"
                                    style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.3s; animation-name: fadeIn;">
                                    <h4>SEO Backlink Analyze</h4>
                                    <p>
                                        In most cases if your website is old or if you hire the wrong SEO company you might
                                        be part of a spam link building. Based on the project &amp; its budget, we run deep
                                        link research to find every single backlink across the website and analyze its
                                        health for ranking.</p>
                                </div>
                                <div class="process-circle wow fadeInLeft" data-wow-duration=".6s" data-wow-delay=".3s"
                                    style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.3s; animation-name: fadeInLeft;">
                                    <div class="circle-plus">
                                        <i class="fa fa-plus"></i>
                                    </div>
                                    <div class="circle-number">
                                        <div class="circle-number-in">05</div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="odd">
                            <div class="process-col">
                                <div class="process-text wow fadeIn" data-wow-duration=".6s" data-wow-delay=".3s"
                                    style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.3s; animation-name: fadeIn;">
                                    <h4>SEO Content Writing</h4>
                                    <p>
                                        We all know content is king! huh? But very little % of the marketer knows how
                                        effective SEO friendly contents are for your ranking on the search engine results
                                        page. Our creative SEO content writer can help you to write content for a higher
                                        ranking.</p>
                                </div>
                                <div class="process-circle wow fadeInRight" data-wow-duration=".6s" data-wow-delay=".3s"
                                    style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.3s; animation-name: fadeInRight;">
                                    <div class="circle-plus">
                                        <i class="fa fa-plus"></i>
                                    </div>
                                    <div class="circle-number">
                                        <div class="circle-number-in">06</div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="even">
                            <div class="process-col">
                                <div class="process-text wow fadeIn" data-wow-duration=".6s" data-wow-delay=".3s"
                                    style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.3s; animation-name: fadeIn;">
                                    <h4>On-Page SEO</h4>
                                    <p>
                                        This is the core of our search engine optimization services. Everybody knows what it
                                        is, but you can find very few who utilize the best practice of on-page optimization.
                                        We will perform the necessary on-page audit to improve the performance of every web
                                        page.</p>
                                </div>
                                <div class="process-circle wow fadeInLeft" data-wow-duration=".6s" data-wow-delay=".3s"
                                    style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.3s; animation-name: fadeInLeft;">
                                    <div class="circle-plus">
                                        <i class="fa fa-plus"></i>
                                    </div>
                                    <div class="circle-number">
                                        <div class="circle-number-in">07</div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="odd">
                            <div class="process-col">
                                <div class="process-text wow fadeIn" data-wow-duration=".6s" data-wow-delay=".3s"
                                    style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.3s; animation-name: fadeIn;">
                                    <h4>Local SEO citation</h4>
                                    <p>
                                        Local citation playing a big role in the ranking on the search engine results page.
                                        By citing the local business, the possibility of being found on local searches will
                                        be increased. We are the best SEO services provider company in Bangladesh, help you
                                        to be listed on the trusted online directories to foster your local SEO strategy.
                                    </p>
                                </div>
                                <div class="process-circle wow fadeInRight" data-wow-duration=".6s" data-wow-delay=".3s"
                                    style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.3s; animation-name: fadeInRight;">
                                    <div class="circle-plus">
                                        <i class="fa fa-plus"></i>
                                    </div>
                                    <div class="circle-number">
                                        <div class="circle-number-in">08</div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="even">
                            <div class="process-col">
                                <div class="process-text wow fadeIn" data-wow-duration=".6s" data-wow-delay=".3s"
                                    style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.3s; animation-name: fadeIn;">
                                    <h4>Off-Page SEO</h4>
                                    <p>
                                        Most SEO’s think off-page as a link building service to improve their ranking in the
                                        SERPs. But it's much more than that. We are the best SEO company with SEO experts in
                                        Bangladesh can ensure best practice
                                        <a href="/off-page-seo-techniques/">of off-page SEO</a> techniques for improving
                                        your ranking.
                                    </p>
                                </div>
                                <div class="process-circle wow fadeInLeft" data-wow-duration=".6s" data-wow-delay=".3s"
                                    style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.3s; animation-name: fadeInLeft;">
                                    <div class="circle-plus">
                                        <i class="fa fa-plus"></i>
                                    </div>
                                    <div class="circle-number">
                                        <div class="circle-number-in">09</div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="odd">
                            <div class="process-col">
                                <div class="process-text wow fadeIn" data-wow-duration=".6s" data-wow-delay=".3s"
                                    style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.3s; animation-name: fadeIn;">
                                    <h4>High DA Linkbuilding</h4>
                                    <p>
                                        DA stands for Domain Authority. We analyze the industry and find a similar website
                                        to build quality content for you, where you can generate organic links to improve
                                        your domain authority and ranking in the search engine result.</p>
                                </div>
                                <div class="process-circle wow fadeInRight" data-wow-duration=".6s" data-wow-delay=".3s"
                                    style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.3s; animation-name: fadeInRight;">
                                    <div class="circle-plus">
                                        <i class="fa fa-plus"></i>
                                    </div>
                                    <div class="circle-number">
                                        <div class="circle-number-in">10</div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="devlivered-box wow zoomIn animated" data-wow-duration=".6s" data-wow-delay=".3s"
                                style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.3s; animation-name: zoomIn;">
                                <i class="fa fa-rocket"></i>
                                <p>
                                    Drive Traffic<br>
                                    &amp; Sell
                                </p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </section> --}}

        @if ($services->count())
            <section class="service-area">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="service-heading">
                                <h1>Our Services</h1>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        @foreach ($services as $service)
                            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                                <div class="service-single-box">
                                    <img src="{{ $service->getFirstMediaUrl('icon') }}" class="scb-img"
                                        alt="SEO &amp; SMM">
                                    <a href="{{ $service->url ?? '#' }}">
                                        <h3>{{ $service->name }}</h3>
                                        {{-- <p>{{ $service->description }}</p> --}}
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </section>
        @endif

        <section class="home-client-section text-center">
            <div class="client-title p-1">
                <h2 class="text-center default-color-1">{{ $clientPage->heading }}</h2>
                <p class="default-color-1">{{ $clientPage->description }}</p>
            </div>
            <div class="customer-service-area">
                <div class="owl-carousel owl-1 owl-theme">
                    @foreach ($clients as $client)
                        <div class="item">
                            <img class="img-fluid" src="{{ $client->getFirstMediaUrl('logo') }}"
                                alt="{{ $client->name }}">
                        </div>
                    @endforeach
                </div>

            </div>
            <div class="mx-auto">
                <a href="{{ route('page', 'clients') }}"
                    class="btn btn-primary custom-button seo-agency-btn mbl-btn talk-to-expart-btn">View All Clients</a>
            </div>
        </section>
        <section class="contact-area">

            <div class="contact-bg"></div>
            <div class="container">
                <div class="row home-contact">

                    <div class="col-lg-6">
                        <div class="contact">
                            <h5>Get In Touch</h5>
                            <!--
                                        <p>
                                        Dhaka Trade Center, Level-9</br>Kawran Bazaar, Dhaka. </br>
                                        <strong>Phone:</strong> +880 1672691228</br>
                                        <strong>mail:</strong>text@webmail.com
                                        </p>

                                        <h5>Send a Massage</h5>
                                        -->
                            <form method="post" action="">
                                <div class="form-row">
                                    <div class="col-6">
                                        <input type="text" class="form-control custom-form" name="cname"
                                            placeholder="Your name" required="">
                                    </div>
                                    <div class="col-6">
                                        <input type="text" class="form-control custom-form" name="email"
                                            placeholder="Email Address" required="">
                                    </div>
                                    <div class="col-6">
                                        <input type="text" class="form-control custom-form" name="company"
                                            placeholder="Company Name">
                                    </div>
                                    <div class="col-6">
                                        <input type="text" class="form-control custom-form" name="phone"
                                            placeholder="Your Phone Number" required="">
                                    </div>

                                    <div class="col-12">
                                        <textarea class="form-control cus-textarea" id="exampleFormControlTextarea1"
                                            name="messageBody" rows="3"></textarea>
                                    </div>


                                    <div class="col-12">
                                        <p class="text-right">
                                            <input type="submit" name="submit" class="contact-btn">
                                        </p>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="Rabbiitfirm-map">
                            <iframe
                                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3627.3343843742914!2d90.02937151544177!3d24.612157461224747!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x37562190a651f969%3A0x81c3c4f5d39f374e!2sMadhupur%20Tangail%20Dhaka%20Bangladesh!5e0!3m2!1sen!2sbd!4v1624353229386!5m2!1sen!2sbd"
                                width="100%" height="392" frameborder="0" style="border:0" allowfullscreen=""
                                loading="lazy"></iframe>
                            {{-- <iframe
                  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3651.3121054045614!2d90.35262916498174!3d23.771897884578816!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755c1b632c5e7c5%3A0x9be3603030538348!2sRabbiitfirm%20Limited!5e0!3m2!1sen!2sbd!4v1605433853218!5m2!1sen!2sbd"
                  ></iframe> --}}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@push('scripts')
    <script>
        $(document).ready(function() {
            $('.owl-carousel.owl-1').owlCarousel({
                loop: true,
                dots: true,
                margin: 5,
                nav: false,
                responsive: {
                    0: {
                        items: 3
                    },
                    600: {
                        items: 5
                    },
                    1000: {
                        items: 7
                    }
                }
            });
        });
    </script>
@endpush
