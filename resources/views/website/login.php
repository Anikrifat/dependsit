<?php include 'inc/header.php' ?>
<div class="content pt-5 mt-4">
    <section class="login py-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-5">
                    <h2 class="mb-5">Log In</h2>
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <label for="useremail">Username/Email</label>
                            <input type="email" id="useremail" class="form-control form-control-lg">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <label for="passlogin">Password</label>
                            <input type="password" id="passlogin" class="form-control form-control-lg">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <input type="submit" value="Login" class="btn btn-lg px-5 rounded-0">
                        </div>
                    </div>
                </div>
                <div class="col-lg-5">
                    <h2 class="mb-5">Register</h2>
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <label for="femail">Username/Email</label>
                            <input type="email" id="femail" class="form-control form-control-lg">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <label for="fpass">Password</label>
                            <input type="password" id="fpass" class="form-control form-control-lg">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <label for="fpass2">Re-type Password</label>
                            <input type="password" id="fpass2" class="form-control form-control-lg">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <input type="submit" value="Register" class="btn btn-lg px-5 rounded-0">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php include 'inc/footer.php' ?>