@extends('layouts.website')

@section('content')
  <div class="content pt-5 mt-4" x-data="{price: '{{ old('amount', $package->price) }}'}">
    <section class="payment py-5 my-4">
      <div class="container">
        <div class="row">
          <div class="col-12 col-md-3"></div>
          <div class="col-12 col-md-6">
            <form class="payment_form w-100" method="post" action="{{ route('package.purchase', $package->id) }}">
              @csrf
              <h3>Make Payment</h3>
              <h5> Choose amount </h5>
              <div id="button">
                <button type="button" class="button4 btn btn-light" @click="price = 5">5 Dollar</button>
                <button type="button" class="button4 btn btn-light" @click="price = 20">20 Dollar</button>
                <button type="button" class="button4 btn btn-light" @click="price = 50">50 Dollar</button>
                <button type="button" class="button4 btn btn-light" @click="price = 90">90 Dollar</button>
              </div>
              <div class="input-group">
                <input type="text" name="amount" class="form-control" placeholder="other amount" :value="price">
              </div>
              @error('amount')
              <span class="text-sm text-danger">{{ $message }}</span>
              @enderror
              <button class="btn btn-block npay" type="submit">Next</button>
            </form>
          </div>
          <div class="col-12 col-md-3"></div>
        </div>
      </div>
    </section>
  </div>
@endsection

@push('scripts')
  <script defer src="https://unpkg.com/alpinejs@3.x.x/dist/cdn.min.js"></script>
@endpush
