@extends('layouts.website')

@section('content')
  <div class="content pt-5 mt-4">

    <section class="portfolio">
      <div class="jumbotron text-center" style="margin-bottom:0; background-color: #FBFCFF;">
        <div class="container">
          <div class="row">
            <div class="col-sm-12">
              <h1>{{ $page->heading }}</h1>
              <hr class="my-4">
              <div align="center">
                <button class="btn btn-orange rounded-0 fw-600 m-1 active filter-button" data-filter="all">all</button>
                <button class="btn btn-orange rounded-0 fw-600 m-1 filter-button" data-filter="{{ $types[1] }}">web design & development</button>
                <button class="btn btn-orange rounded-0 fw-600 m-1 filter-button" data-filter="{{ $types[0] }}">seo</button>
                <button class="btn btn-orange rounded-0 fw-600 m-1 filter-button" data-filter="{{ $types[2] }}">Digital Marketing</button>


                  <div class="row">
                    @foreach($portfolios as $portfolio)
                      <div class="col-lg-3 col-md-4 col-xs-6 thumb filter {{ $types[$portfolio->type] }}">
                        <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="{{ $portfolio->name }}"
                           data-url="{{ $portfolio->url }}"
                           data-image="{{ $portfolio->getFirstMediaUrl('portfolio') }}"
                           data-target="#image-gallery">
                          <img class="img-thumbnail"
                               src="{{ $portfolio->getFirstMediaUrl('portfolio') }}"
                               alt="{{ $portfolio->name }}">
                        </a>
                      </div>
                    @endforeach
                  </div>

                  <div class="modal fade" id="image-gallery" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                      <div class="modal-content rounded-0">
                        <div class="modal-body p-0 position-relative">
                          <button type="button" class="close m_close" data-dismiss="modal">
                            <span aria-hidden="true"><i class="fas fa-times text-dark bg-white p-3 rounded-circle px-3 py-2 fs-2"></i></span><span class="sr-only">Close</span>
                          </button>
                          <img id="image-gallery-image" class="img-responsive col-md-12 p-0" src="">
                        </div>
                        <div class="modal-footer p-0">
                          <div class="row">
                            <div class="col-12 col-md-8">
                              <a href="#" class="btn btn-block btn-danger rounded-0 m-0 text-white">Visit</a>
                            </div>
                            <div class="col-12 col-md-4">

                              <div class="d-flex justify-content-betwwen">
                                <button type="button" class="btn btn-secondary btn-block m-0 rounded-0" id="show-previous-image">
                                  <i class="fa fa-arrow-left"></i>
                                </button>
                                <button type="button" id="show-next-image" class="btn btn-success btn-block m-0 rounded-0">
                                  <i class="fa fa-arrow-right"></i>
                                </button>
                              </div>
                            </div>
                          </div>

                        </div>
                      </div>
                    </div>
                  </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
@endsection
