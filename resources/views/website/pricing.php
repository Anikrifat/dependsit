<?php include 'inc/header.php' ?>
<div class="content pt-5 mt-4">
<section id="pricing py-4 my-3" class="bg-light">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 m-auto col-sm-12">
                    <div class="section-heading text-center">
                        <h2>Pricing Offer that suit Your Need</h2>
                        <p class="text-muted lead">Great service with best options</p>
                        <div class="seperator"></div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-4 col-sm-6">
                    <div class="pricing-box">
                        <h4>Standard</h4>
                        <p class="text-muted">Monthly Package</p>
                        <h3 class="price"><sup>$</sup>25.90</h3>

                        <ul>
                            <li><span>50GB</span> Bandwidth</li>
                            <li>10 Social Account</li>
                            <li>Business Analysing</li>
                            <li>24 hour <span>support</span></li>
                        </ul>

                        <a href="contact.php" class="btn btn-trans-black circled">Buy Now</a>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <div class="pricing-box">
                        <h4>Business</h4>
                        <p class="text-muted">Monthly Package</p>
                        <h3 class="price"><sup>$</sup>45.90</h3>

                        <ul>
                            <li>50GB <span>Bandwidth</span></li>
                            <li>32 Analytics Compaign</li>
                            <li>Business <span>Analysing</span></li>
                            <li><span>24 hour</span> support</li>
                        </ul>

                        <a href="contact.php" class="btn btn-trans-black circled">Buy Now</a>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <div class="pricing-box">
                        <h4>Premium</h4>
                        <p class="text-muted">Monthly Package</p>
                        <h3 class="price"><sup>$</sup>65.90</h3>

                        <ul>
                            <li><span>500GB </span>Bandwidth</li>
                            <li>10 Social Account</li>
                            <li>32 <span>Analytics</span> Compaign</li>
                            <li>24 hour support</li>
                        </ul>

                        <a href="contact.php" class="btn btn-trans-black circled">Buy Now</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php include 'inc/footer.php' ?>