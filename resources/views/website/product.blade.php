@extends('layouts.website')

@section('content')
<div class="content mt-5 pt-5">
    <div class="container">

        <div class="row">


            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="ts_heading ts_toppadder50 ts_bottompadder50">
                    <h3>OUR LATEST PRODUCT</h3>
                    <p>Get the best product in the market</p>
                </div>
            </div>
            {{-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="ts_theme_filter_wrapper">
                    <ul>
                        <li><a class="cateCls bg-danger text-light active" id="0">All</a></li>
                        <li><a class="cateCls bg-danger text-light" id="1">Course</a></li>
                        <li><a class="cateCls bg-danger text-light" id="3">Leads</a></li>

                    </ul>
                </div>
            </div> --}}

            <div class="LatestThemeDiv row">

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                    @include('layouts.includes.website.product-card')
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                    @include('layouts.includes.website.product-card')
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                    @include('layouts.includes.website.product-card')
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                    @include('layouts.includes.website.product-card')
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                    @include('layouts.includes.website.product-card')
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                    @include('layouts.includes.website.product-card')
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                    @include('layouts.includes.website.product-card')
                </div><div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                    @include('layouts.includes.website.product-card')
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                    @include('layouts.includes.website.product-card')
                </div>

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                    @include('layouts.includes.website.product-card')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')

@endpush
