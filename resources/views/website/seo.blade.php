@extends('layouts.website')

@section('content')
  <div class="content pt-5 mt-4 pb-3">
    @if($page->hasMedia('banner'))
      <section class="seo_banner">
        <img src="{{ $page->getFirstMediaUrl('banner') }}" class="imgfluid w-100" alt="Search Engine Optimization">
      </section>
    @endif

    <section class="clear services-content-tab bangladesh-best-seo-content-tab">
      <div class="section-heading p-top-80">
        <h2>{{ $page->heading }}</h2>
        <p class="section-intro">{!! $page->description !!}</p>
      </div>

      @if($page->hasMedia('tabs'))
        @php($tabOne = $page->getFirstMedia('tabs', ['key' => 'tab_one']))
        @php($tabTwo = $page->getFirstMedia('tabs', ['key' => 'tab_two']))
        @php($tabThree = $page->getFirstMedia('tabs', ['key' => 'tab_three']))
        @php($tabFour = $page->getFirstMedia('tabs', ['key' => 'tab_four']))
        <div class="max-width-1170">
          <div class="container tab-container">
            <div class="active-0">
              <ul id="tabs" class="nav nav-tabs" style="justify-content: center;">
                <style>
                  .bangladesh-best-seo-content-tab .tab-link a p::before {
                    background: url({{ asset('assets/website/images/bangladesh-best-seo-tab-icon.png') }});
                  }
                </style>
                <li class="tab-link nav-item bg_icn_1 active">
                  <a data-toggle="tab" expanded="true" roll="tab" href="#or_slide1" class="active show">
                    <p class="normal"></p>
                    <p class="active2"></p>
                    <span>{{ $tabOne->getCustomProperty('name') }}</span>
                  </a>
                </li>
                <li class="tab-link nav-item bg_icn_2">
                  <a data-toggle="tab" expanded="true" roll="tab" href="#or_slide2" class="">
                    <p class="normal"></p>
                    <p class="active2"></p>
                    <span>{{ $tabTwo->getCustomProperty('name') }}</span>
                  </a>
                </li>
                <li class="tab-link nav-item bg_icn_3">
                  <a data-toggle="tab" expanded="true" roll="tab" href="#or_slide3" class="">
                    <p class="normal"></p>
                    <p class="active2"></p>
                    <span>{{ $tabThree->getCustomProperty('name') }}</span>
                  </a>
                </li>
                <li class="tab-link nav-item bg_icn_4">
                  <a data-toggle="tab" expanded="true" roll="tab" href="#or_slide4" class="">
                    <p class="normal"></p>
                    <p class="active2"></p>
                    <span>{{ $tabFour->getCustomProperty('name') }}</span>
                  </a>
                </li>
              </ul>
            </div>
            <div class="slider-section anim-section animated">
              <div class="tab-content main_nav_area">
                <div id="or_slide1" class="tab-pane fade active show">
                  <div class="tab-content-elemets">
                    <div class="icons moveUp">
                      <img class="lazy loaded" src="{{ $tabOne->getUrl() }}" alt="{{ $tabOne->getCustomProperty('heading') }}" data-was-processed="true">
                    </div>
                    <div class="txt-wrapper">
                      <h3>{{ $tabOne->getCustomProperty('heading') }}</h3>
                      <p>{{ $tabOne->getCustomProperty('description') }}</p>
                      <a href="{{ route('page', 'contact-us') }}" class="btn-arrow btn--red" tabindex="-1">
                        Request Consultation
                        <i class="fa fa-arrow-right"></i>
                      </a>
                    </div>
                  </div>
                </div>
                <div id="or_slide2" class="tab-pane fade">
                  <div class="tab-content-elemets">
                    <div class="icons moveUp">
                      <img class="lazy loaded" src="{{ $tabTwo->getUrl() }}" alt="{{ $tabTwo->getCustomProperty('heading') }}" data-src="{{ $tabTwo->getUrl() }}" data-was-processed="true">
                    </div>
                    <div class="txt-wrapper">
                      <h3>{{ $tabTwo->getCustomProperty('heading') }}</h3>
                      <p>{{ $tabTwo->getCustomProperty('description') }}</p>
                      <a href="{{ route('page', 'contact-us') }}" class="btn-arrow btn--red" tabindex="-1">
                        Request Consultation
                        <i class="fa fa-arrow-right"></i>
                      </a>
                    </div>
                  </div>
                </div>
                <div id="or_slide3" class="tab-pane fade">
                  <div class="tab-content-elemets">
                    <div class="icons moveUp">
                      <img class="lazy loaded" src="{{ $tabThree->getUrl() }}" alt="{{ $tabThree->getCustomProperty('heading') }}" data-src="{{ $tabThree->getUrl() }}" data-was-processed="true">
                    </div>
                    <div class="txt-wrapper">
                      <h3>{{ $tabThree->getCustomProperty('heading') }}</h3>
                      <p>{{ $tabThree->getCustomProperty('description') }}</p>
                      <a href="{{ route('page', 'contact-us') }}" class="btn-arrow btn--red" tabindex="-1">
                        Request Consultation
                        <i class="fa fa-arrow-right"></i>
                      </a>
                    </div>
                  </div>
                </div>
                <div id="or_slide4" class="tab-pane fade">
                  <div class="tab-content-elemets">
                    <div class="icons moveUp">
                      <img class="lazy loaded" src="{{ $tabFour->getUrl() }}" alt="{{ $tabFour->getCustomProperty('heading') }}" data-src="{{ $tabFour->getUrl() }}" data-was-processed="true">
                    </div>
                    <div class="txt-wrapper">
                      <h3>{{ $tabFour->getCustomProperty('heading') }}</h3>
                      <p>{{ $tabFour->getCustomProperty('description') }}</p>
                      <a href="{{ route('page', 'contact-us') }}" class="btn-arrow btn--red" tabindex="-1">
                        Request Consultation
                        <i class="fa fa-arrow-right"></i>
                      </a>
                    </div>
                  </div>
                </div>
                <div class="pull-controls fade-anim">
                  <div class="pr-left pull left">
                    <a class="bx-prev" href=""></a>
                  </div>
                  <div class="pr-right pull right">
                    <a class="bx-next" href=""></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="clear"></div>
      @endif
    </section>
    <section class="seo_services py-2">
      <div class="container text-center">
        <h1>{{ $page->getProperties('seo_heading') }}</h1>
        <p>{!! $page->getProperties('seo_description') !!}</p>
        <div class="row">
          @foreach($seoPages as $seoPage)
            <div class="col-sm-3">
              <div class="card">
                @if($seoPage->hasMedia('icon'))
                  <img class="img-fluid card-img-top" src="{{ $seoPage->getFirstMediaUrl('icon') }}" alt="Card image">
                @endif
                <div class="card-body">
                  <h3 class="card-title">{{ $seoPage->name }}</h3>
                  <p class="card-text">{{ $seoPage->sub_heading }}
                  </p>
                  <a href="{{ route('page', $seoPage->slug) }}" class="btn btn-info btn-block p-1">Buy Now</a>
                </div>
              </div>
            </div>
          @endforeach
        </div>
      </div>
    </section>
    <!-- <section id="pricing" class="bg-light">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 m-auto col-sm-12">
                    <div class="section-heading text-center">
                        <h2>Pricing Offer that suit Your Need</h2>
                        <p class="text-muted lead">Great service with best options</p>
                        <div class="seperator"></div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-4 col-sm-6">
                    <div class="pricing-box">
                        <h4>Standard</h4>
                        <p class="text-muted">Monthly Package</p>
                        <h3 class="price"><sup>$</sup>25.90</h3>

                        <ul>
                            <li><span>50GB</span> Bandwidth</li>
                            <li>10 Social Account</li>
                            <li>Business Analysing</li>
                            <li>24 hour <span>support</span></li>
                        </ul>

                        <a href="#" class="btn btn-trans-black circled">Sign Up</a>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <div class="pricing-box">
                        <h4>Business</h4>
                        <p class="text-muted">Monthly Package</p>
                        <h3 class="price"><sup>$</sup>45.90</h3>

                        <ul>
                            <li>50GB <span>Bandwidth</span></li>
                            <li>32 Analytics Compaign</li>
                            <li>Business <span>Analysing</span></li>
                            <li><span>24 hour</span> support</li>
                        </ul>

                        <a href="#" class="btn btn-trans-black circled">Sign Up</a>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <div class="pricing-box">
                        <h4>Premium</h4>
                        <p class="text-muted">Monthly Package</p>
                        <h3 class="price"><sup>$</sup>65.90</h3>

                        <ul>
                            <li><span>500GB </span>Bandwidth</li>
                            <li>10 Social Account</li>
                            <li>32 <span>Analytics</span> Compaign</li>
                            <li>24 hour support</li>
                        </ul>

                        <a href="#" class="btn btn-trans-black circled">Sign Up</a>
                    </div>
                </div>
            </div>
        </div>
    </section> -->
    <!-- <section>
            <div class="container">
                <div class="seo-contant">
                    <div class="seotitle text-center"> <h1>SEO [Search Engine Optimization]</h1></div>

                    <p>An online artistic game that is thrilling as well to keep your site google or any search engine loved one.
                        Audience keep in touch when they will get you on their <b>SERP</b> just in a click.
                        <b>Rabbiitfirm SEO</b> team helps to achieve the goal.</p>
                </div>
                <div class="seo-cint-item">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-right">
                            <div class="seo-item">
                                <b class="sub-title"><i class="fas fa-hand-point-right"></i> What we do? </b>
                                <p>Analyze and give the proper treat on every step to optimize your website.</p>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="seo-item-image">
                                <img src="https://codewareltd.com/assets/images/service/process.png" alt="What we do">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="seo-cint-item">
                    <div class="row">
                        <div class="col-lg-6 col-md-6  col-sm-6 col-xs-12">
                            <div class="seo-item-image">
                                <img src="https://codewareltd.com/assets/images/service/serp-service.png" alt="What result achieve by this foot step">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6  col-sm-6 col-xs-12 text-left">
                            <div class="seo-item">
                                <b class="sub-title"><i class="fas fa-hand-point-left"></i> What result achieve by this foot step?</b>
                                <p>Found your website in Google or any search engine search result</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="seo-cint-item">
                    <div class="row">
                        <div class="col-lg6 col-md-6 col-sm-6 col-xs-12 text-right">
                            <div class="seo-item">
                                <b class="sub-title"> <i class="fas fa-hand-point-right"></i> This giving you!</b>
                                <p>Much organic traffic that is targeted according to your website service </p>

                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="seo-item-image">
                                <img src="https://codewareltd.com/assets/images/service/Web-Traffic.png" alt="This giving you">

                            </div>
                        </div>
                    </div>
                </div>

                <div class="text-center seo-mmm-btn-con">
                    <a href="https://www.codewareltd.com/contact-us/" class="social-discuss-btn" target="_blank"> Let’s grow your Business with Team rabbiitfarm<span class="adi-bag">-Discuss</span></a>
                </div>

                <div class="seo-cint-item">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="seo-item-image">
                                <img src="https://codewareltd.com/assets/images/service/seo-process.png" alt="Codeware SEO through excellence process">

                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-left">
                            <div class="seo-item">
                                <b class="sub-title"> <i class="fas fa-hand-point-left"></i>RabbiITFirm SEO through excellence process</b>
                                <p>People are interested in SEO for their site in turn his/her business but need a smooth beginning – where to start.
                                    You already one step ahead off as you are in our website.
                                    We creates business value for your website in turns it will make your brand value.</p>
                                <p>We do site optimization and start expert’s process of the SEO. SEO team of Rabbiitfirm brings you to extra miles it he field of SEO.</p>

                            </div>
                        </div>

                    </div>
                </div>

                <div class="seo-cint-item">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-right">
                            <div class="seo-item">
                                <b class="sub-title"> <i class="fas fa-hand-point-right"></i> Rabbiitfirm SEO Agency</b>
                                <p><b>Rabbiitfirm SEO</b> team works under a solid plan and strategy that made especially for your website,
                                    we not only brings traffic to your site also we believe in conversion. We don’t follow customary tricks and techniques, i
                                    nstead of our SEO analyst do plan for your site in many different perspectives that
                                    surely hit on the board and improve site visibility within every search engine including Google.</p>

                                <p>We do close monitor on Google algorithm and its changes, and shuffle our work according to Google algorithm changes.
                                    You may have e-commerce site or any informatics archive we target the possible targeted exact audience traffic.</p>
                                <p>So why not <b>Rabbiitfirm SEO</b> team!</p>
                            </div>
                        </div>




                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="seo-item-image">
                                <img src="https://codewareltd.com/assets/images/service/SEO-Team.png" alt="Codeware SEO Agency">
                            </div>
                        </div>

                    </div>
                </div>




            </div>
        </section> -->
  </div>
@endsection
