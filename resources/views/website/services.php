<!DOCTYPE html>
<html lang="en">
<head>
  <title>Link Building Agency</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  
 <style>
 
 .payment_form{
        text-align:center;
        width:400px;
		padding:30px 15px;
		background:#e9ecef;
		border-radius:5px;
    }
   
    h1{
        margin:auto;
    }
    
input[type=text]{
	background:#2f2f5f;
	padding:2 5px;
	}
	
.payment_form .button {
  background-color:#ca4825db;
  border: none;
  color: white;
  padding: 5px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 15px 2px;
  cursor: pointer;
  width:355px;
  border-radius:50px;
}
#button .button4 {
background-color: #dae0e5;
border:none;
margin:10px 10px;
padding:5px 46px;
 }
 .payment_form h3,h5
 {
 color:#2f2f5f;
 }
 
 /* menu*/
 .menu ul {
  list-style-type: none;
  margin: 0;
  padding-bottom:100px;
  overflow: hidden;
  
}

 .menu ul li {
  float: left;
}

 .menu ul li a {
  display: block;
  color: black;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size:20px;
}

.footer_menu ul {
list-style:none;
}

.footer_menu ul li{
 display: block;
}
.footer_menu ul li a {
color:white;
padding:5px 0;
}
.seo_services{

padding:50px 0;
}
</style>
</head>
<body>

<div class="header text-center" style="background:url('images/back.png'); padding:40px 0;background-repeat: no-repeat;
    background-size: contain;"> 
	<div class="container">
	    <div class="row"> 
		 <div class="col-sm-6 text-left">
		 <img src="images/logo.png" alt="" width="50"/>
		 </div>
		 <div class="col-sm-6">
				<?php include ('menu.php') ;?>
			</div>
		</div>
	  <div class="row">
		<div class="col-sm-8">
		  <h3 style="color:#181b31; font-size: 50px; line-height: 60px;">Guest Post Services​ </h3>
		  <p>Our guest posts provide you and your clients with the topically relevant articles and links that you need to help improve and diversify your link profile. At Agency Backlinks, our team of in house writers and outreach staff ensure that your sites receive the most powerful and most topically relevant links back to your site.</p>
		 
		</div>
	  
		<div class="col-sm-4">
			<div class="payment_form">
				
				 <h3>Make Payment</h3>
				  <h5> Choose amount </h5>
					
					<div id="button">
					<button class="button4">5 Dollar</button>
					<button class="button4">20 Dollar</button>
					<button class="button4">50 Dollar</button>
					<button class="button4">90 Dollar </button>
					</div>
                <div class="input-group">				
					<input type="text" name="" class="form-control" placeholder="other amount">
				</div>
				<button type="submit" class="button"> Next </button>
				</div>
		  
		</div>
	  </div>
	</div>
</div>

<!--start seo seo services-->
<section class="seo_services">
	<div class="container text-center">
	<h3> BESPOKE FRESH OUTREACH CAMPAIGNS </h3>
	<h1>One-time pricing options </h1>
	<p> Unlike other link providers, we don't charge a monthly rental fee. This ensures you can easily budget for your campaign needs. </p>
 <style>
* {
  box-sizing: border-box;
}

.columns {
  float: left;
  width: 25%;
  padding: 8px;
}

.price {
  list-style-type: none;
  border: 1px solid #eee;
  margin: 0;
  padding: 0;
  -webkit-transition: 0.3s;
  transition: 0.3s;
}

.price:hover {
  box-shadow: 0 8px 12px 0 rgba(0,0,0,0.2)
}

.price .header {
  background-color:white;
  color: black;
  font-size: 25px;
}

.price li {
  border-bottom: 1px solid #eee;
  padding: 20px;
  text-align: center;
}

.price .grey {
  background-color: #eee;
  font-size: 20px;
}

.columns.button {
  background-color: #4CAF50;
  border: none;
  color: white;
  padding: 10px 25px;
  text-align: center;
  text-decoration: none;
  font-size: 18px;
}

@media only screen and (max-width: 600px) {
  .columns {
    width: 100%;
  }
  
 .right img,.industry img{
      
    width:400px;  
      
  }
}
</style>


<div class="columns">
  <ul class="price">
    <li class="header">DR20+ Guest Posts </li>
    <li>$125</li>
	<li>PER LINK </li> 
    <li>Over 20 domain rating </li>
    <li>Unlimited URLs per order</li>
    <li>Up to 500 words of premium </li>
    <li>Real websites with real traffic </li>
    <li>Real site owner relationships </li>
    <li>Full reports </li>
    <li class="grey"><a href="#" class="button">Order Now </a></li>
  </ul>
</div>

<div class="columns">
  <ul class="price">
    <li class="header">X10 Pack of DR30+ </li>
    <li>$150 </li>
	<li>PER LINK </li>

    <li>Over 20 domain rating </li>
    <li>Unlimited URLs per order</li>
    <li>Up to 500 words of premium </li>
    <li>Real websites with real traffic </li>
    <li>Real site owner relationships </li>
    <li>Full reports </li>
    <li class="grey"><a href="#" class="button">Order Now </a></li>
  </ul>
</div>

<div class="columns">
  <ul class="price">
    <li class="header">DR30+ Guest Posts </li>
    <li>$165 </li>
	<li>PER LINK </li>

    <li>Over 20 domain rating </li>
    <li>Unlimited URLs per order</li>
    <li>Up to 500 words of premium </li>
    <li>Real websites with real traffic </li>
    <li>Real site owner relationships </li>
    <li>Full reports </li>
    <li class="grey"><a href="#" class="button">Order Now</a></li>
  </ul>
</div>
<div class="columns">
  <ul class="price">
    <li class="header">DR40+ Guest Posts </li>
    <li>$249</li>
	<li>PER LINK </li>

    <li>Over 20 domain rating </li>
    <li>Unlimited URLs per order</li>
    <li>Up to 500 words of premium </li>
    <li>Real websites with real traffic </li>
    <li>Real site owner relationships </li>
    <li>Full reports </li>
    <li class="grey"><a href="#" class="button">Order Now</a></li>
  </ul>
</div>
	</div>
</section>
<!--end seo servies-->

<!--start customer-->
<section class="customer" style="background:#F0F9FF;padding:60px 0;">
	<div class="container">
	<h1>Our results speak for themselves </h1>
	<p>Learn why hundreds of SEO professionals choose Agency Backlinks to propel their marketing efforts. </p>
		 <div class="row">
			<div class="col-sm-6">
			  <h3>James Dooley – FATRANK</h3>        
              <p> “James and his team enable us to maximize our agency deliverables by providing a reliable white label SEO solution. They get it done.” </p>
			</div>
				<div class="col-sm-6">
			  <h3>Kurt Phillip – Convertica</h3>        
              <p>Agency Backlinks always deliver and adapt to our industry needs. The best white hat links we’ve purchased” </p>
			</div>
			
		 </div>
	</div>
</section>
<!--end customer-->
<section class="bbc" style="background:#6071FE;padding:10px;">
	<img src="images/forbes.png"/>
</section>


<!--start customer-->
<section class="customer" style="background:#F0F9FF;padding:60px 0;">
	<div class="container">
	
		 <div class="row">
			<div class="col-sm-6">
			  <h3>Links you can actually brag about</h3>        
              <p> Simply provide your dedicated account manager your website URL and target phrases and let us do the rest </p>
			  <div class="post">
			 <h3> Real Genuine Websites </h3>
<p> Forget automated backlinks or crappy looking blogs; our sites are made by real authors and webmasters who care about producing top quality content </p>
			  </div>
			  <div class="post">
			 <h3> Aged In-Content Links </h3>
<p> Links from pages that have garnered years of trust and authority in their own right. Let's transfer that trust onto your websites </p>
			  </div>
			  <div class="post">
			 <h3> Years Of Agency Experience </h3>
<p> Our team all have years in the industry whether working with huge brands in an agency environment or on their websites </p>
			  </div>
			</div>
				<div class="col-sm-6">
				    <div class="right">
					    <img src="images/firstimage.png" class="img-responsive" width="560" alt="no"/>
					</div>
			</div>
			
		 </div>
	</div>
</section>
<!--end customer-->

<section class="bbc" style="background:#6071FE;padding:50px;">
<div class="container">
     <h3> INDUSTRY PROVEN </h3>
	<h1> The proof is in the pudd..errr Google search results </h1>
	<div class="industry"> 
	<img src="images/cloudcomputingcourses.jpg" alt="no"/>
	</div>
</div>
</section>
<!--end google search-->
<section class="seo_services" style="background:#F1F1F4;padding:100px 0;">
	<div class="container">
	<h3> REGAIN TIME AND CLARITY </h3>
	<h1> Why smart SEO teams outsource to us </h1>
	
		 <div class="row">
			<div class="col-sm-4">
			  <h3>Comprehensive SEO Management</h3>        
              <p>We are your one-stop-shop for your SEO and brand transformation </p>
			</div>
				<div class="col-sm-4">
			  <h3>Driving Significant Results</h3>        
              <p>Providing our customers with a healthy ROI… and then some </p>
			</div>
			<div class="col-sm-4">
			  <h3>Tailor-made Marketing Campaigns</h3>        
               <p> No project is ever too big or too small for us </p> 
			</div>
		 </div> <!--end row-->
		 <div class="row">
			<div class="col-sm-4">
			  <h3>No Passing The Book</h3>        
              <p>A seamless working relationship that combines efficient communication channels with real results </p>
			</div>
				<div class="col-sm-4">
			  <h3>Relationships With Industry Leaders</h3>        
              <p>We have landed links with Forbes, Huffington Post, and many other huge publications </p>
			</div>
			<div class="col-sm-4">
			  <h3>Our Mission Goes Further Than SEO</h3>        
               <p> A symbiotic working relationship designed to transform all aspects of your business</p> 
			</div>
		 </div> <!--end row-->
	</div>
</section>
<!--end seo servies-->


<section class="bbc" style="background:#6071FE;padding:50px;">
<div class="container">
     <h3> OUR ACTIONABLE AND DETAILED RESOURCES </h3>
	<h1>Fresh insights from our minds </h1>
	<div class="row">
		<div class="col-sm-4">
		<img src="images/jameshome-150x150.jpg" alt="no"/>
		  <h3> How To Scrape Google (Maps & SERPs) For B2B Leads </h3>
		  <p> In this post, we’ll be covering everything in the video below, where you can follow along and get clarity on the processes </p>
		</div>
		<div class="col-sm-4">
		<img src="images/jameshome-150x150.jpg" alt="no"/>
		  <h3> Building 100% White Hat Infographic Links At Scale </h3>
		  <p> Introduction This post is based on the private workshop we ran for our clients  </p>
		</div>
	</div>
</div>
</section>

<!--start accordion-->
<section class="bbc" style="background:#FDFDFD;padding:50px;">
<div class="container">
     <h3> FREQUENTLY ASKED QUESTIONS </h3>
	<h1>Got questions? We've got answers </h1>
	
	<div id="accordion">
  <div class="card">
    <div class="card-header" id="headingOne">
      <h5 class="mb-0">
        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          Collapsible Group Item #1
        </button>
      </h5>
    </div>

    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
      <div class="card-body">
        With your fully managed seo campaign, like it says in the title – your seo is fully managed. This means links, on page optimisation and every other moving part in the seo machine is carried out by our specialists. Whatever the package you decide to choose, you and your clients will have access to the very best when it comes to our resources and fulfilment. Just sit back, relax and we’ll take care of everything seo. Ensure that you are choosing the right package for your business as you may not be at a stage to scale just yet (but we’ll be here waiting once you are 😉 )
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingTwo">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          Collapsible Group Item #2
        </button>
      </h5>
    </div>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
      <div class="card-body">
       With your fully managed seo campaign, like it says in the title – your seo is fully managed. This means links, on page optimisation and every other moving part in the seo machine is carried out by our specialists. Whatever the package you decide to choose, you and your clients will have access to the very best when it comes to our resources and fulfilment. Just sit back, relax and we’ll take care of everything seo. Ensure that you are choosing the right package for your business as you may not be at a stage to scale just yet (but we’ll be here waiting once you are 😉 ).
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingThree">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          Collapsible Group Item #3
        </button>
      </h5>
    </div>
    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
      <div class="card-body">
        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
      </div>
    </div>
  </div>
</div>
</div>
</section>
<!--end accordion-->


<!--start footer-->
<section class="footer" style="background:#182540;padding:50px; color:white;">
<div class="container">
	<div class="row">
		<div class="col-sm-3">
			<img src="images/logo.png" alt="no" width="50"/>
		</div>
		<div class="col-sm-3">
		<h2> COMPANY </h2>
			<div class="footer_menu">
				<ul>
				  <li><a href="#">About Us </a></li>
				  <li><a href="#">Contact Us </a></li>
				  <li><a href="#">GDPR Policy</a></li>
				  <li><a href="#">Earnings Disclaimer </a></li>
				  <li><a href="#">Privacy Policy </a></li>
				</ul>
			</div>
		</div>
		
		<div class="col-sm-3">
			<h2> OUR SERVICES </h2>
			<div class="footer_menu">
			<ul>
			  <li><a href="#home">Link Insertions </a></li>
			  <li><a href="#news">Guest Posts</a></li>
			  <li><a href="#contact">Fully Managed SEO</a></li>
			  <li><a href="#about">Local SEO Packages</a></li>
			  <li><a href="#about">Earnings Disclaimer</a></li>
			  <li><a href="#about">Privacy Policy</a></li>
			</ul>
			
			</div>
		</div>
		
		<div class="col-sm-3">
			<h2> Contact us </h2>
			<div class="footer_menu">
			<ul>
			  <li><a href="#home">Agency Backlinks</a></li>
			  <li><a href="#news">Suit 2A</a></li>
			  <li><a href="#contact">Blackthorn House</a></li>
			  <li><a href="#about">St Paul’s Square</a></li>
			  <li><a href="#about">Birmingham</a></li>
			  <li><a href="#about">	B3 1RL</a></li>
			</ul>
			</div>
		</div>
	  </div>
</div>
</section>
<!--end footer-->


 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>
