@extends('layouts.website')

@section('content')
  <div class="content pt-5 mt-4">
    <div class="site-section py-5">
      <div class="container">
        <div class="row">
          <div class="col-lg-6">
            <h2 class="mb-5 text-black">Order Info</h2>
            <form action="{{ route('package.purchase.place-order', [$package->id, $amount]) }}" method="post">
              @csrf
              <input type="hidden" name="amount" value="{{ $amount }}">
              <div class="row">
                <div class="col-md-6 form-group">
                  <label for="fname">First Name</label>
                  <input type="text" id="fname" name="first_name" class="form-control form-control-lg {{ $errors->has('first_name') ? 'is-invalid' : null }}" value="{{ old('first_name', auth()->user()->name ?? '') }}" {{ auth()->check() ? 'disabled' : '' }}>
                  @error('first_name')
                  <span class="invalid-feedback">{{ $message }}</span>
                  @enderror
                </div>
                <div class="col-md-6 form-group">
                  <label for="lname">Last Name</label>
                  <input type="text" id="lname" name="last_name" class="form-control form-control-lg {{ $errors->has('last_name') ? 'is-invalid' : null }}" value="{{ old('last_name') }}" {{ auth()->check() ? 'disabled' : '' }}>
                  @error('last_name')
                  <span class="invalid-feedback">{{ $message }}</span>
                  @enderror
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 form-group">
                  <label for="eaddress">Email Address</label>
                  <input type="text" id="eaddress" name="email" class="form-control form-control-lg {{ $errors->has('email') ? 'is-invalid' : null }}" value="{{ old('email', auth()->user()->email ?? '') }}" {{ auth()->check() ? 'disabled' : '' }}>
                  @error('email')
                  <span class="invalid-feedback">{{ $message }}</span>
                  @enderror
                </div>
                <div class="col-md-6 form-group">
                  <label for="tel">Mobile Number</label>
                  <input type="number" id="tel" name="mobile" class="form-control form-control-lg {{ $errors->has('mobile') ? 'is-invalid' : null }}" value="{{ old('mobile', auth()->user()->phone ?? '') }}" {{ auth()->check() ? 'disabled' : '' }}>
                  @error('mobile')
                  <span class="invalid-feedback">{{ $message }}</span>
                  @enderror
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 form-group">
                  <label for="message">Message</label>
                  <textarea id="message" name="message" cols="30" rows="2" class="form-control {{ $errors->has('message') ? 'is-invalid' : null }}">{{ old('message') }}</textarea>
                  @error('message')
                  <span class="invalid-feedback">{{ $message }}</span>
                  @enderror
                </div>
              </div>
              <div class="row">
                <div class="col-12">
                  <input type="submit" value="Place Order & Payment" class="btn btn-primary btn-lg px-5 mx-0">
                </div>
              </div>
            </form>
          </div>
          <div class="col-lg-5 ml-auto">
            <div class="mb-3 bg-light">
              <h3 class="mb-5 text-black">Order Summery</h3>
              <p class="mb-0 font-weight-bold text-black">{{ $package->heading }}</p>
              <p class="mb-3 text-black">{{ $package->sub_heading }}</p>
              <p class="mb-0 font-weight-bold text-black">Features</p>
              @foreach($package->features as $feature)
                <p class="text-black mb-0" href="#">
                  <i class="fa fa-arrow-right"></i> {{ $feature }}
                </p>
              @endforeach
              <p class="mt-3 text-black">
                <strong>Amount: </strong> $ {{ $amount }}
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
