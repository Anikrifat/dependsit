@extends('layouts.website')

@section('content')
  <div class="content pt-5 mt-4">
    <section class="service_baner">
      <img src="{{ asset('assets/website/images/web-desig.jpg') }}" alt="" class="img-fluid w-100">
    </section>
    <section class="web_content">
      <div class="container">
        <div class="web-application">
          <div class="text-center web-title">
            <h1>web Development</h1>
            <p>We offer a
              <strong> wide range</strong> of services to reach your
              <strong>targeted audience</strong> by sharing your valuable information and focusing on retaining your customers.
            </p>
          </div>
          <div class="service-section">
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="web-item webapp-item">
                  <a href="#">
                    <img src="https://codewareltd.com/assets/images/service/serv-Web-application.png" alt="WEB APPLICATION">
                    <h1>WEB APPLICATION</h1>
                  </a>
                </div>
              </div>

              <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                <div class="web-item webdesign-item">
                  <a href="#">
                    <img src="https://codewareltd.com/assets/images/service/serv-Web-Design.png" alt="WEB DESIGN">
                    <h1>WEB Development</h1>
                  </a>
                </div>
              </div>

              <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                <div class="web-item psddesign-item">
                  <a href="#">
                    <img src="https://codewareltd.com/assets/images/service/serv-PSD-to-HTML.png" alt="HTML">
                    <h1>HTML</h1>
                  </a>
                </div>
              </div>

              <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                <div class="web-item webcmscus-item">
                  <a href="#">
                    <img src="https://codewareltd.com/assets/images/service/serv-CMScustomaization.png" alt="CMS CUSTOMIZATION">
                    <h1>CMS CUSTOMIZATION</h1>
                  </a>
                </div>
              </div>

              <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                <div class="web-item webecommerce-item">
                  <a href="#">
                    <img src="https://codewareltd.com/assets/images/service/serv-E-commerce.png" alt="E-commerce">
                    <h1>E-commerce</h1>
                  </a>
                </div>
              </div>
            </div>
          </div>

          <div class="about-sec">
            <div class="row">
              <div class="col-lg-12">
                <p>At
                  <strong>Rabbiitfirm LTD</strong>, a team of excellence web development do offshore web development to fulfill job provider planning and dream,
                  we development and develop application and control leading-edge web sites and e-business services that interprets small to large effects for an
                  organization in the virtual world.
                  <strong>Rabbiitfirm</strong>, a leading promising IT firm based in Bangladesh, allows customers according to serve theirs clients
                  higher through dependable techniques or that performs an essential position into the advancement concerning our customer’s organization.
                </p>
                <p>
                  <strong>Rabbiitfirm</strong> is outfitted including the armory of its exceptionally advanced technologies
                  specifically tailor-made for step to step website improvement services.
                </p>
                <p>We supply professional website or web applications according to most over the main countries
                  around the world as consists of Australia, Canada, India, UK and USA.
                </p>
                <p>We provide a huge organized proficient team about offshore website development and improvement functions.
                  We do planning, development, development and finally implement for the
                  valued customer – especially website developmenting, CMS based any kind of development, word press customization or even own customized CMS. </p>

              </div>
            </div>
          </div>

        </div>
      </div>
    </section>
  </div>
@endsection
