<?php

use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\CategorySectionController;
use App\Http\Controllers\Admin\ClientsController;
use App\Http\Controllers\Admin\GuestOrderController as GuestOrderControllerForAdmin;
use App\Http\Controllers\Admin\InvoiceController;
use App\Http\Controllers\Admin\PackagesController;
use App\Http\Controllers\Admin\PageGalleryController;
use App\Http\Controllers\Admin\PagesController as AdminPagesController;
use App\Http\Controllers\Admin\PageTabsController;
use App\Http\Controllers\Admin\PermissionController;
use App\Http\Controllers\Admin\PortfoliosController;
use App\Http\Controllers\Admin\PostController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\ServicesController;
use App\Http\Controllers\Admin\SettingController;
use App\Http\Controllers\Admin\SliderController;
use App\Http\Controllers\Admin\SubcategoryController;
use App\Http\Controllers\Admin\TagController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Buyer\ChatController;
use App\Http\Controllers\Buyer\OrderController;
use App\Http\Controllers\Website\OrderController as GuestOrderController;
use App\Http\Controllers\PayPalController;
use App\Http\Controllers\Website\PagesController;
use Illuminate\Support\Facades\Route;
use Spatie\MediaLibrary\MediaCollections\Models\Media as MediaAlias;

// cache clear
Route::get('reboot', function () {
  Artisan::call('cache:clear');
  Artisan::call('view:clear');
  Artisan::call('route:clear');
  Artisan::call('config:clear');
  Artisan::call('config:cache');
  Artisan::call('view:cache');
  dd('Done');
});

Route::get('storageLink', function () {
  Artisan::call('storage:link');
  dd('Done');
});

Route::get('migrate', function () {
  Artisan::call('migrate');
  dd('Done');
});

Route::get('cancel', [PayPalController::class, 'cancel'])->name('payment.cancel');
Route::get('payment/success', [PayPalController::class, 'success'])->name('payment.success');

Route::get('pay_invoice/{invoice}/{token}', [InvoiceController::class, 'invoicePayView'])->name('invoice.pay.view');

Route::prefix('dashboard')->middleware(['auth', 'verified'])->group(function () {
  Route::view('/', 'dashboard.dashboard')->name('dashboard');
  Route::resource('tag', TagController::class);
  Route::resource('category', CategoryController::class);
  Route::resource('subcategory', SubcategoryController::class);
  Route::resource('post', PostController::class);
  Route::resource('order', OrderController::class);
  Route::get('guest-order', [GuestOrderControllerForAdmin::class, 'showGuestOrders'])->name('guest.orders');
  Route::get('guest-order/{guestOrder}', [GuestOrderControllerForAdmin::class, 'showGuestOrderDetails'])->name('guest.orders.details');
  Route::post('guest-order/{guestOrder}/update-status', [GuestOrderControllerForAdmin::class, 'changeOrderStatus'])->name('guest.orders.update-status');
  Route::delete('guest-order/{guestOrder}/delete', [GuestOrderControllerForAdmin::class, 'deleteGuestOrder'])->name('guest.orders.delete');
  Route::resource('user', UserController::class);
  Route::resource('chat', ChatController::class);
  Route::resource('clients', ClientsController::class);
  Route::resource('portfolios', PortfoliosController::class);
  Route::resource('slider', SliderController::class);
  Route::resource('services', ServicesController::class);
  Route::get('invoice/pay/{invoice}', [InvoiceController::class, 'pay'])->name('invoice.pay');
  Route::resource('invoice', InvoiceController::class);
  Route::post('admin_chat', [ChatController::class, 'adminChat'])->name('admin.chat');
  Route::resource('settings/categorysection', CategorySectionController::class);
  Route::get('change_password', [UserController::class, 'change_password'])->name('change_password');
  Route::get('settings/company_settings', [SettingController::class, 'editCompanySetting'])->name('company.edit');
  Route::post('settings/company_setting', [SettingController::class, 'updateCompanySetting'])->name('company.update');
  Route::get('payLater/{order}', [OrderController::class, 'payLater'])->name('payLater.update');
  Route::post('changeOrderStatus/{order}', [OrderController::class, 'changeOrderStatus'])->name('changeOrderStatus');

  Route::prefix('website-settings')->group(function () {
    Route::resource('pages', AdminPagesController::class)->except(['show']);
    Route::prefix('pages/{page}')->group(function () {
      Route::put('packages/update-info', [PackagesController::class, 'updatePackageInfo'])->name('packages.update-info');
      Route::get('packages/get-package-info/{package}', [PackagesController::class, 'getPackageInfo']);
      Route::resource('packages', PackagesController::class)->except(['show', 'create', 'edit']);

      Route::get('gallery', [PageGalleryController::class, 'getPageGallery'])->name('pages.gallery');
      Route::put('gallery', [PageGalleryController::class, 'updatePageGallery']);

      Route::get('tabs', [PageTabsController::class, 'getPageTabs'])->name('pages.tabs');
      Route::put('tabs', [PageTabsController::class, 'updatePageTabs']);
    });
  });

  Route::delete('remove-media/{media}', function (MediaAlias $media) {
    $media->delete();
    return back()->with('success', 'Media successfully deleted.');
  })->name('remove-media');

  //Buyer & Reseller
  Route::get('guest_post', [\App\Http\Controllers\Buyer\PostController::class, 'allPost'])->name('guest_post.index');
  Route::get('guest_post/{post}', [\App\Http\Controllers\Buyer\PostController::class, 'viewPost'])->name('guest_post.view');
  Route::get('reseller_order', [\App\Http\Controllers\Buyer\PostController::class, 'resellerOrder'])->name('reseller_order.create');

  // Role Permission
  Route::resource('developer/permission', PermissionController::class);
  Route::get('role/assign', [RoleController::class, 'roleAssign'])->name('role.assign');
  Route::post('role/assign', [RoleController::class, 'storeAssign'])->name('store.assign');
  Route::resource('role', RoleController::class);

  //ajax
  Route::post('getSubcategory', [CategoryController::class, 'categorySubcategory'])->name('load.subcategory');
  Route::post('editSubcategory', [CategoryController::class, 'categoryEditSubcategory'])->name('edit.subcategory');
});

// Website routes
Route::get('purchase/{package}', [GuestOrderController::class, 'showAmountSelectionPage'])->name('package.purchase');
Route::post('purchase/{package}', [GuestOrderController::class, 'submitAmount']);
Route::get('purchase/{package}/{amount}/confirm-and-payment', [GuestOrderController::class, 'showConfirmAndPaymentPage'])->name('package.purchase.place-order');
Route::post('purchase/{package}/{amount}/confirm-and-payment', [GuestOrderController::class, 'placeOrder']);
Route::any('/{page:slug?}', [PagesController::class, 'showPages'])->name('page')->where('slug', '.*');
